﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Get
{
    public class Client
    {
        /// <summary>
        /// Método para obtener un objeto Client desde la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAO-GET-002
        /// </summary>
        /// <param name="id">identificador del cliente</param>
        /// <returns>Retorna un objeto Client</returns>
        public Dao.Client GetClient(int id)
        {
            try
            {
                var context = new XEROREPORTEntities();
                context.Configuration.LazyLoadingEnabled = false;
                var obj = (from c in context.Client
                           where c.idClient == id
                           select c).FirstOrDefault();
                return obj;
            }
            catch (Exception ex)
            {
                throw new Exception("SQL error: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para obtener un IQueryable de objetos Client desde la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAO-GET-003
        /// </summary>
        /// <param name="idCompany">identificador de la compañia</param>
        /// <returns>Retorna un IQueryable de objetos Client y su relación con Group</returns>
        public IQueryable<Dao.Client> GetClients(int idCompany)
        {
            try
            {
                var context = new XEROREPORTEntities();
                context.Configuration.LazyLoadingEnabled = false;
                var obj = (from c in context.Client.Include("Group").AsNoTracking() where c.idCompany.Equals(idCompany) select c);
                return obj;
            }
            catch (Exception ex)
            {
                throw new Exception("SQL error: " + ex.Message);
            }
        }
    }
}
