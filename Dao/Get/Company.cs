﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Get
{
    public class Company
    {
        /// <summary>
        /// Método para obtener un objeto Company desde la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAO-GET-004
        /// </summary>
        /// <param name="id">identificador de la compañia</param>
        /// <returns>Retorna un objeto Company</returns>
        public Dao.Company GetCompany(int id)
        {
            try
            {
                var context = new XEROREPORTEntities();
                context.Configuration.LazyLoadingEnabled = false;
                var obj = (from c in context.Company
                           where c.idCompany == id
                           select c).FirstOrDefault();
                return obj;
            }
            catch (Exception ex)
            {
                throw new Exception("SQL error: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para obtener un IQueryable de objetos Company desde la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAO-GET-005
        /// </summary>
        /// <returns>Retorna un IQueryable de objetos Company</returns>
        public IQueryable<Dao.Company> GetCompanies()
        {
            try
            {
                var context = new XEROREPORTEntities();
                context.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in context.Company.AsNoTracking() select x);
                return obj;
            }
            catch (Exception ex)
            {
                throw new Exception("SQL error: " + ex.Message);
            }
        }

    }
}
