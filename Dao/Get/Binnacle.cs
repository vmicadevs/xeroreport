﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Get
{
    public class Binnacle
    {
        /// <summary>
        /// Método para obtener un IQueryable de objetos Binnacle desde la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAO-GET-001
        /// </summary>
        /// <param name="idCompany">identificador de la compañia</param>
        /// <param name="types">tipo de bitacora a consultar</param>
        /// <returns>Retorna un IQueryable de objetos Binnacle</returns>
        public IQueryable<Dao.Binnacle> GetBinnacles(int idCompany, int types)
        {
            try
            {
                var context = new XEROREPORTEntities();
                context.Configuration.LazyLoadingEnabled = false;
                var obj = (from g in context.Binnacle.AsNoTracking()
                           where g.idCompany.Equals(idCompany)
                           where g.idTypeBinnacle.Equals(types)
                           select g);
                return obj;
            }
            catch (Exception ex)
            {
                throw new Exception("SQL error: " + ex.Message);
            }
        }
    }
}
