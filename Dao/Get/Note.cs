﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Get
{
    public class Note
    {
        /// <summary>
        /// Método para obtener un objeto Note desde la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAO-GET-009
        /// </summary>
        /// <param name="id">identificador de la nota</param>
        /// <returns>Retorna un objeto Note</returns>
        public Dao.Note GetNote(int id)
        {
            try
            {
                var context = new XEROREPORTEntities();
                context.Configuration.LazyLoadingEnabled = false;
                var obj = (from g in context.Note
                           where g.idNote == id
                           select g).FirstOrDefault();
                return obj;
            }
            catch (Exception ex)
            {
                throw new Exception("SQL error: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para obtener un IQueryable de objetos Note de una compañia desde la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAO-GET-010
        /// </summary>
        /// <param name="idCompany">identificador de la compañia</param>
        /// <returns>Retorna un IQueryable de objetos Note</returns>
        public IQueryable<Dao.Note> GetNotes(int idCompany)
        {
            try
            {
                var context = new XEROREPORTEntities();
                context.Configuration.LazyLoadingEnabled = false;
                var obj = (from n in context.Note.Include("Subnote").AsNoTracking() where n.idCompany.Equals(idCompany) select n);
                return obj;
            }
            catch (Exception ex)
            {
                throw new Exception("SQL error: " + ex.Message);
            }
        }
    }
}
