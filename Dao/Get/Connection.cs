﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Get
{
    public class Connection
    {
        /// <summary>
        /// Método para obtener un objeto Connection desde la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAO-GET-005
        /// </summary>
        /// <param name="id">identificador de la conexion</param>
        /// <returns>Retorna un objeto Connection</returns>
        public Dao.Connection GetConnection(int id)
        {
            try
            {
                var context = new XEROREPORTEntities();
                context.Configuration.LazyLoadingEnabled = false;
                var obj = (from c in context.Connection
                           where c.idConnection == id
                           select c).FirstOrDefault();
                return obj;
            }
            catch (Exception ex)
            {
                throw new Exception("SQL error: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para obtener un IQueryable de objetos Connection de una compañia desde la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAO-GET-006
        /// </summary>
        /// <param name="idcompany">identificador de la compañia</param>
        /// <returns>Retorna un IQueryable de objetos Connection</returns>
        public IQueryable<Dao.Connection> GetConnections(int idcompany)
        {
            try
            {
                var context = new XEROREPORTEntities();
                context.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in context.Connection.AsNoTracking() where x.idCompany.Equals(idcompany) select x);
                return obj;
            }
            catch (Exception ex)
            {
                throw new Exception("SQL error: " + ex.Message);
            }
        }
    }
}
