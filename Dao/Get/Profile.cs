﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Get
{
    public class Profile
    {
        /// <summary>
        /// Método para obtener un objeto Profile desde la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAO-GET-011
        /// </summary>
        /// <param name="id">identificador del perfil</param>
        /// <returns>Retorna un objeto Profile</returns>
        public Dao.Profile GetProfile(int id)
        {
            try
            {
                var context = new XEROREPORTEntities();
                context.Configuration.LazyLoadingEnabled = false;
                var obj = (from p in context.Profile
                           where p.idProfile == id
                           select p).FirstOrDefault();
                return obj;
            }
            catch (Exception ex)
            {
                throw new Exception("SQL error: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para obtener un IQueryable de objetos Profile desde la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAO-GET-012
        /// </summary>
        /// <returns>Retorna un IQueryable de objetos Profile</returns>
        public IQueryable<Dao.Profile> GetProfiles()
        {
            try
            {
                var context = new XEROREPORTEntities();
                context.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in context.Profile.AsNoTracking() select x);
                return obj;
            }
            catch (Exception ex)
            {
                throw new Exception("SQL error: " + ex.Message);
            }
        }

    }
}
