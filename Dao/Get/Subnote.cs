﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Get
{
    public class Subnote
    {
        /// <summary>
        /// Método para obtener un objeto Subnote desde la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAO-GET-013
        /// </summary>
        /// <param name="id">identificador de la sub nota</param>
        /// <returns>Retorna un objeto Subnote</returns>
        public Dao.Subnote GetSubnote(int id)
        {
            try
            {
                var context = new XEROREPORTEntities();
                context.Configuration.LazyLoadingEnabled = false;
                var obj = (from g in context.Subnote
                           where g.idSubnote == id
                           select g).FirstOrDefault();
                return obj;
            }
            catch (Exception ex)
            {
                throw new Exception("SQL error: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para obtener un IQueryable de objetos Subnote de una nota desde la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAO-GET-014
        /// </summary>
        /// <param name="idNote">identificador de la nota</param>
        /// <returns>Retorna un IQueryable de objetos Subnote</returns>
        public IQueryable<Dao.Subnote> GetSubnotes(int idNote)
        {
            try
            {
                var context = new XEROREPORTEntities();
                context.Configuration.LazyLoadingEnabled = false;
                var obj = (from n in context.Subnote.AsNoTracking() where n.idNote.Equals(idNote) select n);
                return obj;
            }
            catch (Exception ex)
            {
                throw new Exception("SQL error: " + ex.Message);
            }
        }
    }
}
