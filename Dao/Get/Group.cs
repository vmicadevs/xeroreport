﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Get
{
    public class Group
    {
        /// <summary>
        /// Método para obtener un objeto Group desde la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAO-GET-007
        /// </summary>
        /// <param name="id">identificador del grupo</param>
        /// <returns>Retorna un objeto Group</returns>
        public Dao.Group GetGroup(int id)
        {
            try
            {
                var context = new XEROREPORTEntities();
                context.Configuration.LazyLoadingEnabled = false;
                var obj = (from g in context.Group
                           where g.idGroup == id
                           select g).FirstOrDefault();
                return obj;
            }
            catch (Exception ex)
            {
                throw new Exception("SQL error: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para obtener un IQueryable de objetos Connection de una compañia desde la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAO-GET-008
        /// </summary>
        /// <param name="idcompany">identificador de la compañia</param>
        /// <returns>Retorna un IQueryable de objetos Connection</returns>
        public IQueryable<Dao.Group> GetGroups(int idCompany)
        {
            try
            {
                var context = new XEROREPORTEntities();
                context.Configuration.LazyLoadingEnabled = false;
                var obj = (from g in context.Group.Include("Client").AsNoTracking() where g.idCompany.Equals(idCompany) select g);
                return obj;
            }
            catch (Exception ex)
            {
                throw new Exception("SQL error: " + ex.Message);
            }
        }
    }
}
