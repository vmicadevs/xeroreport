﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Get
{
    public class User
    {
        /// <summary>
        /// Método para obtener un objeto User activo desde la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAO-GET-015
        /// </summary>
        /// <param name="username">nombre de usuario</param>
        /// <param name="password">hash de contraseña</param>
        /// <returns>Retorna un objeto User</returns>
        public Dao.User validaLogin(string username, string password)
        {
            try
            {
                var context = new XEROREPORTEntities();
                context.Configuration.LazyLoadingEnabled = false;
                var obj = (from u in context.User
                           where u.username.Equals(username)
                           where u.password.Equals(password)
                           where u.status == 1
                           select u).FirstOrDefault();
                return obj;
            }
            catch (Exception ex)
            {
                throw new Exception("SQL error: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para obtener un IQueryable de objetos User desde la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAO-GET-016
        /// </summary>
        /// <returns>Retorna un IQueryable de objetos User incluyendo su perfil y compañia</returns>
        public IQueryable<Dao.User> GetUsers()
        {
            try
            {
                var context = new XEROREPORTEntities();
                context.Configuration.LazyLoadingEnabled = false;
                var obj = (from x in context.User.Include("Profile").Include("Company").AsNoTracking() select x);
                return obj;
            }
            catch (Exception ex)
            {
                throw new Exception("SQL error: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para obtener un objeto User desde la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAO-GET-017
        /// </summary>
        /// <param name="id">identificador del usuario</param>
        /// <returns>Retorna un objeto User</returns>
        public Dao.User GetUser(int id)
        {
            try
            {
                var context = new XEROREPORTEntities();
                context.Configuration.LazyLoadingEnabled = false;
                var obj = (from u in context.User
                           where u.idUser.Equals(id)
                           select u).FirstOrDefault();
                return obj;
            }
            catch (Exception ex)
            {
                throw new Exception("SQL error: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para obtener un objeto User desde la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAO-GET-018
        /// </summary>
        /// <param name="username">nombre del usuario</param>
        /// <returns>Retorna un objeto User</returns>
        public Dao.User GetUser(string username)
        {
            try
            {
                var context = new XEROREPORTEntities();
                context.Configuration.LazyLoadingEnabled = false;
                var obj = (from u in context.User.AsNoTracking()
                           where u.username.Equals(username)
                           select u).FirstOrDefault();
                return obj;
            }
            catch (Exception ex)
            {
                throw new Exception("SQL error: " + ex.Message);
            }
        }
    }
}
