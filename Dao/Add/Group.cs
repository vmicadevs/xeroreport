﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Add
{
    public class Group
    {
        /// <summary>
        /// Método para insertar un objeto Group a la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAO-ADD-005
        /// </summary>
        /// <param name="group">Objeto Group</param>
        /// <returns>Retorna el id del objeto insertado.</returns>
        public int AddGroup(Dao.Group group)
        {
            try
            {
                var context = new XEROREPORTEntities();
                context.Group.Add(group);
                context.SaveChanges();
                return group.idGroup;
            }
            catch (Exception ex)
            {
                throw new Exception("SQL error: " + ex.Message);
            }
        }
    }
}
