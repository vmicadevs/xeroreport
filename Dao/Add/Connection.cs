﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Add
{
    public class Connection
    {
        /// <summary>
        /// Método para insertar un objeto Connection a la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAO-ADD-004
        /// </summary>
        /// <param name="connection">Objeto Connection</param>
        /// <returns>Retorna el id del objeto insertado.</returns>
        public int AddConnection(Dao.Connection connection)
        {
            try
            {
                var context = new XEROREPORTEntities();
                context.Connection.Add(connection);
                context.SaveChanges();
                return connection.idConnection;
            }
            catch (Exception ex)
            {
                throw new Exception("SQL error: " + ex.Message);
            }
        }
    }
}
