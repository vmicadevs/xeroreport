﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Add
{
    public class User
    {
        /// <summary>
        /// Método para insertar un objeto User a la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAO-ADD-008
        /// </summary>
        /// <param name="user">Objeto User</param>
        /// <returns>Retorna el id del objeto insertado.</returns>
        public long AddUser(Dao.User user)
        {
            try
            {
                var context = new XEROREPORTEntities();
                context.User.Add(user);
                context.SaveChanges();
                return user.idUser;
            }
            catch (Exception ex)
            {
                throw new Exception("SQL error: " + ex.Message);
            }
        }
    }
}
