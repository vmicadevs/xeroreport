﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Add
{
    public class Note
    {
        /// <summary>
        /// Método para insertar un objeto Note a la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAO-ADD-006
        /// </summary>
        /// <param name="note">Objeto Note</param>
        /// <returns>Retorna el id del objeto insertado.</returns>
        public int AddNote(Dao.Note note)
        {
            try
            {
                var context = new XEROREPORTEntities();
                context.Note.Add(note);
                context.SaveChanges();
                return note.idNote;
            }
            catch (Exception ex)
            {
                throw new Exception("SQL error: " + ex.Message);
            }
        }
    }
}
