﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Add
{
    public class Binnacle
    {
        /// <summary>
        /// Método para insertar un objeto Binnacle a la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAO-ADD-001
        /// </summary>
        /// <param name="binnacle">Objeto Binnacle</param>
        /// <returns>Retorna el id del objeto insertado.</returns>
        public long AddBinnacle(Dao.Binnacle binnacle)
        {
            try
            {
                var context = new XEROREPORTEntities();
                context.Binnacle.Add(binnacle);
                context.SaveChanges();
                return binnacle.idBinnacle;
            }
            catch (Exception ex)
            {
                throw new Exception("SQL error: " + ex.Message);
            }
        }

    }
}
