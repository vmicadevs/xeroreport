﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Add
{
    public class Client
    {
        /// <summary>
        /// Método para insertar un objeto Client a la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAO-ADD-002
        /// </summary>
        /// <param name="client">Objeto Client</param>
        /// <returns>Retorna el id del objeto insertado.</returns>
        public int AddClient(Dao.Client client)
        {
            try
            {
                var context = new XEROREPORTEntities();
                context.Client.Add(client);
                context.SaveChanges();
                return client.idClient;
            }
            catch (Exception ex)
            {
                throw new Exception("SQL error: " + ex.Message);
            }
        }
    }
}
