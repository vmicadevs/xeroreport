﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Add
{
    public class Company
    {
        /// <summary>
        /// Método para insertar un objeto Company a la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAO-ADD-003
        /// </summary>
        /// <param name="company">Objeto Company</param>
        /// <returns>Retorna el id del objeto insertado.</returns>
        public int AddCompany(Dao.Company company)
        {
            try
            {
                var context = new XEROREPORTEntities();
                context.Company.Add(company);
                context.SaveChanges();
                return company.idCompany;
            }
            catch (Exception ex)
            {
                throw new Exception("SQL error: " + ex.Message);
            }
        }

    }
}
