﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Add
{
    public class Subnote
    {
        /// <summary>
        /// Método para insertar un objeto Subnote a la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAO-ADD-007
        /// </summary>
        /// <param name="subnote">Objeto Subnote</param>
        /// <returns>Retorna el id del objeto insertado.</returns>
        public int AddSubnote(Dao.Subnote subnote)
        {
            try
            {
                var context = new XEROREPORTEntities();
                context.Subnote.Add(subnote);
                context.SaveChanges();
                return subnote.idSubnote;
            }
            catch (Exception ex)
            {
                throw new Exception("SQL error: " + ex.Message);
            }
        }
    }
}
