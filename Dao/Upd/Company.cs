﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Upd
{
    public class Company
    {
        /// <summary>
        /// Método para actualizar un registro de Company en la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAO-UPD-002
        /// </summary>
        /// <param name="company">Objeto Company</param>
        /// <returns>Retorna un numero entero con la cantidad de cambios realizados</returns>
        public int UpdCompany(Dao.Company company)
        {
            try
            {
                var context = new XEROREPORTEntities();
                context.Entry(company).State = System.Data.Entity.EntityState.Modified;
                return context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("SQL error: " + ex.Message);
            }
        }
    }
}
