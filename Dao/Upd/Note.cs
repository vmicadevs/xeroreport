﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Upd
{
    public class Note
    {
        /// <summary>
        /// Método para actualizar un registro de Note en la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAO-UPD-005
        /// </summary>
        /// <param name="note">Objeto Note</param>
        /// <returns>Retorna un numero entero con la cantidad de cambios realizados</returns>
        public int UpdNote(Dao.Note note)
        {
            try
            {
                var context = new XEROREPORTEntities();
                context.Entry(note).State = System.Data.Entity.EntityState.Modified;
                return context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("SQL error: " + ex.Message);
            }
        }
    }
}
