﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Upd
{
    public class Group
    {
        /// <summary>
        /// Método para actualizar un registro de Group en la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAO-UPD-004
        /// </summary>
        /// <param name="group">Objeto Group</param>
        /// <returns>Retorna un numero entero con la cantidad de cambios realizados</returns>
        public int UpdGroup(Dao.Group group)
        {
            try
            {
                var context = new XEROREPORTEntities();
                context.Entry(group).State = System.Data.Entity.EntityState.Modified;
                return context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("SQL error: " + ex.Message);
            }
        }
    }
}
