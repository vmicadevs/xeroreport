﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Upd
{
    public class User
    {
        /// <summary>
        /// Método para actualizar un registro de User en la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAO-UPD-007
        /// </summary>
        /// <param name="user">Objeto User</param>
        /// <returns>Retorna un numero entero con la cantidad de cambios realizados</returns>
        public int UpdUser(Dao.User user)
        {
            try
            {
                var context = new XEROREPORTEntities();
                context.Entry(user).State = System.Data.Entity.EntityState.Modified;
                return context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("SQL error: " + ex.Message);
            }
        }
    }
}
