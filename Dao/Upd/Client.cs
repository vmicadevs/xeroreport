﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Upd
{
    public class Client
    {
        /// <summary>
        /// Método para actualizar un registro de Client en la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAO-UPD-001
        /// </summary>
        /// <param name="client">Objeto cliente</param>
        /// <returns>Retorna un numero entero con la cantidad de cambios realizados</returns>
        public int UpdClient(Dao.Client client)
        {
            try
            {
                var context = new XEROREPORTEntities();
                context.Entry(client).State = System.Data.Entity.EntityState.Modified;
                return context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("SQL error: " + ex.Message);
            }
        }
    }
}
