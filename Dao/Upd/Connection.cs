﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Upd
{
    public class Connection
    {
        /// <summary>
        /// Método para actualizar un registro de Connection en la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAO-UPD-003
        /// </summary>
        /// <param name="connection">Objeto Connection</param>
        /// <returns>Retorna un numero entero con la cantidad de cambios realizados</returns>
        public int UpdConnection(Dao.Connection connection)
        {
            try
            {
                var context = new XEROREPORTEntities();
                context.Entry(connection).State = System.Data.Entity.EntityState.Modified;
                return context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("SQL error: " + ex.Message);
            }
        }
    }
}
