﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dao.Upd
{
    public class Subnote
    {
        /// <summary>
        /// Método para actualizar un registro de Subnote en la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAO-UPD-006
        /// </summary>
        /// <param name="subnote">Objeto Subnote</param>
        /// <returns>Retorna un numero entero con la cantidad de cambios realizados</returns>
        public int UpdSubnote(Dao.Subnote subnote)
        {
            try
            {
                var context = new XEROREPORTEntities();
                context.Entry(subnote).State = System.Data.Entity.EntityState.Modified;
                return context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("SQL error: " + ex.Message);
            }
        }
    }
}
