﻿$(document).ready(function () {

    getGroups();

    Test();

    $('#groupsList').select2({
        placeholder: 'Select an option'
    });

    $('#customersList').select2({
        placeholder: 'Select an option from groups'
    });

    $('#groupsList').change(function () {
        getCustomers();
    });

});

$("#btnDescargar").on("click", function () {


    $.ajax({
        url: '/Home/Archivo',
        type: "POST",
        dataType: 'json',
        success: function (data, textStatus, jqXHR) {
            if (data.Cod == 0) {
                window.location = '/Home/Descarga?Handler=' + data.Handler;
            }
        }, error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR + textStatus + errorThrown);
            console.log(jqXHR);
        }
    });
});

function getGroups() {
    $.ajax({
        url: '/Report/getGroups',
        type: "POST",
        dataType: 'json',
        success: function (data, textStatus, jqXHR) {
            if (data.Cod == 0) {

                $('#groupsList').empty();

                $.each(data.Data, function (i, item) {
                    $('#groupsList').append('<option value="' + item.id + '"> ' + item.name + ' </option>');

                });

                $("#groupsList").select2({
                    placeholder: {
                        id: '-1',
                        text: 'Select an option'
                    },
                });
                $('#groupsList').val(null).trigger('change');

                console.log(data.Data)
            }
        }, error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR + textStatus + errorThrown);
            console.log(jqXHR);
        }
    });
};

function getCustomers() {

    var groupId = $('#groupsList').select2('data');

    if (groupId.length === 0) {

        $('#customersList').empty();
        $('#customersList').select2({
            placeholder: {
                id: '-1',
                text: 'Select an option from groups'
            }
        });
    }
    else {

        $.ajax({
            url: '/Report/getGroup',
            type: "POST",
            dataType: 'Json',
            data: { id: groupId[0].id },
            success: function (data, textStatus, jqXHR) {
                if (data.Cod == 0) {
                    var lista = data.Data;
                    if (lista.length == 0) {

                        $('#customersList').empty();
                        $("#customersList").select2({
                            placeholder: 'not found result'
                        });
                    }
                    else {

                        $('#customersList').empty();

                        $.each(lista, function (i, item) {
                            $('#customersList').append('<option value="' + item.id + '"> ' + item.name + ' </option>');
                        });

                        $("#customersList").select2({
                            placeholder: {
                                id: '-1',
                                text: "Select an option"
                            }
                        });
                        $('#customersList').val(null).trigger('change');
                    }
                }
                else {
                    alert('Error al procesar la consulta,' + data.Msg);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Error al procesar la consulta, Intente Nuevamente.');
            }
        });
    }
}

function Test() {
    $.ajax({
        url: '/Report/TestAPI',
        type: "POST",
        dataType: 'json',
        success: function (data, textStatus, jqXHR) {
            if (data.Cod == 0) {
                console.log(data.Data)
            }
        }, error: function (jqXHR, textStatus, errorThrown) {
            //console.log(jqXHR + textStatus + errorThrown);
            console.log(jqXHR);
        }
    });
};