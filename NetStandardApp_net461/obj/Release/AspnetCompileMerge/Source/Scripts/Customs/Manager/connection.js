﻿$(document).ready(function () {

    $('#tblConnections').DataTable({
        paging: true,
        ordering: true,
        info: false,
        pageLength: 10,
        columnDefs: [{
            "targets": [4, 5],
            "orderable": false
        }],
        order: [[3, 'asc'], [0, 'asc']]
    });

    LoadConnections();

    $("#btnAdd").on("click", function () {
        var validator = $("#frmAdd").validate();
        validator.resetForm();
        $('input').removeClass('error');

        $("#name").val("");
        $("#clientid").val("");
        $("#clientsecret").val("");
        $("#status").val(1);

        $("#mdlAdd").modal('show');
    });

    $("#addconnection").on("click", function () {

        var l = Ladda.create(this);
        l.start();

        if ($("#frmAdd").valid()) {
            $.ajax({
                url: 'AddConnection',
                type: "POST",
                dataType: 'Json',
                data: $("#frmAdd").serialize(),
                success: function (data, textStatus, jqXHR) {

                    switch (data.Cod) {
                        case 1:
                            $('#mdlAdd').modal('toggle');
                            l.stop();
                            window.location.href = data.url;
                            break;
                        case 2:
                            l.stop();
                            swal("Add connection", data.Msg, "warning");
                            break;
                        default:
                            l.stop();
                            swal("Add connection", data.Msg, "error");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    l.stop();
                    swal({ title: 'Add connection', text: 'Error processing the query.', type: 'error' });
                }
            });
        }
        else {
            l.stop();
        }
    });

    $("#editconnection").on("click", function () {

        var l = Ladda.create(this);
        l.start();

        if ($("#frmEdit").valid()) {

            $.ajax({
                url: 'EditConnection',
                type: "POST",
                dataType: 'Json',
                data: {
                    editid: $("#editid").val(),
                    editname: $("#editname").val(),
                    editclientid: $("#editclientid").val(),
                    editclientsecret: $("#editclientsecret").val(),
                    editstatus: $("#editstatus").val()
                },
                success: function (data, textStatus, jqXHR) {
                    switch (data.Cod) {
                        case 1:
                            if (data.url == "") {
                                $("#mdlEdit").modal('toggle');
                                l.stop();
                                swal("Edit connection", data.Msg, "success");
                                LoadConnections();
                            }
                            else {
                                $("#mdlEdit").modal('toggle');
                                l.stop();
                                window.location.href = data.url;
                            }
                            break;
                        case 2:
                            l.stop();
                            swal("Edit connection", data.Msg, "warning");
                            break;
                        default:
                            l.stop();
                            swal("Edit connection", data.Msg, "error");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    l.stop();
                    swal({ title: 'Edit connection', text: 'Error processing the query.', type: 'error' });
                }
            });
        } else {
            l.stop();
        }
    });

    $('[data-toggle="tooltip"]').tooltip();
});

function showMdlEdit(id, name, clientid, clientsecret, status) {

    var validator = $("#frmEdit").validate();
    validator.resetForm();
    $('input').removeClass('error');

    $("#editid").val(id);
    $("#editname").val(name);
    $("#editclientid").val(clientid);
    $("#editclientsecret").val(clientsecret);
    $("#editstatus").val(status);

    $("#mdlEdit").modal('show');
}

function reauthorize(id) {

    var l = Ladda.create(document.querySelector('#btnreauthorize'));
    l.start();

    $.ajax({
        url: 'ReauthorizeConnection',
        type: "POST",
        dataType: 'Json',
        data: { id },
        success: function (data, textStatus, jqXHR) {
            switch (data.Cod) {
                case 1:
                    l.stop();
                    window.location.href = data.url;
                    break;
                case 2:
                    l.stop();
                    swal("Reauthorize connection", data.Msg, "warning");
                    break;
                default:
                    l.stop();
                    swal("Reauthorize connection", data.Msg, "error");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            l.stop();
            swal({ title: 'Reauthorize connection', text: 'Error processing the query.', type: 'error' });
        }
    });
}

function LoadConnections() {

    $('#iboxTableConnections').toggleClass('sk-loading');

    $.ajax({
        url: 'GetListConnections',
        type: "POST",
        dataType: 'Json',
        success: function (data, textStatus, jqXHR) {
            if (data.Cod == 1) {
                var lista = data.connections;
                if (lista.length == 0) {
                    $('#iboxTableConnections').toggleClass('sk-loading');
                    swal({ title: 'Search connections', text: 'No search results were found.', type: 'warning' });
                }
                else {
                    $('#tblConnections').DataTable().clear().draw();
                    $.each(lista, function (i, item) {
                        var status = 0;
                        var txtStatus = "";
                        switch (item.status) {
                            case 1:
                                txtStatus = '<span class="text-success">Active</span>';
                                status = 1;
                                break;
                            case 2:
                                txtStatus = '<span class="text-warning">Pending validation</span>';
                                status = 2;
                                break;
                            case 3:
                                txtStatus = '<span class="text-danger">Validation error</span>';
                                status = 3;
                                break;
                            default:
                                txtStatus = '<span class="text-danger">Inactive</span>';
                                status = 0;
                                break;
                        }
                        var txtbtn = item.id + ",'" + item.name + "','" + item.clientid + "','" + item.clientsecret + "'," + status;

                        //Reauthorize Xero

                        $("#tblConnections").DataTable().row.add([
                            item.name,
                            item.clientid,
                            item.clientsecret,
                            txtStatus,
                            '<div class="text-center"><button type="button" id="btnreauthorize" class="btn btn-xs btn-warning ladda-button" data-style="zoom-in" data-toggle="tooltip" title="Reauthorize Xero" onclick="reauthorize(' + item.id + ')"><i class="fa fa-plug"></i></button></div>',
                            '<div class="text-center"><button type="button" class="btn btn-xs btn-success" data-toggle="tooltip" title="Edit connection " onclick="showMdlEdit(' + txtbtn + ')"><i class="fa fa-edit"></i></button></div>'
                        ]);
                    });
                    $("#tblConnections").DataTable().draw(false);
                    $('[data-toggle="tooltip"]').tooltip();
                    $('#iboxTableConnections').toggleClass('sk-loading');
                }
            }
            else {
                $('#iboxTableConnections').toggleClass('sk-loading');
                swal({ title: 'Search connections', text: 'Error processing the query, ' + data.Msg, type: 'error' });
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('#iboxTableConnections').toggleClass('sk-loading');
            swal({ title: 'Search connections', text: 'Error processing the query.', type: 'error' });
        }
    });
}