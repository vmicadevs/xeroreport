﻿$(document).ready(function () {

    $('.dual_select').bootstrapDualListbox({
        selectorMinimalHeight: 200
    });

    var updateOutput = function (e) {
        var list = e.length ? e : $(e.target),
            output = list.data('output');
        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize')));
        } else {
            output.val('JSON browser support required for this demo.');
        }
    };

    $('#nestable').nestable({
        group: 1,
        maxDepth: 1
    }).on('change', updateOutput);

    updateOutput($('#nestable').data('output', $('#nestable-output')));

    $('#nestable-menu').on('click', function (e) {
        var target = $(e.target),
            action = target.data('action');
        if (action === 'expand-all') {
            $('.dd').nestable('expandAll');
        }
        if (action === 'collapse-all') {
            $('.dd').nestable('collapseAll');
        }
    });

    $("#bygroup").on('click', function () {
        $("#rowHome").hide();
        $("#rowGroup").show();
        $("#rowClient").hide();
        $("#rowNotes").hide();
        $("#rowOrder").hide();
        $("#back").show();
        $("#backnote").hide();
        getDatesGroup();
        getGroups();
    });

    $("#byclient").on('click', function () {
        $("#rowHome").hide();
        $("#rowGroup").hide();
        $("#rowNotes").hide();
        $("#rowOrder").hide();
        $("#rowClient").show();
        $("#back").show();
        $("#backnote").hide();
        getDatesClient();
        getClients();
    });

    $("#notes").on('click', function () {
        $("#rowHome").hide();
        $("#rowGroup").hide();
        $("#rowClient").hide();
        $("#rowOrder").hide();
        $("#rowNotes").show();
        $("#back").show();
        $("#backnote").hide();
    });

    $("#back").on('click', function () {
        $("#rowHome").show();
        $("#rowGroup").hide();
        $("#rowClient").hide();
        $("#rowNotes").hide();
        $("#rowOrder").hide();
        $("#back").hide();
        $("#backnote").hide();
    });

    $("#backnote").on('click', function () {
        $("#rowHome").hide();
        $("#rowGroup").hide();
        $("#rowClient").hide();
        $("#rowNotes").show();
        $("#rowOrder").hide();
        $("#back").show();
        $("#backnote").hide();
    });

    $('#bygroupListDate').select2({
        placeholder: 'Select an option'
    });

    $('#bygroupListGroups').select2({
        placeholder: 'Select an option'
    });

    $('#bygroupListClient').select2({
        placeholder: 'Select an option from groups'
    });

    $("#bygroupListClient").on("select2:select", function (evt) {
        var element = evt.params.data.element;
        var $element = $(element);

        $element.detach();
        $(this).append($element);
        $(this).trigger("change");
    });

    $('#bygroupListGroups').change(function () {
        getClientsByGroup();
    });

    $('#byclientListDate').select2({
        placeholder: 'Select an option'
    });

    $('#byclientListClient').select2({
        placeholder: 'Select an option'
    });

    $("#byclientListClient").on("select2:select", function (evt) {
        var element = evt.params.data.element;
        var $element = $(element);

        $element.detach();
        $(this).append($element);
        $(this).trigger("change");
    });

    $("#getreportbygroup").on("click", function () {

        var l = Ladda.create(document.querySelector('#getreportbygroup'));
        l.start();

        var dateId = $('#bygroupListDate').val();
        var groupId = $('#bygroupListGroups').val();
        var clientsIds = $('#bygroupListClient').val();

        if (groupId == null) {
            swal("Get report", "You must select a group.", "warning");
            l.stop();
        }

        if (clientsIds.length == 0) {
            swal("Get report", "You must select at least one client.", "warning");
            l.stop();
        }

        if (groupId != null && clientsIds.length > 0) {
            $.ajax({
                url: '/Report/GetReport',
                type: "POST",
                dataType: 'Json',
                data: { dateid: dateId, groupid: groupId, clientid: clientsIds },
                success: function (data, textStatus, jqXHR) {
                    switch (data.Cod) {
                        case 1:
                            swal("Get report", "Report generated successfully, download will start shortly", "success");
                            window.location = '/Report/Download?Handler=' + data.Handler + '&Group=' + data.Group;
                            getGroups();
                            getDatesGroup();
                            l.stop();
                            break;
                        case 2:
                            l.stop();
                            swal("Get report", data.Msg, "warning");
                            break;
                        default:
                            l.stop();
                            swal("Get report", data.Msg, "error");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    l.stop();
                    swal('Get report', 'Error processing the query.', 'error');
                }
            });
        }
    });

    $("#getreportbyclient").on("click", function () {

        var l = Ladda.create(document.querySelector('#getreportbyclient'));
        l.start();

        var groupId = 0;
        var clientsIds = $('#byclientListClient').val();
        var dateId = $('#byclientListDate').val();

        if (clientsIds.length == 0) {
            swal("Get report", "You must select at least one client.", "warning");
            l.stop();
        }

        console.log(clientsIds);

        if (groupId != null && clientsIds.length > 0) {
            $.ajax({
                url: '/Report/GetReport',
                type: "POST",
                dataType: 'Json',
                data: { dateid: dateId, groupid: groupId, clientid: clientsIds },
                success: function (data, textStatus, jqXHR) {
                    switch (data.Cod) {
                        case 1:
                            swal("Get report", "Report generated successfully, download will start shortly", "success");
                            window.location = '/Report/Download?Handler=' + data.Handler;
                            getClients();
                            getDatesClient();
                            l.stop();
                            break;
                        case 2:
                            l.stop();
                            swal("Get report", data.Msg, "warning");
                            break;
                        default:
                            l.stop();
                            swal("Get report", data.Msg, "error");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    l.stop();
                    swal('Get report', 'Error processing the query.', 'error');
                }
            });
        }
    });

    $("#btngetnotes").on("click", function () {

        var l = Ladda.create(document.querySelector('#btngetnotes'));
        l.start();

        if ($("#sltNotes").val().length > 0) {
            $.ajax({
                url: '/Report/GetListNotes',
                type: "POST",
                dataType: 'Json',
                data: {
                    idNotes: JSON.stringify($("#sltNotes").val())
                },
                success: function (data, textStatus, jqXHR) {
                    switch (data.Cod) {
                        case 1:
                            swal("Generate notes", "Notes generated successfully, download will start shortly", "success");
                            window.location = '/Report/DownloadNotes?Handler=' + data.Handler;
                            l.stop();
                            break;
                        case 2:
                            l.stop();
                            swal("Generate notes", data.Msg, "warning");
                            break;
                        default:
                            l.stop();
                            swal("Generate notes", data.Msg, "error");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    l.stop();
                    swal('Generate notes', 'Error processing the query.', 'error');
                }
            });
        } else {
            l.stop();
            swal("Generate notes", "You must select at least one note.", "warning");
        }
    });

    $("#btneditorder").on("click", function () {

        var l = Ladda.create(document.querySelector('#btneditorder'));
        l.start();

        if ($("#sltNotes").val().length > 0) {
            $.ajax({
                url: '/Report/SetSelectedNotes',
                type: "POST",
                dataType: 'Json',
                data: {
                    idNotes: JSON.stringify($("#sltNotes").val())
                },
                success: function (data, textStatus, jqXHR) {
                    switch (data.Cod) {
                        case 1:

                            var cnt = 1;
                            var cajaNestable = '[';
                            $("#divNotes").empty();

                            $.each(data.notes, function (indice, item) {
                                var eldiv = '';
                                eldiv += '<li class="dd-item" id="item' + cnt + '" data-id="' + item.id + '">';
                                eldiv += '<div class="dd-handle" title="" id="txtNote' + cnt + '">' + item.option + '</div>';
                                eldiv += '<input type="hidden" name="name" />';
                                eldiv += '</li>';

                                $("#divNotes").append(eldiv);

                                cajaNestable += '{"id":' + item.id + '},';
                                cnt++;
                            });
                            cajaNestable = cajaNestable.substring(0, cajaNestable.length - 1);
                            cajaNestable += ']';
                            $("#nestable2-output").val(cajaNestable);

                            l.stop();

                            $("#rowHome").hide();
                            $("#rowGroup").hide();
                            $("#rowClient").hide();
                            $("#rowOrder").show();
                            $("#rowNotes").hide();
                            $("#back").hide();
                            $("#backnote").show();

                            break;
                        case 2:
                            l.stop();
                            swal("Generate notes", data.Msg, "warning");
                            break;
                        default:
                            l.stop();
                            swal("Generate notes", data.Msg, "error");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    l.stop();
                    swal('Generate notes', 'Error processing the query.', 'error');
                }
            });
        } else {
            l.stop();
            swal("Generate notes", "You must select at least one note.", "warning");
        }

    });

    $("#btngetnotesorder").on("click", function () {

        var l = Ladda.create(document.querySelector('#btngetnotesorder'));
        l.start();

        $.ajax({
            url: '/Report/GetListNotesOrder',
            type: "POST",
            dataType: 'Json',
            data: { orden: $('#nestable-output').val() },
            success: function (data, textStatus, jqXHR) {
                switch (data.Cod) {
                    case 1:
                        swal("Generate notes", "Notes generated successfully, download will start shortly", "success");
                        window.location = '/Report/DownloadNotes?Handler=' + data.Handler;
                        l.stop();
                        break;
                    case 2:
                        l.stop();
                        swal("Generate notes", data.Msg, "warning");
                        break;
                    default:
                        l.stop();
                        swal("Generate notes", data.Msg, "error");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                l.stop();
                swal('Generate notes', 'Error processing the query.', 'error');
            }
        });
    });

});

function getDatesGroup() {

    var l = Ladda.create(document.querySelector('#getreportbygroup'));
    l.start();

    $('#bygroupListDate').empty();

    $.ajax({
        url: '/Report/GetDates',
        type: 'POST',
        dataType: 'Json',
        success: function (data, textStatus, jqXHR) {
            switch (data.Cod) {
                case 1:
                    var lista = data.dates;

                    $.each(lista, function (i, item) {
                        $("#bygroupListDate").append(new Option(item.txtdate, item.date));
                    });
                    l.stop();
                    break;
                case 2:
                    l.stop();
                    $("#bygroupListDate").select2({ placeholder: 'not found result' });
                    swal("Get date groups", data.Msg, "warning");
                    break;
                default:
                    l.stop();
                    $("#bygroupListDate").select2({ placeholder: 'not found result' });
                    swal("Get date groups", data.Msg, "error");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            l.stop();
            $("#bygroupListDate").select2({ placeholder: 'not found result' });
            swal('Get date groups', 'Error processing the query.', 'error');
        }
    });
};

function getGroups() {

    var l = Ladda.create(document.querySelector('#getreportbygroup'));
    l.start();

    $('#bygroupListGroups').empty();

    $.ajax({
        url: '/Report/GetGroups',
        type: 'POST',
        dataType: 'Json',
        success: function (data, textStatus, jqXHR) {
            switch (data.Cod) {
                case 1:
                    var lista = data.groups;

                    $('#bygroupListGroups').append('<option value="" selected disabled>Select an option</option>');

                    $.each(lista, function (i, item) {
                        $("#bygroupListGroups").append(new Option(item.name, item.id));
                    });

                    $("#bygroupListGroups").select2({ placeholder: { id: '-1', text: 'Select an option' } });
                    $('#bygroupListGroups').val(null).trigger('change');

                    l.stop();
                    break;
                case 2:
                    l.stop();
                    $("#bygroupListGroups").select2({ placeholder: 'not found result' });
                    swal("Get groups", data.Msg, "warning");
                    break;
                default:
                    l.stop();
                    $("#bygroupListGroups").select2({ placeholder: 'not found result' });
                    swal("Get groups", data.Msg, "error");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            l.stop();
            $("#bygroupListGroups").select2({ placeholder: 'not found result' });
            swal('Get groups', 'Error processing the query.', 'error');
        }
    });
};

function getClientsByGroup() {

    var l = Ladda.create(document.querySelector('#getreportbygroup'));
    l.start();

    $('#bygroupListClient').empty();

    var groupId = $('#bygroupListGroups').val();

    if (groupId == null) {
        $('#bygroupListClient').select2({ placeholder: { id: '-1', text: 'Select an option from groups' } });
        l.stop();
    }
    else {
        $.ajax({
            url: '/Report/GetClientsByGroup',
            type: "POST",
            dataType: 'Json',
            data: { id: groupId },
            success: function (data, textStatus, jqXHR) {
                switch (data.Cod) {
                    case 1:
                        var lista = data.clients;

                        $.each(lista, function (i, item) {
                            $("#bygroupListClient").append(new Option(item.name, item.id));
                        });

                        $("#bygroupListClient").select2({ placeholder: { id: '-1', text: 'Select an option' } });

                        l.stop();
                        break;
                    case 2:
                        l.stop();
                        $("#bygroupListClient").select2({ placeholder: 'not found result' });
                        swal("Get clients", data.Msg, "warning");
                        break;
                    default:
                        l.stop();
                        $("#bygroupListClient").select2({ placeholder: 'not found result' });
                        swal("Get clients", data.Msg, "error");
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                l.stop();
                $("#bygroupListClient").select2({ placeholder: 'not found result' });
                swal('Get clients', 'Error processing the query.', 'error');
            }
        });
    }
}

function getDatesClient() {

    var l = Ladda.create(document.querySelector('#getreportbyclient'));
    l.start();

    $('#byclientListDate').empty();

    $.ajax({
        url: '/Report/GetDates',
        type: 'POST',
        dataType: 'Json',
        success: function (data, textStatus, jqXHR) {
            switch (data.Cod) {
                case 1:
                    var lista = data.dates;

                    $.each(lista, function (i, item) {
                        $("#byclientListDate").append(new Option(item.txtdate, item.date));
                    });
                    l.stop();
                    break;
                case 2:
                    l.stop();
                    $("#byclientListDate").select2({ placeholder: 'not found result' });
                    swal("Get date clients", data.Msg, "warning");
                    break;
                default:
                    l.stop();
                    $("#byclientListDate").select2({ placeholder: 'not found result' });
                    swal("Get date clients", data.Msg, "error");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            l.stop();
            $("#byclientListDate").select2({ placeholder: 'not found result' });
            swal('Get date clients', 'Error processing the query.', 'error');
        }
    });
};

function getClients() {

    var l = Ladda.create(document.querySelector('#getreportbyclient'));
    l.start();

    $('#byclientListClient').empty();

    $.ajax({
        url: '/Report/GetClients',
        type: 'POST',
        dataType: 'Json',
        success: function (data, textStatus, jqXHR) {
            switch (data.Cod) {
                case 1:
                    var lista = data.clients;


                    $.each(lista, function (i, item) {
                        $("#byclientListClient").append(new Option(item.name, item.id));
                    });

                    $("#byclientListClient").select2({ placeholder: { id: '-1', text: 'Select an option' } });

                    l.stop();
                    break;
                case 2:
                    l.stop();
                    $("#byclientListClient").select2({ placeholder: 'not found result' });
                    swal("Get clients", data.Msg, "warning");
                    break;
                default:
                    l.stop();
                    $("#byclientListClient").select2({ placeholder: 'not found result' });
                    swal("Get clients", data.Msg, "error");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            l.stop();
            $("#byclientListClient").select2({ placeholder: 'not found result' });
            swal('Get clients', 'Error processing the query.', 'error');
        }
    });
};







