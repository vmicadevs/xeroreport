﻿$(document).ready(function () {

    $('#tblCompanies').DataTable({
        paging: true,
        ordering: true,
        info: false,
        pageLength: 10,
        columnDefs: [{
            "targets": [2],
            "orderable": false
        }],
        order: [[1, 'asc'], [0, 'asc']]
    });

    LoadCompanies();

    $("#btnAdd").on("click", function () {
        var validator = $("#frmAdd").validate();
        validator.resetForm();
        $('input').removeClass('error');
        $("#txtName").val("");
        $("#status").val(1);
        $("#mdlAdd").modal('show');
    });

    $("#addcompany").on("click", function () {

        var l = Ladda.create(this);
        l.start();

        if ($("#frmAdd").valid()) {
            $.ajax({
                url: 'AddCompany',
                type: "POST",
                dataType: 'Json',
                data: {
                    name: $("#txtName").val(),
                    status: $("#status").val()
                },
                success: function (data, textStatus, jqXHR) {

                    switch (data.Cod) {
                        case 1:
                            $('#mdlAdd').modal('toggle');
                            l.stop();
                            $("#txtName").val("");
                            $("#status").val(1);
                            swal("Add company", data.Msg, "success");
                            LoadCompanies();
                            break;
                        case 2:
                            l.stop();
                            swal("Add company", data.Msg, "warning");
                            break;
                        default:
                            l.stop();
                            swal("Add company", data.Msg, "error");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    l.stop();
                    swal('Add company', 'Error processing the query.', 'error');
                }
            });
        }
        else {
            l.stop();
        }
    });

    $("#editcompany").on("click", function () {

        var l = Ladda.create(this);
        l.start();

        if ($("#frmEdit").valid()) {

            $.ajax({
                url: 'EditCompany',
                type: "POST",
                dataType: 'Json',
                data: {
                    id: $("#editId").val(),
                    name: $("#editName").val(),
                    status: $("#editStatus").val()
                },
                success: function (data, textStatus, jqXHR) {

                    switch (data.Cod) {
                        case 1:
                            $("#mdlEdit").modal('toggle');
                            l.stop();
                            swal("Edit company", data.Msg, "success");
                            LoadCompanies();
                            break;
                        case 2:
                            l.stop();
                            swal("Edit company", data.Msg, "warning");
                            break;
                        default:
                            l.stop();
                            swal("Edit company", data.Msg, "error");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    l.stop();
                    swal('Edit company', 'Error processing the query.', 'error');
                }
            });
        } else {
            l.stop();
        }
    });
});

function showMdlEdit(id, name, status) {
    var validator = $("#frmEdit").validate();
    validator.resetForm();
    $('input').removeClass('error');
    $("#editId").val(id);
    $("#editName").val(name);
    $("#editStatus").val(status);
    $("#mdlEdit").modal('show');
}

function LoadCompanies() {

    $('#iboxTableCompanies').toggleClass('sk-loading');

    $.ajax({
        url: 'GetListCompanies',
        type: "POST",
        dataType: 'Json',
        success: function (data, textStatus, jqXHR) {
            if (data.Cod == 1) {
                var lista = data.companies;
                if (lista.length == 0) {
                    $('#iboxTableCompanies').toggleClass('sk-loading');
                    swal('Search companies', 'No search results were found.', 'warning');
                }
                else {
                    $('#tblCompanies').DataTable().clear().draw();
                    $.each(lista, function (i, item) {
                        var status = item.status == 1 ? 1 : 0;
                        var txtbtn = item.idCompany + ",'" + item.name + "'," + status;
                        $("#tblCompanies").DataTable().row.add([
                            item.name,
                            item.status == 1 ? '<span class="text-success">Active</span>' : '<span class="text-danger">Inactive</span>',
                            '<div class="text-center"><button type="button" class="btn btn-xs btn-success" onclick="showMdlEdit(' + txtbtn + ')"><i class="fa fa-edit"></i></button></div>'
                        ]);
                    });
                    $("#tblCompanies").DataTable().draw(false);
                    $('#iboxTableCompanies').toggleClass('sk-loading');
                }
            }
            else {
                $('#iboxTableCompanies').toggleClass('sk-loading');
                swal('Search companies', 'Error processing the query, ' + data.Msg, 'error');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('#iboxTableCompanies').toggleClass('sk-loading');
            swal('Search companies', 'Error processing the query.', 'error');
        }
    });
}
