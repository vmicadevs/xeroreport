﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NetStandardApp_net461.Utilities
{
    public static class PayrollClass
    {



        public class Rootobject
        {
            public string Id { get; set; }
            public string Status { get; set; }
            public string ProviderName { get; set; }
            public DateTime DateTimeUTC { get; set; }
            public Payrun[] PayRuns { get; set; }
        }

        public class Payrun
        {
            public string PayRunID { get; set; }
            public string PayrollCalendarID { get; set; }
            public DateTime PayRunPeriodStartDate { get; set; }
            public DateTime PayRunPeriodEndDate { get; set; }
            public DateTime PaymentDate { get; set; }
            public double Wages { get; set; }
            public double Deductions { get; set; }
            public double Tax { get; set; }
            public double Super { get; set; }
            public double Reimbursement { get; set; }
            public double NetPay { get; set; }
            public string PayRunStatus { get; set; }
            public DateTime UpdatedDateUTC { get; set; }

            public Payslip[] Payslips { get; set; }
        }

         public class Payslip
        {
            public string EmployeeID { get; set; }
            public string PayslipID { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public float Wages { get; set; }
            public float Deductions { get; set; }
            public float Tax { get; set; }
            public float Super { get; set; }
            public float Reimbursements { get; set; }
            public float NetPay { get; set; }
            public DateTime UpdatedDateUTC { get; set; }
        }




    }
}