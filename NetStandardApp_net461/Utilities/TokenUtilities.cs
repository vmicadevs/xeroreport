﻿using System;
using System.IO;
using System.Text.Json;
using Xero.NetStandard.OAuth2.Token;


public static class TokenUtilities
{
    public static string StoreToken(XeroOAuth2Token xeroToken , string name)
    {
        try
        {
            string serializedXeroToken = JsonSerializer.Serialize(xeroToken);

            return serializedXeroToken;
        }
        catch (Exception ex)
        {
            throw new Exception("Could not serialize stored token for " + name + " connection.");
        }

    }

    public static XeroOAuth2Token GetStoredToken(string storeToken, string name)
    {
        try
        {
            var xeroToken = JsonSerializer.Deserialize<XeroOAuth2Token>(storeToken);

            return xeroToken;
        }
        catch (Exception ex)
        {
            throw new Exception("It was not possible to obtain the stored token for the " + name + " connection");
        }
    }

}