﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace NetStandardApp_net461.Filters
{
    public class UserLogin : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return (HttpContext.Current.Session["usuario"] != null);
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            //filterContext.Result = new HttpUnauthorizedResult();
            filterContext.Result = new RedirectToRouteResult(
                                    new RouteValueDictionary
                                    {
                                        { "controller", "Error" },
                                        { "action", "Unauthorized" }
                                    });
        }
    }
}