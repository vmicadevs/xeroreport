﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace NetStandardApp_net461.Filters
{
    public class ProfileAuthorization : AuthorizeAttribute
    {
        private Dao.User user { get; set; }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            user = (Dao.User)HttpContext.Current.Session["usuario"];
            HttpContext.Current.Session["idUsuario"] = user.idUser;

            if (user != null)
            {
                if (httpContext.Request.Path.StartsWith("/Administrator") && user.idProfile == 1 ||
                    httpContext.Request.Path.StartsWith("/Manager") && user.idProfile == 2 ||
                    httpContext.Request.Path.StartsWith("/Accountant") && user.idProfile == 3)
                {
                    return true;
                }
            }
            return false;
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectToRouteResult(
                                    new RouteValueDictionary
                                    {
                                        { "controller", "Error" },
                                        { "action", "Unauthorized" }
                                    });
        }

    }
}