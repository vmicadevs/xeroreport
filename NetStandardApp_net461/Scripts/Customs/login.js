﻿$(document).ready(function () {

    $("#usuarioxr").focus();

    $('#msgError').hide();

    Ladda.bind('input[type=button]');

    $("#btnLogin").on("click", function () {
        var l = Ladda.create(this);
        l.start();

        $.ajax({
            url: "Home/Login",
            type: "POST",
            dataType: 'Json',
            data: {
                "user": $("#usuarioxr").val(),
                "pass": $("#passwordxr").val()
            },
            success: function (data, textStatus, jqXHR) {
                if (data.resultado == 1) {
                    window.location.href = data.url;
                }
                else {
                    $('#msgError').show().text(data.mensaje);
                    l.stop();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR + textStatus + errorThrown);
                $('#msgError').show().text("Error al ejecutar la solicitud");
                l.stop();
            }
        });
    });

    $("#passwordxr").keypress(function (e) {
        if (e.which == 13) {
            $("#btnLogin").click();
        }
    });
});