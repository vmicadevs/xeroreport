﻿$(document).ready(function () {

    $('#tblGroups').DataTable({
        paging: true,
        ordering: true,
        info: false,
        pageLength: 10,
        columnDefs: [{
            "targets": [3],
            "orderable": false
        }],
        order: [[2, 'asc'], [0, 'asc']]
    });

    LoadGroups();

    $("#btnAdd").on("click", function () {
        var validator = $("#frmAdd").validate();
        validator.resetForm();
        $('input').removeClass('error');

        $("#name").val("");
        $("#status").val(1);

        $("#mdlAdd").modal('show');
    });

    $("#addgroup").on("click", function () {

        var l = Ladda.create(this);
        l.start();

        if ($("#frmAdd").valid()) {
            $.ajax({
                url: 'AddGroup',
                type: "POST",
                dataType: 'Json',
                data: $("#frmAdd").serialize(),
                success: function (data, textStatus, jqXHR) {

                    switch (data.Cod) {
                        case 1:
                            $('#mdlAdd').modal('toggle');
                            l.stop();
                            swal("Add group", data.Msg, "success");
                            LoadGroups();
                            break;
                        case 2:
                            l.stop();
                            swal("Add group", data.Msg, "warning");
                            break;
                        default:
                            l.stop();
                            swal("Add group", data.Msg, "error");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    l.stop();
                    swal('Add group', 'Error processing the query.', 'error');
                }
            });
        }
        else {
            l.stop();
        }
    });

    $("#editgroup").on("click", function () {

        var l = Ladda.create(this);
        l.start();

        if ($("#frmEdit").valid()) {

            $.ajax({
                url: 'EditGroup',
                type: "POST",
                dataType: 'Json',
                data: {
                    editid: $("#editid").val(),
                    editname: $("#editname").val(),
                    editstatus: $("#editstatus").val()
                },
                success: function (data, textStatus, jqXHR) {
                    switch (data.Cod) {
                        case 1:
                            $("#mdlEdit").modal('toggle');
                            l.stop();
                            swal("Edit group", data.Msg, "success");
                            LoadGroups();
                            break;
                        case 2:
                            l.stop();
                            swal("Edit group", data.Msg, "warning");
                            break;
                        default:
                            l.stop();
                            swal("Edit group", data.Msg, "error");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    l.stop();
                    swal('Edit group', 'Error processing the query.', 'error');
                }
            });
        } else {
            l.stop();
        }
    });

    $('[data-toggle="tooltip"]').tooltip();
});

function showMdlEdit(id, name, clients, status) {

    var validator = $("#frmEdit").validate();
    validator.resetForm();
    $('input').removeClass('error');

    $("#editid").val(id);
    $("#editname").val(name);
    $("#editstatus").val(status);

    $("#mdlEdit").modal('show');
}

function LoadGroups() {

    $('#iboxTableGroups').toggleClass('sk-loading');

    $.ajax({
        url: 'GetListGroups',
        type: "POST",
        dataType: 'Json',
        success: function (data, textStatus, jqXHR) {
            if (data.Cod == 1) {
                var lista = data.groups;
                if (lista.length == 0) {
                    $('#iboxTableGroups').toggleClass('sk-loading');
                    swal('Search groups', 'No search results were found.', 'warning');
                }
                else {
                    $('#tblGroups').DataTable().clear().draw();
                    $.each(lista, function (i, item) {
                        var status = item.status == 1 ? 1 : 0;
                        var txtbtn = item.id + ",'" + item.name + "'," + item.clients + "," + status;

                        $("#tblGroups").DataTable().row.add([
                            item.name,
                            item.clients,
                            item.status == 1 ? '<span class="text-success">Active</span>' : '<span class="text-danger">Inactive</span>',
                            '<div class="text-center"><button type="button" class="btn btn-xs btn-success" onclick="showMdlEdit(' + txtbtn + ')"><i class="fa fa-edit"></i></button></div>'
                        ]);
                    });
                    $("#tblGroups").DataTable().draw(false);
                    $('#iboxTableGroups').toggleClass('sk-loading');
                }
            }
            else {
                $('#iboxTableGroups').toggleClass('sk-loading');
                swal('Search groups', 'Error processing the query, ' + data.Msg, 'error');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('#iboxTableGroups').toggleClass('sk-loading');
            swal('Search groups', 'Error processing the query.', 'error');
        }
    });
}