﻿$(document).ready(function () {

    $('#tblNotes').DataTable({
        paging: true,
        ordering: true,
        info: false,
        pageLength: 10,
        columnDefs: [{
            "targets": [2, 3],
            "orderable": false
        }],
        order: [[1, 'asc']]
    });

    $('#tblSubnotes').DataTable({
        paging: true,
        ordering: true,
        info: false,
        pageLength: 10,
        columnDefs: [{
            "targets": [2],
            "orderable": false
        }],
        order: [[1, 'asc']]
    });

    LoadNotes();

    $("#btnAdd").on("click", function () {
        var validator = $("#frmAdd").validate();
        validator.resetForm();
        $('input').removeClass('error');

        $("#txtnote").val("");
        $("#status").val(1);

        $("#mdlAdd").modal('show');
    });

    $("#showaddsubnote").on("click", function () {
        var validator = $("#frmAddsub").validate();
        validator.resetForm();
        $('input').removeClass('error');

        $("#txtsubnote").val("");
        $("#statussub").val(1);

        $("#mdlsubnotes").modal('hide');
        $("#mdlAddsubnote").modal('show');
    });
    
    $("#closeaddsubnote").on("click", function () {
        $("#mdlAddsubnote").modal('hide');
        $("#mdlsubnotes").modal('show');
    });

    $("#closeeditsubnote").on("click", function () {
        $("#mdlEditSub").modal('hide');
        $("#mdlsubnotes").modal('show');
    });

    $("#addnote").on("click", function () {

        var l = Ladda.create(this);
        l.start();

        if ($("#frmAdd").valid()) {
            $.ajax({
                url: 'AddNote',
                type: "POST",
                dataType: 'Json',
                data: $("#frmAdd").serialize(),
                success: function (data, textStatus, jqXHR) {

                    switch (data.Cod) {
                        case 1:
                            $('#mdlAdd').modal('toggle');
                            l.stop();
                            swal("Add note", data.Msg, "success");
                            LoadNotes();
                            break;
                        case 2:
                            l.stop();
                            swal("Add note", data.Msg, "warning");
                            break;
                        default:
                            l.stop();
                            swal("Add note", data.Msg, "error");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    l.stop();
                    swal('Add note', 'Error processing the query.', 'error');
                }
            });
        }
        else {
            l.stop();
        }
    });

    $("#addsubnote").on("click", function () {

        var l = Ladda.create(this);
        l.start();

        var id = $("#idNote").val();

        if ($("#frmAddsub").valid()) {
            $.ajax({
                url: 'AddSubnote',
                type: "POST",
                dataType: 'Json',
                data: $("#frmAddsub").serialize(),
                success: function (data, textStatus, jqXHR) {

                    switch (data.Cod) {
                        case 1:
                            $('#mdlAddsubnote').modal('toggle');
                            l.stop();
                            swal("Add subnote", data.Msg, "success");
                            LoadSubnotes(id);
                            $("#mdlsubnotes").modal('show');
                            break;
                        case 2:
                            l.stop();
                            swal("Add subnote", data.Msg, "warning");
                            break;
                        default:
                            l.stop();
                            swal("Add subnote", data.Msg, "error");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    l.stop();
                    swal('Add subnote', 'Error processing the query.', 'error');
                }
            });
        }
        else {
            l.stop();
        }
    });

    $("#editnote").on("click", function () {

        var l = Ladda.create(this);
        l.start();

        if ($("#frmEdit").valid()) {

            $.ajax({
                url: 'EditNote',
                type: "POST",
                dataType: 'Json',
                data: {
                    editid: $("#editid").val(),
                    editnote: $("#edittxtnote").val(),
                    editstatus: $("#editstatus").val()
                },
                success: function (data, textStatus, jqXHR) {
                    switch (data.Cod) {
                        case 1:
                            $("#mdlEdit").modal('toggle');
                            l.stop();
                            swal("Edit note", data.Msg, "success");
                            LoadNotes();
                            break;
                        case 2:
                            l.stop();
                            swal("Edit note", data.Msg, "warning");
                            break;
                        default:
                            l.stop();
                            swal("Edit note", data.Msg, "error");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    l.stop();
                    swal('Edit note', 'Error processing the query.', 'error');
                }
            });
        } else {
            l.stop();
        }
    });

    $("#editsubnote").on("click", function () {

        var l = Ladda.create(this);
        l.start();

        var id = $("#idNote").val();

        if ($("#frmEditSub").valid()) {

            $.ajax({
                url: 'EditSubnote',
                type: "POST",
                dataType: 'Json',
                data: {
                    editid: $("#editidsub").val(),
                    editsubnote: $("#edittxtsubnote").val(),
                    editstatus: $("#editstatussub").val()
                },
                success: function (data, textStatus, jqXHR) {
                    switch (data.Cod) {
                        case 1:
                            $('#mdlEditSub').modal('toggle');
                            l.stop();
                            swal("Edit subnote", data.Msg, "success");
                            LoadSubnotes(id);
                            $("#mdlsubnotes").modal('show');
                            break;
                        case 2:
                            l.stop();
                            swal("Edit subnote", data.Msg, "warning");
                            break;
                        default:
                            l.stop();
                            swal("Edit subnote", data.Msg, "error");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    l.stop();
                    swal('Edit subnote', 'Error processing the query.', 'error');
                }
            });
        } else {
            l.stop();
        }
    });

    $('[data-toggle="tooltip"]').tooltip();

});

function showMdlEdit(id, num, txtnote, status) {

    var validator = $("#frmEdit").validate();
    validator.resetForm();
    $('input').removeClass('error');

    $("#editid").val(id);
    $("#editnum").val(num);
    $("#edittxtnote").val(txtnote);
    $("#editstatus").val(status);

    $("#mdlEdit").modal('show');
}

function showMdlSubnotes(id, num, txtnote) {

    $("#titleSubnote").html(num + " " + txtnote);

    $("#idNote").val(id);  

    LoadSubnotes(id);

    $("#mdlsubnotes").modal('show');

}

function showMdlEditsub(id) {

    $.ajax({
        url: 'GetSubnote',
        type: "POST",
        dataType: 'Json',
        data: { idsubnote: id },
        success: function (data, textStatus, jqXHR) {
            if (data.Cod == 1) {

                var validator = $("#frmEditSub").validate();
                validator.resetForm();
                $('input').removeClass('error');

                $("#editidsub").val(id);
                $("#edittxtsubnote").val(data.txtSubnote);
                $("#editstatussub").val(data.statusSubnote);

                $("#mdlsubnotes").modal('hide');
                $("#mdlEditSub").modal('show');
            }
            else {
                swal('Search notes', 'Error processing the query, ' + data.Msg, 'error');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            swal('Search notes', 'Error processing the query.', 'error');
        }
    });
}

function LoadNotes() {

    $('#iboxTableNotes').toggleClass('sk-loading');

    $.ajax({
        url: 'GetListNotes',
        type: "POST",
        dataType: 'Json',
        success: function (data, textStatus, jqXHR) {
            if (data.Cod == 1) {
                var lista = data.notes;
                if (lista.length == 0) {
                    $('#iboxTableNotes').toggleClass('sk-loading');
                    swal('Search notes', 'No search results were found.', 'warning');
                }
                else {
                    $('#tblNotes').DataTable().clear().draw();
                    $.each(lista, function (i, item) {
                        var status = item.status == 1 ? 1 : 0;
                        var txtbtn = item.id + ",'" + item.num + "','" + item.txtnote + "'," + status;
                        var txtbtn2 = item.id + ",'" + item.num + "','" + item.txtnote + "'";

                        $("#tblNotes").DataTable().row.add([
                            item.num + " " + item.txtnote,
                            item.status == 1 ? '<span class="text-success">Active</span>' : '<span class="text-danger">Inactive</span>',
                            '<div class="text-center"><button type="button" class="btn btn-xs btn-success" data-toggle="tooltip" data-placement="top" title="Edit note" onclick="showMdlEdit(' + txtbtn + ')"><i class="fa fa-edit"></i></button></div>',
                            '<div class="text-center"><button type="button" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="top" title="Subnotes" onclick="showMdlSubnotes(' + txtbtn2 + ')"><i class="fa fa-bars"></i></button></div>'
                        ]);
                    });
                    $("#tblNotes").DataTable().draw(false);
                    $('[data-toggle="tooltip"]').tooltip();
                    $('#iboxTableNotes').toggleClass('sk-loading');
                }
            }
            else {
                $('#iboxTableNotes').toggleClass('sk-loading');
                swal('Search notes', 'Error processing the query, ' + data.Msg, 'error');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('#iboxTableNotes').toggleClass('sk-loading');
            swal('Search notes', 'Error processing the query.', 'error');
        }
    });
}

function LoadSubnotes(id) {

    $('#tblSubnotes').DataTable().clear().draw();

    $.ajax({
        url: 'GetListSubnotes',
        type: "POST",
        dataType: 'Json',
        data: { idnote: id },
        success: function (data, textStatus, jqXHR) {
            if (data.Cod == 1) {
                var lista = data.subnotes;
                if (lista.length == 0) {
                    swal('Search notes', 'No search results were found.', 'warning');
                }
                else {
                    $.each(lista, function (i, item) {
                        $("#tblSubnotes").DataTable().row.add([
                            item.txtsubnote,
                            item.status == 1 ? '<span class="text-success">Active</span>' : '<span class="text-danger">Inactive</span>',
                            '<div class="text-center"><button type="button" class="btn btn-xs btn-success" onclick="showMdlEditsub(' + item.id + ')"><i class="fa fa-edit"></i></button></div>'
                        ]);
                    });
                    $("#tblSubnotes").DataTable().draw(false);
                }
            }
            else {
                swal('Search notes', 'Error processing the query, ' + data.Msg, 'error');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            swal('Search notes', 'Error processing the query.', 'error');
        }
    });
}