﻿$(document).ready(function () {

    $('#tblClients').DataTable({
        paging: true,
        ordering: true,
        info: false,
        pageLength: 10,
        columnDefs: [{
            "targets": [3],
            "orderable": false
        }],
        order: [[2, 'asc'], [0, 'asc']]
    });

    LoadClients();

    $("#btnAdd").on("click", function () {
        var validator = $("#frmAdd").validate();
        validator.resetForm();
        $('input').removeClass('error');

        $("#name").val("");

        $("#group").empty();

        $.ajax({
            url: 'GetListCbxGroups',
            type: "POST",
            dataType: 'Json',
            success: function (data, textStatus, jqXHR) {
                var lista = data.groups;

                $('#group').append('<option value="" selected disabled>Select an option</option>');
                $("#group").append(new Option("Unassigned", "0"));

                $.each(lista, function (i, item) {
                    $("#group").append(new Option(item.name, item.id));
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#group').append('<option value="" selected disabled>Select an option</option>');
                $("#group").append(new Option("Unassigned", "0"));
            }
        });

        $("#connection").empty();

        $.ajax({
            url: 'GetListCbxConnections',
            type: "POST",
            dataType: 'Json',
            success: function (data, textStatus, jqXHR) {
                var lista = data.conn;

                $('#connection').append('<option value="" selected disabled>Select an option</option>');
                $("#connection").append(new Option("All connections", "0"));

                $.each(lista, function (i, item) {
                    $("#connection").append(new Option(item.name, item.id));
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#connection').append('<option value="" selected disabled>Select an option</option>');
                $("#connection").append(new Option("All connections", "0"));
            }
        });

        $("#status").val(1);

        $("#mdlAdd").modal('show');
    });

    $("#addclient").on("click", function () {

        var l = Ladda.create(this);
        l.start();

        document.getElementById("closeaddclient").disabled = true; 

        if ($("#frmAdd").valid()) {
            $.ajax({
                url: 'AddClient',
                type: "POST",
                dataType: 'Json',
                data: $("#frmAdd").serialize(),
                success: function (data, textStatus, jqXHR) {
                    switch (data.Cod) {
                        case 1:
                            $('#mdlAdd').modal('toggle');
                            l.stop();                            
                            swal("Add client", data.Msg, "success");
                            document.getElementById("closeaddclient").disabled = false; 
                            LoadClients();
                            break;
                        case 2:
                            l.stop();
                            swal("Add client", data.Msg, "warning");
                            document.getElementById("closeaddclient").disabled = false; 
                            break;
                        default:
                            l.stop();
                            swal("Add client", data.Msg, "error");
                            document.getElementById("closeaddclient").disabled = false; 
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    l.stop();
                    swal('Add client', 'Error processing the query.', 'error');
                    document.getElementById("closeaddclient").disabled = false; 
                }
            });
        }
        else {
            l.stop();
            document.getElementById("closeaddclient").disabled = false; 
        }
    });

    $("#editclient").on("click", function () {

        var l = Ladda.create(this);
        l.start();

        document.getElementById("closeeditclient").disabled = true; 

        if ($("#frmEdit").valid()) {

            $.ajax({
                url: 'EditClient',
                type: "POST",
                dataType: 'Json',
                data: {
                    editid: $("#editid").val(),
                    editname: $("#editname").val(),
                    editgroup: $("#editgroup").val(),
                    editconnection: $("#editconnection").val(),
                    editstatus: $("#editstatus").val()
                },
                success: function (data, textStatus, jqXHR) {
                    switch (data.Cod) {
                        case 1:
                            $("#mdlEdit").modal('toggle');
                            l.stop();
                            swal("Edit client", data.Msg, "success");
                            document.getElementById("closeeditclient").disabled = false; 
                            LoadClients();
                            break;
                        case 2:
                            l.stop();
                            swal("Edit client", data.Msg, "warning");
                            document.getElementById("closeeditclient").disabled = false; 
                            break;
                        default:
                            l.stop();
                            swal("Edit client", data.Msg, "error");
                            document.getElementById("closeeditclient").disabled = false; 
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    l.stop();
                    swal('Edit client', 'Error processing the query.', 'error');
                    document.getElementById("closeeditclient").disabled = false; 
                }
            });
        } else {
            l.stop();
            document.getElementById("closeeditclient").disabled = false; 
        }
    });

    $('[data-toggle="tooltip"]').tooltip();
});

function showMdlEdit(id, name, group, conn, status) {

    var validator = $("#frmEdit").validate();
    validator.resetForm();
    $('input').removeClass('error');

    $("#editid").val(id);
    $("#editname").val(name);
    $("#editstatus").val(status);

    $('#editgroup').empty();

    $.ajax({
        url: 'GetListCbxAllGroups',
        type: "POST",
        dataType: 'Json',
        success: function (data, textStatus, jqXHR) {
            var lista = data.groups;

            $('#editgroup').append('<option value="" disabled>Select an option</option>');
            $("#editgroup").append(new Option("Unassigned", "0"));

            $.each(lista, function (i, item) {
                $("#editgroup").append(new Option(item.name, item.id));
            });

            $("#editgroup").val(group);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('#editgroup').append('<option value="" selected disabled>Select an option</option>');
            $("#editgroup").append(new Option("Unassigned", "0"));
        }
    });

    $('#editconnection').empty();

    $.ajax({
        url: 'GetListCbxAllConnections',
        type: "POST",
        dataType: 'Json',
        success: function (data, textStatus, jqXHR) {
            var lista = data.conn;

            $('#editconnection').append('<option value="" disabled>Select an option</option>');
            $("#editconnection").append(new Option("All connections", "0"));

            $.each(lista, function (i, item) {
                $("#editconnection").append(new Option(item.name, item.id));
            });

            $("#editconnection").val(conn);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('#editconnection').append('<option value="" selected disabled>Select an option</option>');
            $("#editconnection").append(new Option("All connections", "0"));
        }
    });

    $("#mdlEdit").modal('show');
}

function LoadClients() {

    $('#iboxTableClients').toggleClass('sk-loading');

    $.ajax({
        url: 'GetListClients',
        type: "POST",
        dataType: 'Json',
        success: function (data, textStatus, jqXHR) {
            if (data.Cod == 1) {
                var lista = data.clients;
                if (lista.length == 0) {
                    $('#iboxTableClients').toggleClass('sk-loading');
                    swal('Search clients', 'No search results were found.', 'warning');
                }
                else {
                    $('#tblClients').DataTable().clear().draw();
                    $.each(lista, function (i, item) {
                        var status = item.status == 1 ? 1 : 0;
                        var txtbtn = item.id + ",'" + item.name + "'," + item.idGroup + "," + item.idconn + "," + status;

                        $("#tblClients").DataTable().row.add([
                            item.name,
                            item.namegroup,
                            item.status == 1 ? '<span class="text-success">Active</span>' : '<span class="text-danger">Inactive</span>',
                            '<div class="text-center"><button type="button" class="btn btn-xs btn-success" onclick="showMdlEdit(' + txtbtn + ')"><i class="fa fa-edit"></i></button></div>'
                        ]);
                    });
                    $("#tblClients").DataTable().draw(false);
                    $('#iboxTableClients').toggleClass('sk-loading');
                }
            }
            else {
                $('#iboxTableClients').toggleClass('sk-loading');
                swal('Search clients', 'Error processing the query, ' + data.Msg, 'error');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('#iboxTableClients').toggleClass('sk-loading');
            swal('Search clients', 'Error processing the query.', 'error');
        }
    });
}