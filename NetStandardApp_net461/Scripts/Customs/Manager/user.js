﻿$(document).ready(function () {

    $('#tblUsers').DataTable({
        paging: true,
        ordering: true,
        info: false,
        pageLength: 10,
        columnDefs: [{
            "targets": [4],
            "orderable": false
        }],
        order: [[3, 'asc'], [0, 'asc']]
    });

    LoadUsers();

    $("#btnAdd").on("click", function () {
        var validator = $("#frmAdd").validate();
        validator.resetForm();
        $('input').removeClass('error');

        $("#fullname").val("");
        $("#username").val("");
        $("#password").val("");
        $("#status").val(1);

        $("#mdlAdd").modal('show');
    });

    $("#adduser").on("click", function () {

        var l = Ladda.create(this);
        l.start();

        if ($("#frmAdd").valid()) {
            $.ajax({
                url: 'AddUser',
                type: "POST",
                dataType: 'Json',
                data: $("#frmAdd").serialize(),
                success: function (data, textStatus, jqXHR) {

                    switch (data.Cod) {
                        case 1:
                            $('#mdlAdd').modal('toggle');
                            l.stop();
                            swal("Add user", data.Msg, "success");
                            LoadUsers();
                            break;
                        case 2:
                            l.stop();
                            swal("Add user", data.Msg, "warning");
                            break;
                        default:
                            l.stop();
                            swal("Add user", data.Msg, "error");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    l.stop();
                    swal('Add user', 'Error processing the query.', 'error');
                }
            });
        }
        else {
            l.stop();
        }
    });

    $("#edituser").on("click", function () {

        var l = Ladda.create(this);
        l.start();

        if ($("#frmEdit").valid()) {

            $.ajax({
                url: 'EditUser',
                type: "POST",
                dataType: 'Json',
                data: {
                    editid: $("#editid").val(),
                    editfullname: $("#editfullname").val(),
                    editusername: $("#editusername").val(),
                    editstatus: $("#editstatus").val(),
                    editpassword: $("#editpassword").val()
                },
                success: function (data, textStatus, jqXHR) {
                    switch (data.Cod) {
                        case 1:
                            $("#mdlEdit").modal('toggle');
                            l.stop();
                            swal("Edit user", data.Msg, "success");
                            LoadUsers();
                            break;
                        case 2:
                            l.stop();
                            swal("Edit user", data.Msg, "warning");
                            break;
                        default:
                            l.stop();
                            swal("Edit user", data.Msg, "error");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    l.stop();
                    swal('Edit user', 'Error processing the query.', 'error');
                }
            });
        } else {
            l.stop();
        }
    });

    $("#btnChangePassword").on("click", function () {
        $("#editpassword").val('');
        $("#editpassword").removeAttr('disabled');
    });

    $('[data-toggle="tooltip"]').tooltip();
});

function showMdlEdit(id, username, name, status) {

    var validator = $("#frmEdit").validate();
    validator.resetForm();
    $('input').removeClass('error');

    $("#editid").val(id);
    $("#editusername").val(username);
    $("#editpassword").val("editpassword");
    $("#editpassword").attr('disabled', 'disabled');
    $("#editfullname").val(name);
    $("#editstatus").val(status);

    $("#mdlEdit").modal('show');
}

function LoadUsers() {

    $('#iboxTableUsers').toggleClass('sk-loading');

    $.ajax({
        url: 'GetListUsers',
        type: "POST",
        dataType: 'Json',
        success: function (data, textStatus, jqXHR) {
            if (data.Cod == 1) {
                var lista = data.users;
                if (lista.length == 0) {
                    $('#iboxTableUsers').toggleClass('sk-loading');
                    swal('Search users', 'No search results were found.', 'warning');
                }
                else {
                    $('#tblUsers').DataTable().clear().draw();
                    $.each(lista, function (i, item) {
                        var status = item.status == 1 ? 1 : 0;
                        var txtbtn = item.id + ",'" + item.username + "','" + item.name + "'," + status;

                        $("#tblUsers").DataTable().row.add([
                            item.name,
                            item.profile,
                            item.username,
                            item.status == 1 ? '<span class="text-success">Active</span>' : '<span class="text-danger">Inactive</span>',
                            '<div class="text-center"><button type="button" class="btn btn-xs btn-success" onclick="showMdlEdit(' + txtbtn + ')"><i class="fa fa-edit"></i></button></div>'
                        ]);
                    });
                    $("#tblUsers").DataTable().draw(false);
                    $('#iboxTableUsers').toggleClass('sk-loading');
                }
            }
            else {
                $('#iboxTableUsers').toggleClass('sk-loading');
                swal('Search users', 'Error processing the query, ' + data.Msg, 'error');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('#iboxTableUsers').toggleClass('sk-loading');
            swal('Search users', 'Error processing the query.', 'error');
        }
    });
}