﻿$(document).ready(function () {

    $('#tblUsers').DataTable({
        paging: true,
        ordering: true,
        info: false,
        pageLength: 10,
        columnDefs: [{
            "targets": [5],
            "orderable": false
        }],
        order: [[4, 'asc'], [0, 'asc']]
    });

    LoadUsers();
    LoadProfilesAdd();
    LoadCompaniesAdd();
    LoadProfilesEdit();
    LoadCompaniesEdit();

    $("#btnAdd").on("click", function () {
        var validator = $("#frmAdd").validate();
        validator.resetForm();
        $('input').removeClass('error');

        $("#fullname").val("");
        $("#company").val("");
        $("#profile").val("");
        $("#username").val("");
        $("#password").val("");
        $("#status").val(1);

        $("#mdlAdd").modal('show');
    });

    $("#adduser").on("click", function () {

        var l = Ladda.create(this);
        l.start();

        if ($("#frmAdd").valid()) {
            $.ajax({
                url: 'AddUser',
                type: "POST",
                dataType: 'Json',
                data: $("#frmAdd").serialize(),
                success: function (data, textStatus, jqXHR) {

                    switch (data.Cod) {
                        case 1:
                            $('#mdlAdd').modal('toggle');
                            l.stop();
                            swal("Add user", data.Msg, "success");
                            LoadUsers();
                            break;
                        case 2:
                            l.stop();
                            swal("Add user", data.Msg, "warning");
                            break;
                        default:
                            l.stop();
                            swal("Add user", data.Msg, "error");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    l.stop();
                    swal('Add user', 'Error processing the query.', 'error');
                }
            });
        }
        else {

            l.stop();
        }
    });

    $("#edituser").on("click", function () {

        var l = Ladda.create(this);
        l.start();

        if ($("#frmEdit").valid()) {

            $.ajax({
                url: 'EditUser',
                type: "POST",
                dataType: 'Json',
                data: {
                    editid: $("#editid").val(),
                    editfullname: $("#editfullname").val(),
                    editcompany: $("#editcompany").val(),
                    editprofile: $("#editprofile").val(),
                    editusername: $("#editusername").val(),
                    editstatus: $("#editstatus").val(),
                    editpassword: $("#editpassword").val()
                },
                success: function (data, textStatus, jqXHR) {
                    switch (data.Cod) {
                        case 1:
                            $("#mdlEdit").modal('toggle');
                            l.stop();
                            swal("Edit user", data.Msg, "success");
                            LoadUsers();
                            break;
                        case 2:
                            l.stop();
                            swal("Edit user", data.Msg, "warning");
                            break;
                        default:
                            l.stop();
                            swal("Edit user", data.Msg, "error");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    l.stop();
                    swal('Edit user', 'Error processing the query.', 'error');
                }
            });
        } else {

            $('#spanbtneditpw').css('padding-bottom', '25px');

            l.stop();
        }
    });

    $("#btnChangePassword").on("click", function () {
        $("#editpassword").val('');
        $("#editpassword").removeAttr('disabled');
    });

    $('[data-toggle="tooltip"]').tooltip();
});

function showMdlEdit(id, username, company, profile, name, status) {

    var validator = $("#frmEdit").validate();
    validator.resetForm();
    $('input').removeClass('error');

    $("#editid").val(id);
    $("#editusername").val(username);
    $("#editpassword").val("editpassword");
    $("#editpassword").attr('disabled', 'disabled');
    $("#editcompany").val(company);
    $("#editprofile").val(profile);
    $("#editfullname").val(name);
    $("#editstatus").val(status);

    $("#mdlEdit").modal('show');
}

function LoadUsers() {

    $('#iboxTableUsers').toggleClass('sk-loading');

    $.ajax({
        url: 'GetListUsers',
        type: "POST",
        dataType: 'Json',
        success: function (data, textStatus, jqXHR) {
            if (data.Cod == 1) {
                var lista = data.users;
                if (lista.length == 0) {
                    $('#iboxTableUsers').toggleClass('sk-loading');
                    swal('Search users', 'No search results were found.', 'warning');
                }
                else {
                    $('#tblUsers').DataTable().clear().draw();
                    $.each(lista, function (i, item) {
                        var status = item.status == 1 ? 1 : 0;
                        var txtbtn = item.id + ",'" + item.username + "'," + item.idCompany + "," + item.idProfile + ",'" + item.name + "'," + status;

                        $("#tblUsers").DataTable().row.add([
                            item.name,
                            item.company,
                            item.profile,
                            item.username,
                            item.status == 1 ? '<span class="text-success">Active</span>' : '<span class="text-danger">Inactive</span>',
                            '<div class="text-center"><button type="button" class="btn btn-xs btn-success" onclick="showMdlEdit(' + txtbtn + ')"><i class="fa fa-edit"></i></button></div>'
                        ]);
                    });
                    $("#tblUsers").DataTable().draw(false);
                    $('#iboxTableUsers').toggleClass('sk-loading');
                }
            }
            else {
                $('#iboxTableUsers').toggleClass('sk-loading');
                swal('Search users', 'Error processing the query, ' + data.Msg, 'error');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('#iboxTableUsers').toggleClass('sk-loading');
            swal('Search users', 'Error processing the query.', 'error');
        }
    });
}

function LoadProfilesAdd() {

    $("#profile").empty();

    $.ajax({
        url: 'GetListProfilesActive',
        type: "POST",
        dataType: 'Json',
        success: function (data, textStatus, jqXHR) {
            if (data.Cod == 1) {
                $("#profile").append('<option value="" selected disabled>Select profile</option>');
                $.each(data.profiles, function (i, item) {
                    $("#profile").append('<option value="' + item.id + '">' + item.name + '</option>');
                });
            }
            else {
                $("#profile").append('<option value="" selected disabled>error getting profiles</option>');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $("#profile").append('<option value="" selected disabled>error getting profiles</option>');
        }
    });
}

function LoadCompaniesAdd() {

    $("#company").empty();

    $.ajax({
        url: 'GetListCompaniesActive',
        type: "POST",
        dataType: 'Json',
        success: function (data, textStatus, jqXHR) {
            if (data.Cod == 1) {
                $("#company").append('<option value="" selected disabled>Select company</option>');
                $.each(data.companies, function (i, item) {
                    $("#company").append('<option value="' + item.id + '">' + item.name + '</option>');
                });
            }
            else {
                $("#company").append('<option value="" selected disabled>error getting companies</option>');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $("#company").append('<option value="" selected disabled>error getting companies</option>');
        }
    });
}

function LoadProfilesEdit() {

    $("#editprofile").empty();

    $.ajax({
        url: 'GetListProfiles',
        type: "POST",
        dataType: 'Json',
        success: function (data, textStatus, jqXHR) {
            if (data.Cod == 1) {
                $("#editprofile").append('<option value="" disabled>Select profile</option>');
                $.each(data.profiles, function (i, item) {
                    $("#editprofile").append('<option value="' + item.id + '">' + item.name + '</option>');
                });
            }
            else {
                $("#editprofile").append('<option value="" selected disabled>error getting profiles</option>');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $("#editprofile").append('<option value="" selected disabled>error getting profiles</option>');
        }
    });
}

function LoadCompaniesEdit() {

    $("#editcompany").empty();

    $.ajax({
        url: 'GetListCompanies',
        type: "POST",
        dataType: 'Json',
        success: function (data, textStatus, jqXHR) {
            if (data.Cod == 1) {
                $("#editcompany").append('<option value="" disabled>Select company</option>');
                $.each(data.companies, function (i, item) {
                    $("#editcompany").append('<option value="' + item.idCompany + '">' + item.name + '</option>');
                });
            }
            else {
                $("#editcompany").append('<option value="" selected disabled>error getting companies</option>');
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $("#editcompany").append('<option value="" selected disabled>error getting companies</option>');
        }
    });
}