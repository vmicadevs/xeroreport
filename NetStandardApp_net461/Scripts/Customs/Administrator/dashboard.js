﻿$(document).ready(function () {

    $('#listCompanies').select2({
        placeholder: 'Select an option'
    });

    getCompanies();

    $('#listCompanies').change(function () {
        getDashboard();
    });

    $('[data-toggle="tooltip"]').tooltip();

});

function getCompanies() {

    $('#listCompanies').empty();

    $.ajax({
        url: 'GetListCompanies',
        type: 'POST',
        dataType: 'Json',
        success: function (data, textStatus, jqXHR) {
            switch (data.Cod) {
                case 1:
                    var lista = data.companies;

                    $.each(lista, function (i, item) {
                        $("#listCompanies").append(new Option(item.name, item.idCompany));
                    });

                    getDashboard();

                    break;
                case 2:
                    $("#listCompanies").select2({ placeholder: 'not found result' });
                    swal("Get companies", data.Msg, "warning");
                    break;
                default:
                    $("#listCompanies").select2({ placeholder: 'not found result' });
                    swal("Get companies", data.Msg, "error");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $("#listCompanies").select2({ placeholder: 'not found result' });
            swal('Get companies', 'Error processing the query.', 'error');
        }
    });
};

function getDashboard() {

    $.ajax({
        url: 'GetDashboard',
        type: 'POST',
        dataType: 'Json',
        data: { company: $('#listCompanies').val() },
        success: function (data, textStatus, jqXHR) {
            switch (data.Cod) {
                case 1:

                    $('#nameCompany').text(data.name);

                    $('#trd').text(data.trd);
                    $('#tnd').text(data.tnd);

                    $('#ar').text(data.ar);
                    $('#mr').text(data.mr);
                    $('#nr').text(data.nr);
                    $('#mu').text(data.mu);
                    $('#au').text(data.au);

                    $('#tc').text(data.tc);                    
                    $('#tg').text(data.tg);
                    $('#tcl').text(data.tcl);

                    break;
                case 2:
                    swal("Get companies", data.Msg, "warning");
                    break;
                default:
                    swal("Get companies", data.Msg, "error");
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            swal('Get companies', 'Error processing the query.', 'error');
        }
    });
};