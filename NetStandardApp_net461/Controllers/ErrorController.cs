﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NetStandardApp_net461.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult PageNotFound()
        {
            if (Session["usuario"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            Response.StatusCode = 404;
            return View();
        }

        public ActionResult InternalServerError()
        {
            if (Session["usuario"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            Response.StatusCode = 500;
            return View();
        }
        
        public ActionResult Unauthorized()
        {
            if (Session["usuario"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            Response.StatusCode = 401;
            return View();
        }

    }
}