﻿using NetStandardApp_net461.Filters;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Xero.NetStandard.OAuth2.Model;
using Xero.NetStandard.OAuth2.Token;
using Xero.NetStandard.OAuth2.Api;
using Xero.NetStandard.OAuth2.Config;
using Xero.NetStandard.OAuth2.Client;
using System.Net.Http;
using Microsoft.Extensions.DependencyInjection;


namespace NetStandardApp_net461.Controllers
{
    [UserLogin]
    public class ManagerController : Controller
    {
        [ProfileAuthorization]
        [OutputCache(Duration = 0, NoStore = true)]
        public ActionResult Index()
        {
            return RedirectToAction("Report");
        }

        [ProfileAuthorization]
        [OutputCache(Duration = 0, NoStore = true)]
        public ActionResult Report()
        {
            Dao.User user = Session["usuario"] as Dao.User;
            var notes = Dal.Note.GetNotes(user.idCompany).Where(n => n.status.Equals(1)).Select(n => n).ToList();
            ViewBag.notes = notes;

            return View();
        }

        [ProfileAuthorization]
        [OutputCache(Duration = 0, NoStore = true)]
        public ActionResult Notes()
        {
            return View();
        }

        [ProfileAuthorization]
        [OutputCache(Duration = 0, NoStore = true)]
        public ActionResult Groups()
        {
            return View();
        }

        [ProfileAuthorization]
        [OutputCache(Duration = 0, NoStore = true)]
        public ActionResult Clients()
        {
            return View();
        }

        [ProfileAuthorization]
        [OutputCache(Duration = 0, NoStore = true)]
        public ActionResult Connections()
        {
            ViewBag.MsgCarga = Dal.Utils.GetUltimoRegistroLog("addConnection", "Connection");

            return View();
        }

        [ProfileAuthorization]
        [OutputCache(Duration = 0, NoStore = true)]
        public ActionResult Users()
        {
            return View();
        }

        [ProfileAuthorization]
        [OutputCache(Duration = 0, NoStore = true)]
        public ActionResult Client()
        {
            return View();
        }

        #region Users

        [HttpPost]
        public JsonResult GetListUsers()
        {
            try
            {
                Dao.User user = Session["usuario"] as Dao.User;
                if (user == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var users_ = Dal.User.GetUsers();

                var users = (from x in users_
                             where x.idCompany == user.idCompany
                             where x.idProfile == 3
                             select new
                             {
                                 id = x.idUser,
                                 x.username,
                                 x.name,
                                 x.idProfile,
                                 profile = x.Profile.name,
                                 x.status,
                             }).ToList();

                return Json(new { Cod = 1, users });
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult AddUser()
        {
            try
            {
                Dao.User user = Session["usuario"] as Dao.User;
                if (user == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var username = Request.Form["username"].Trim();
                var password = Request.Form["password"].Trim();
                var idCompany = user.idCompany;
                var idProfile = 3;
                var name = Request.Form["fullname"].Trim();
                var status = int.Parse(Request.Form["status"]);

                if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password) || string.IsNullOrEmpty(name))
                {
                    return Json(new { Cod = 2, Msg = "Some values are invalid!" });
                }

                if (Dal.User.ExistUser(username))
                {
                    return Json(new { Cod = 2, Msg = "The username entered is already registered! " });
                }

                if (!Dal.Utils.validPassword(password))
                {
                    return Json(new { Cod = 2, Msg = "The password must have at least one lowercase letter, one uppercase letter, one number, a minimum length of 8 characters and one special character (@, #, $, %, ^, &, +, =, -, *, /, !, (, ), ?, ¡)." });
                }

                if (Dal.User.AddUser(username, password, name, idCompany, idProfile, status, user.idUser) > 0)
                {
                    return Json(new { Cod = 1, Msg = "Record created successfully!" });
                }

                return Json(new { Cod = 3, Msg = "Error creating record." });
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult EditUser()
        {
            try
            {
                Dao.User user = Session["usuario"] as Dao.User;
                if (user == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var id = int.Parse(Request.Form["editid"]);
                var username = Request.Form["editusername"].Trim();
                var password = Request.Form["editpassword"].Trim();
                var idCompany = user.idCompany;
                var idProfile = 3;
                var name = Request.Form["editfullname"].Trim();
                var status = int.Parse(Request.Form["editstatus"]);

                if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password) || string.IsNullOrEmpty(name))
                {
                    return Json(new { Cod = 2, Msg = "Some values are invalid!" });
                }

                if (Dal.User.ExistUser(username, id))
                {
                    return Json(new { Cod = 2, Msg = "The username entered is already registered!" });
                }

                if (!password.Equals("editpassword"))
                {
                    if (!Dal.Utils.validPassword(password))
                    {
                        return Json(new { Cod = 2, Msg = "The password must have at least one lowercase letter, one uppercase letter, one number, a minimum length of 8 characters and one special character (@, #, $, %, ^, &, +, =, -, *, /, !, (, ), ?, ¡)." });
                    }
                }

                var newUser = Dal.User.UpdUser(id, username, password, name, idCompany, idProfile, status, user.idUser);
                if (newUser != null)
                {
                    return Json(new { Cod = 1, Msg = "Record edited successfully!" });
                }

                return Json(new { Cod = 3, Msg = "Error editing the registry." });

            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        #endregion

        #region Groups

        [HttpPost]
        public JsonResult GetListGroups()
        {
            try
            {
                Dao.User user = Session["usuario"] as Dao.User;
                if (user == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var groups_ = Dal.Group.GetGroups(user.idCompany);

                var groups = (from x in groups_
                              select new
                              {
                                  id = x.idGroup,
                                  x.name,
                                  clients = x.Client.Count,
                                  x.status,
                              }).ToList();

                return Json(new { Cod = 1, groups });
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult AddGroup()
        {
            try
            {
                Dao.User user = Session["usuario"] as Dao.User;
                if (user == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var name = Request.Form["name"].Trim();
                var idCompany = user.idCompany;
                var status = int.Parse(Request.Form["status"]);

                if (string.IsNullOrEmpty(name))
                {
                    return Json(new { Cod = 2, Msg = "Some values are invalid!" });
                }

                if (Dal.Group.ExistGroup(name, user.idCompany))
                {
                    return Json(new { Cod = 2, Msg = "The name entered is already registered! " });
                }

                if (Dal.Group.AddGroup(name, idCompany, status, user.idUser) > 0)
                {
                    return Json(new { Cod = 1, Msg = "Record created successfully!" });
                }

                return Json(new { Cod = 3, Msg = "Error creating record." });
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult EditGroup()
        {
            try
            {
                Dao.User user = Session["usuario"] as Dao.User;
                if (user == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var id = int.Parse(Request.Form["editid"]);
                var name = Request.Form["editname"].Trim();
                var idCompany = user.idCompany;
                var status = int.Parse(Request.Form["editstatus"]);

                if (string.IsNullOrEmpty(name))
                {
                    return Json(new { Cod = 2, Msg = "Some values are invalid!" });
                }

                if (Dal.Group.ExistGroup(name, user.idCompany, id))
                {
                    return Json(new { Cod = 2, Msg = "The name entered is already registered!" });
                }

                var newGroup = Dal.Group.UpdGroup(id, idCompany, name, status, user.idUser);
                if (newGroup != null)
                {
                    return Json(new { Cod = 1, Msg = "Record edited successfully!" });
                }

                return Json(new { Cod = 3, Msg = "Error editing the registry." });

            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        #endregion

        #region Connections

        [HttpPost]
        public JsonResult GetListConnections()
        {
            try
            {
                Dao.User user = Session["usuario"] as Dao.User;
                if (user == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var connections_ = Dal.Connection.GetConnections(user.idCompany);

                var connections = (from x in connections_
                                   where x.idCompany == user.idCompany
                                   select new
                                   {
                                       id = x.idConnection,
                                       x.name,
                                       clientid = Dal.Utils.ocultarCaracteres(x.xeroClientId),
                                       clientsecret = Dal.Utils.ocultarCaracteres(x.xeroClientSecret),
                                       x.status,
                                   }).ToList();

                return Json(new { Cod = 1, connections });
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult AddConnection()
        {
            try
            {
                Dao.User user = Session["usuario"] as Dao.User;
                if (user == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var name = Request.Form["name"].Trim();
                var clientid = Request.Form["clientid"].Trim();
                var clientsecret = Request.Form["clientsecret"].Trim();
                var idCompany = user.idCompany;

                if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(clientid) || string.IsNullOrEmpty(clientsecret))
                {
                    return Json(new { Cod = 2, Msg = "Some values are invalid!" });
                }

                if (Dal.Connection.ExistConnection(name, clientid, user.idCompany))
                {
                    return Json(new { Cod = 2, Msg = "The name or client id entered is already registered! " });
                }

                var urlcallback = ConfigurationManager.AppSettings["XeroCallbackUri"];
                var scope = ConfigurationManager.AppSettings["XeroScope"];
                var state = ConfigurationManager.AppSettings["XeroState"];

                var storedtoken = "";

                var id = Dal.Connection.AddConnection(name, idCompany, clientid, clientsecret, urlcallback, scope, state, storedtoken, 3, user.idUser);

                if (id > 0)
                {
                    Session["ic"] = id;
                    Session["cn"] = name;
                    Session["xci"] = clientid;
                    Session["xcs"] = clientsecret;

                    Dal.Utils.Log("addConnection", string.Format("{0: yyyy/MM/dd} | {1}", DateTime.Now, "Start of " + name + " connector validation process in xero."), "Connection");

                    return Json(new { Cod = 1, Msg = "Record created successfully!", url = Url.Action("Index", "Authorization") });
                }

                return Json(new { Cod = 3, Msg = "Error creating record." });
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult ReauthorizeConnection()
        {
            try
            {
                Dao.User user = Session["usuario"] as Dao.User;
                if (user == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var id = int.Parse(Request.Form["id"]);

                var conn = Dal.Connection.GetConnection(id);

                if (conn != null)
                {
                    Session["ic"] = id;
                    Session["cn"] = conn.name;
                    Session["xci"] = conn.xeroClientId;
                    Session["xcs"] = conn.xeroClientSecret;

                    Dal.Utils.Log("addConnection", string.Format("{0: yyyy/MM/dd} | {1}", DateTime.Now, "Start of " + conn.name + " connector validation process in xero."), "Connection");

                    return Json(new { Cod = 1, Msg = "Success", url = Url.Action("Index", "Authorization") });
                }

                return Json(new { Cod = 3, Msg = "Error getting the connector information ." });
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult EditConnection()
        {
            try
            {
                Dao.User user = Session["usuario"] as Dao.User;
                if (user == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var id = int.Parse(Request.Form["editid"]);
                var name = Request.Form["editname"].Trim();
                var clientid = Request.Form["editclientid"].Trim();
                var clientsecret = Request.Form["editclientsecret"].Trim();
                var status = int.Parse(Request.Form["editstatus"]);

                if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(clientid) || string.IsNullOrEmpty(clientsecret))
                {
                    return Json(new { Cod = 2, Msg = "Some values are invalid!" });
                }

                var connection_ = Dal.Connection.GetConnection(id);

                var clientid_ = Dal.Utils.ocultarCaracteres(connection_.xeroClientId);
                var clientsecret_ = Dal.Utils.ocultarCaracteres(connection_.xeroClientSecret);

                if (clientid_.Equals(clientid))
                {
                    clientid = connection_.xeroClientId;
                }

                if (clientsecret_.Equals(clientsecret))
                {
                    clientsecret = connection_.xeroClientSecret;
                }

                if (Dal.Connection.ExistConnection(name, clientid, user.idCompany, id))
                {
                    return Json(new { Cod = 2, Msg = "The name or client id entered is already registered! " });
                }

                var urlcallback = ConfigurationManager.AppSettings["XeroCallbackUri"];
                var scope = ConfigurationManager.AppSettings["XeroScope"];
                var state = ConfigurationManager.AppSettings["XeroState"];

                var storedtoken = "";

                var conn_ = Dal.Connection.UpdConnection(id, name, clientid, clientsecret, urlcallback, scope, state, storedtoken, status, user.idUser);

                if (conn_ != null)
                {
                    if (status != 0)
                    {
                        Session["ic"] = id;
                        Session["cn"] = name;
                        Session["xci"] = clientid;
                        Session["xcs"] = clientsecret;

                        Dal.Utils.Log("addConnection", string.Format("{0: yyyy/MM/dd} | {1}", DateTime.Now, "Start of " + name + " connector validation process in xero."), "Connection");

                        return Json(new { Cod = 1, Msg = "Record edited successfully!", url = Url.Action("Index", "Authorization") });
                    }
                    else
                    {
                        return Json(new { Cod = 1, Msg = "Record edited successfully!", url = "" });
                    }
                }

                return Json(new { Cod = 3, Msg = "Error editing the registry." });

            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        #endregion

        #region Clients

        [HttpPost]
        public JsonResult GetListClients()
        {
            try
            {
                Dao.User user = Session["usuario"] as Dao.User;
                if (user == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var clients_ = Dal.Client.GetClients(user.idCompany);

                var clients = (from c in clients_
                               where c.idCompany == user.idCompany
                               select new
                               {
                                   id = c.idClient,
                                   c.name,
                                   c.idGroup,
                                   namegroup = c.Group == null ? "Unassigned" : c.Group.name,
                                   idconn = c.idConnection == null ? 0 : c.idConnection,
                                   c.status,
                               }).ToList();

                return Json(new { Cod = 1, clients });
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult GetListCbxGroups()
        {
            try
            {
                Dao.User user = Session["usuario"] as Dao.User;
                if (user == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var groups_ = Dal.Group.GetGroups(user.idCompany);

                var groups = (from x in groups_
                              where x.status.Equals(1)
                              select new
                              {
                                  id = x.idGroup,
                                  x.name
                              }).OrderBy(g => g.name).ToList();

                return Json(new { Cod = 1, groups });
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult GetListCbxConnections()
        {
            try
            {
                Dao.User user = Session["usuario"] as Dao.User;
                if (user == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var conn_ = Dal.Connection.GetConnections(user.idCompany);

                var conn = (from x in conn_
                            where x.status.Equals(1)
                            select new
                            {
                                id = x.idConnection,
                                x.name
                            }).OrderBy(c => c.name).ToList();

                return Json(new { Cod = 1, conn });
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult GetListCbxAllGroups()
        {
            try
            {
                Dao.User user = Session["usuario"] as Dao.User;
                if (user == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var groups_ = Dal.Group.GetGroups(user.idCompany);

                var groups = (from x in groups_
                              where x.idCompany.Equals(user.idCompany)
                              select new
                              {
                                  id = x.idGroup,
                                  name = x.name + " [" + (x.status == 1 ? "Active" : "Inactive") + "]"
                              }).OrderBy(g => g.name).ToList();

                return Json(new { Cod = 1, groups });
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult GetListCbxAllConnections()
        {
            try
            {
                Dao.User user = Session["usuario"] as Dao.User;
                if (user == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var conn_ = Dal.Connection.GetConnections(user.idCompany);

                var conn = (from x in conn_
                            select new
                            {
                                id = x.idConnection,
                                name = x.name + " [" + (x.status == 1 ? "Active" : "Inactive") + "]"
                            }).OrderBy(g => g.name).ToList();

                return Json(new { Cod = 1, conn });
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        [HttpPost]
        public async Task<JsonResult> AddClient()
        {
            try
            {
                Dao.User user = Session["usuario"] as Dao.User;
                if (user == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var name = Request.Form["name"].Trim();

                int group = int.Parse(Request.Form["group"]);
                int? idgroup = null;
                if (group != 0) { idgroup = group; }

                int connectionselect = int.Parse(Request.Form["connection"]);
                int? idconnectionselect = null;
                if (connectionselect != 0) { idconnectionselect = connectionselect; }

                var idCompany = user.idCompany;
                var status = int.Parse(Request.Form["status"]);

                if (string.IsNullOrEmpty(name))
                {
                    return Json(new { Cod = 2, Msg = "Some values are invalid!" });
                }

                if (Dal.Client.ExistClient(name, idCompany))
                {
                    return Json(new { Cod = 2, Msg = "The name entered is already registered! " });
                }

                int idConnection = 0;
                string idTenant = "";

                if (idconnectionselect == null)
                {
                    var listConnections = Dal.Connection.GetConnections(user.idCompany).Where(c => c.status == 1).Select(c => c).ToList();

                    foreach (var conn in listConnections)
                    {
                        if (idConnection != 0)
                        {
                            break;
                        }

                        var xeroToken = TokenUtilities.GetStoredToken(conn.storedToken, conn.name);
                        var utcTimeNow = DateTime.UtcNow;

                        var serviceProvider = new ServiceCollection().AddHttpClient().BuildServiceProvider();
                        var httpClientFactory = serviceProvider.GetService<IHttpClientFactory>();

                        XeroConfiguration XeroConfig = new XeroConfiguration
                        {
                            ClientId = conn.xeroClientId,
                            ClientSecret = conn.xeroClientSecret,
                            CallbackUri = new Uri(conn.xeroCallbackUri),
                            Scope = conn.xeroScope,
                            State = conn.xeroState
                        };

                        if (utcTimeNow > xeroToken.ExpiresAtUtc)
                        {
                            var client = new XeroClient(XeroConfig, httpClientFactory);
                            xeroToken = (XeroOAuth2Token)await client.RefreshAccessTokenAsync(xeroToken);

                            var txtXeroToken = TokenUtilities.StoreToken(xeroToken, conn.name);

                            var conn_ = Dal.Connection.UpdConnection(conn.idConnection, conn.name, conn.xeroClientId, conn.xeroClientSecret, conn.xeroCallbackUri, conn.xeroScope, conn.xeroState, txtXeroToken, conn.status, user.idUser);
                            conn.storedToken = conn_.storedToken;
                        }

                        string accessToken = xeroToken.AccessToken;

                        var listTenants = xeroToken.Tenants.ToList();

                        foreach (var tenant in listTenants)
                        {

                            string xeroTenantId = tenant.TenantId.ToString();

                            var AccountingApi = new AccountingApi();
                            var response = await AccountingApi.GetOrganisationsAsync(accessToken, xeroTenantId);

                            var organisation_info = new Organisation();
                            organisation_info = response._Organisations[0];

                            if (organisation_info.Name.Equals(name, StringComparison.OrdinalIgnoreCase))
                            {
                                idConnection = conn.idConnection;
                                idTenant = xeroTenantId;
                                break;
                            }
                        }
                    }

                }
                else
                {

                    var conn = Dal.Connection.GetConnection(idconnectionselect.Value);

                    var xeroToken = TokenUtilities.GetStoredToken(conn.storedToken, conn.name);
                    var utcTimeNow = DateTime.UtcNow;

                    var serviceProvider = new ServiceCollection().AddHttpClient().BuildServiceProvider();
                    var httpClientFactory = serviceProvider.GetService<IHttpClientFactory>();

                    XeroConfiguration XeroConfig = new XeroConfiguration
                    {
                        ClientId = conn.xeroClientId,
                        ClientSecret = conn.xeroClientSecret,
                        CallbackUri = new Uri(conn.xeroCallbackUri),
                        Scope = conn.xeroScope,
                        State = conn.xeroState
                    };

                    if (utcTimeNow > xeroToken.ExpiresAtUtc)
                    {
                        var client = new XeroClient(XeroConfig, httpClientFactory);
                        xeroToken = (XeroOAuth2Token)await client.RefreshAccessTokenAsync(xeroToken);

                        var txtXeroToken = TokenUtilities.StoreToken(xeroToken, conn.name);

                        var conn_ = Dal.Connection.UpdConnection(conn.idConnection, conn.name, conn.xeroClientId, conn.xeroClientSecret, conn.xeroCallbackUri, conn.xeroScope, conn.xeroState, txtXeroToken, conn.status, user.idUser);
                        conn.storedToken = conn_.storedToken;
                    }

                    string accessToken = xeroToken.AccessToken;

                    var listTenants = xeroToken.Tenants.ToList();

                    foreach (var tenant in listTenants)
                    {

                        string xeroTenantId = tenant.TenantId.ToString();

                        var AccountingApi = new AccountingApi();
                        var response = await AccountingApi.GetOrganisationsAsync(accessToken, xeroTenantId);

                        var organisation_info = new Organisation();
                        organisation_info = response._Organisations[0];

                        if (organisation_info.Name.Equals(name, StringComparison.OrdinalIgnoreCase))
                        {
                            idConnection = conn.idConnection;
                            idTenant = xeroTenantId;
                            break;
                        }
                    }


                }


                if (idConnection != 0)
                {
                    if (Dal.Client.AddClient(name, idCompany, idgroup, idConnection, idTenant, status, user.idUser) > 0)
                    {
                        return Json(new { Cod = 1, Msg = "Record created successfully!" });
                    }
                    return Json(new { Cod = 3, Msg = "Error creating record." });
                }
                else
                {
                    return Json(new { Cod = 2, Msg = "Client not found on existing connections to xero." });
                }

            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        [HttpPost]
        public async Task<JsonResult> EditClient()
        {
            try
            {
                Dao.User user = Session["usuario"] as Dao.User;
                if (user == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var idClient = int.Parse(Request.Form["editid"]);
                var name = Request.Form["editname"].Trim();

                int group = int.Parse(Request.Form["editgroup"]);
                int? idgroup = null;
                if (group != 0) { idgroup = group; }

                int connectionselect = int.Parse(Request.Form["editconnection"]);
                int? idconnectionselect = null;
                if (connectionselect != 0) { idconnectionselect = connectionselect; }

                var idCompany = user.idCompany;
                var status = int.Parse(Request.Form["editstatus"]);

                if (string.IsNullOrEmpty(name))
                {
                    return Json(new { Cod = 2, Msg = "Some values are invalid!" });
                }

                if (Dal.Client.ExistClient(name, idCompany, idClient))
                {
                    return Json(new { Cod = 2, Msg = "The name entered is already registered! " });
                }

                int? idConnection = null;
                string idTenant = "";

                if (status == 1)
                {
                    if (idconnectionselect == null)
                    {
                        var listConnections = Dal.Connection.GetConnections(user.idCompany).Where(c => c.status == 1).Select(c => c).ToList();

                        foreach (var conn in listConnections)
                        {
                            var xeroToken = TokenUtilities.GetStoredToken(conn.storedToken, conn.name);
                            var utcTimeNow = DateTime.UtcNow;

                            var serviceProvider = new ServiceCollection().AddHttpClient().BuildServiceProvider();
                            var httpClientFactory = serviceProvider.GetService<IHttpClientFactory>();

                            XeroConfiguration XeroConfig = new XeroConfiguration
                            {
                                ClientId = conn.xeroClientId,
                                ClientSecret = conn.xeroClientSecret,
                                CallbackUri = new Uri(conn.xeroCallbackUri),
                                Scope = conn.xeroScope,
                                State = conn.xeroState
                            };

                            if (utcTimeNow > xeroToken.ExpiresAtUtc)
                            {
                                var client = new XeroClient(XeroConfig, httpClientFactory);
                                xeroToken = (XeroOAuth2Token)await client.RefreshAccessTokenAsync(xeroToken);

                                var txtXeroToken = TokenUtilities.StoreToken(xeroToken, conn.name);

                                var conn_ = Dal.Connection.UpdConnection(conn.idConnection, conn.name, conn.xeroClientId, conn.xeroClientSecret, conn.xeroCallbackUri, conn.xeroScope, conn.xeroState, txtXeroToken, conn.status, user.idUser);
                                conn.storedToken = conn_.storedToken;
                            }

                            string accessToken = xeroToken.AccessToken;

                            var listTenants = xeroToken.Tenants.ToList();

                            foreach (var tenant in listTenants)
                            {

                                string xeroTenantId = tenant.TenantId.ToString();

                                var AccountingApi = new AccountingApi();
                                var response = await AccountingApi.GetOrganisationsAsync(accessToken, xeroTenantId);

                                var organisation_info = new Organisation();
                                organisation_info = response._Organisations[0];

                                if (organisation_info.Name.Equals(name, StringComparison.OrdinalIgnoreCase))
                                {
                                    idConnection = conn.idConnection;
                                    idTenant = xeroTenantId;
                                    break;
                                }
                            }
                        }

                    }
                    else
                    {
                        var conn = Dal.Connection.GetConnection(idconnectionselect.Value);

                        var xeroToken = TokenUtilities.GetStoredToken(conn.storedToken, conn.name);
                        var utcTimeNow = DateTime.UtcNow;

                        var serviceProvider = new ServiceCollection().AddHttpClient().BuildServiceProvider();
                        var httpClientFactory = serviceProvider.GetService<IHttpClientFactory>();

                        XeroConfiguration XeroConfig = new XeroConfiguration
                        {
                            ClientId = conn.xeroClientId,
                            ClientSecret = conn.xeroClientSecret,
                            CallbackUri = new Uri(conn.xeroCallbackUri),
                            Scope = conn.xeroScope,
                            State = conn.xeroState
                        };

                        if (utcTimeNow > xeroToken.ExpiresAtUtc)
                        {
                            var client = new XeroClient(XeroConfig, httpClientFactory);
                            xeroToken = (XeroOAuth2Token)await client.RefreshAccessTokenAsync(xeroToken);

                            var txtXeroToken = TokenUtilities.StoreToken(xeroToken, conn.name);

                            var conn_ = Dal.Connection.UpdConnection(conn.idConnection, conn.name, conn.xeroClientId, conn.xeroClientSecret, conn.xeroCallbackUri, conn.xeroScope, conn.xeroState, txtXeroToken, conn.status, user.idUser);
                            conn.storedToken = conn_.storedToken;
                        }

                        string accessToken = xeroToken.AccessToken;

                        var listTenants = xeroToken.Tenants.ToList();

                        foreach (var tenant in listTenants)
                        {

                            string xeroTenantId = tenant.TenantId.ToString();

                            var AccountingApi = new AccountingApi();
                            var response = await AccountingApi.GetOrganisationsAsync(accessToken, xeroTenantId);

                            var organisation_info = new Organisation();
                            organisation_info = response._Organisations[0];

                            if (organisation_info.Name.Equals(name, StringComparison.OrdinalIgnoreCase))
                            {
                                idConnection = conn.idConnection;
                                idTenant = xeroTenantId;
                                break;
                            }
                        }
                    }

                    if (idConnection != null)
                    {
                        if (Dal.Client.UpdClient(idClient, name, idCompany, idgroup, idConnection, idTenant, status, user.idUser) != null)
                        {
                            return Json(new { Cod = 1, Msg = "Record edited successfully!" });
                        }
                        return Json(new { Cod = 3, Msg = "Error edited record." });
                    }
                    else
                    {
                        return Json(new { Cod = 2, Msg = "Client not found on existing connections to xero." });
                    }

                }
                else
                {
                    if (Dal.Client.UpdClient(idClient, name, idCompany, idgroup, idConnection, idTenant, 0, user.idUser) != null)
                    {
                        return Json(new { Cod = 1, Msg = "Record created successfully!" });
                    }
                    return Json(new { Cod = 3, Msg = "Error creating record." });
                }

            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        #endregion

        #region Notes

        [HttpPost]
        public JsonResult GetListNotes()
        {
            try
            {
                Dao.User user = Session["usuario"] as Dao.User;
                if (user == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var notes_ = Dal.Note.GetNotes(user.idCompany);

                var notes = (from x in notes_
                             select new
                             {
                                 id = x.idNote,
                                 num = x.idNote + ". ",
                                 txtnote = x.txtNote,
                                 x.status,
                             }).OrderBy(o => o.id).ToList();

                return Json(new { Cod = 1, notes });
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult AddNote()
        {
            try
            {
                Dao.User user = Session["usuario"] as Dao.User;
                if (user == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var txtnote = Request.Form["txtnote"].Trim();
                var idCompany = user.idCompany;
                var status = int.Parse(Request.Form["status"]);

                if (string.IsNullOrEmpty(txtnote))
                {
                    return Json(new { Cod = 2, Msg = "Some values are invalid!" });
                }

                if (Dal.Note.ExistNote(txtnote, user.idCompany))
                {
                    return Json(new { Cod = 2, Msg = "The name entered is already registered! " });
                }

                if (Dal.Note.AddNote(txtnote, idCompany, status, user.idUser) > 0)
                {
                    return Json(new { Cod = 1, Msg = "Record created successfully!" });
                }

                return Json(new { Cod = 3, Msg = "Error creating record." });
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult EditNote()
        {
            try
            {
                Dao.User user = Session["usuario"] as Dao.User;
                if (user == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var id = int.Parse(Request.Form["editid"]);
                var txtnote = Request.Form["editnote"].Trim();
                var idCompany = user.idCompany;
                var status = int.Parse(Request.Form["editstatus"]);

                if (string.IsNullOrEmpty(txtnote))
                {
                    return Json(new { Cod = 2, Msg = "Some values are invalid!" });
                }

                if (Dal.Note.ExistNote(txtnote, user.idCompany, id))
                {
                    return Json(new { Cod = 2, Msg = "The note entered is already registered!" });
                }

                var note_ = Dal.Note.UpdNote(id, idCompany, txtnote, status, user.idUser);
                if (note_ != null)
                {
                    return Json(new { Cod = 1, Msg = "Record edited successfully!" });
                }

                return Json(new { Cod = 3, Msg = "Error editing the registry." });

            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        #endregion

        #region Subnotes

        [HttpPost]
        public JsonResult GetListSubnotes()
        {
            try
            {
                Dao.User user = Session["usuario"] as Dao.User;
                if (user == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var idnote = int.Parse(Request.Form["idnote"]);

                var subnotes_ = Dal.Subnote.GetSubnote(idnote);

                var subnotes = (from x in subnotes_
                                select new
                                {
                                    id = x.idSubnote,
                                    txtsubnote = x.txtSubnote,
                                    x.status,
                                }).ToList();

                return Json(new { Cod = 1, subnotes });
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult GetSubnote()
        {
            try
            {
                Dao.User user = Session["usuario"] as Dao.User;
                if (user == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var idsubnote = int.Parse(Request.Form["idsubnote"]);

                var subnotes_ = Dal.Subnote.GetOnlySubnote(idsubnote);

                if (subnotes_ != null)
                {
                    var txtSubnote = subnotes_.txtSubnote;
                    var statusSubnote = subnotes_.status;
                    return Json(new { Cod = 1, txtSubnote, statusSubnote });
                }
                else {
                    return Json(new { Cod = 0, Msg = "Subnote not found in search." });
                }                
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult AddSubnote()
        {
            try
            {
                Dao.User user = Session["usuario"] as Dao.User;
                if (user == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var txtsubnote = Request.Form["txtsubnote"].Trim();
                var idnote = int.Parse(Request.Form["idNote"]);
                var status = int.Parse(Request.Form["statussub"]);

                if (string.IsNullOrEmpty(txtsubnote))
                {
                    return Json(new { Cod = 2, Msg = "Some values are invalid!" });
                }

                if (Dal.Subnote.AddSubnote(txtsubnote, idnote, status, user.idUser) > 0)
                {
                    return Json(new { Cod = 1, Msg = "Record created successfully!" });
                }

                return Json(new { Cod = 3, Msg = "Error creating record." });
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult EditSubnote()
        {
            try
            {
                Dao.User user = Session["usuario"] as Dao.User;
                if (user == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var id = int.Parse(Request.Form["editid"]);
                var txtsubnote = Request.Form["editsubnote"].Trim();
                var status = int.Parse(Request.Form["editstatus"]);

                if (string.IsNullOrEmpty(txtsubnote))
                {
                    return Json(new { Cod = 2, Msg = "Some values are invalid!" });
                }

                var note_ = Dal.Subnote.UpdSubnote(id, txtsubnote, status, user.idUser);
                if (note_ != null)
                {
                    return Json(new { Cod = 1, Msg = "Record edited successfully!" });
                }

                return Json(new { Cod = 3, Msg = "Error editing the registry." });

            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        #endregion
    }
}