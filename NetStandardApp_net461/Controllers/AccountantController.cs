﻿using NetStandardApp_net461.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NetStandardApp_net461.Controllers
{
    [UserLogin]
    public class AccountantController : Controller
    {
        [ProfileAuthorization]
        [OutputCache(Duration = 0, NoStore = true)]
        public ActionResult Index()
        {
            return RedirectToAction("Report");
        }

        [ProfileAuthorization]
        [OutputCache(Duration = 0, NoStore = true)]
        public ActionResult Report()
        {
            Dao.User user = Session["usuario"] as Dao.User;
            var notes = Dal.Note.GetNotes(user.idCompany).Where(n => n.status.Equals(1)).Select(n => n).ToList();
            ViewBag.notes = notes;

            return View();
        }
    }
}