﻿using SpreadsheetLight;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NetStandardApp_net461.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Login()
        {
            try
            {
                var usuario = (Request.Form["user"]);
                var password = (Request.Form["pass"]);

                if (!string.IsNullOrEmpty(usuario) && !string.IsNullOrEmpty(password))
                {
                    //if (Dal.Acceso.Attempts(usuario) > 3)
                    //{
                    //    return Json(new { resultado = 2, mensaje = "Access has been blocked for multiple attempts, contact the system manager to enable your account." });
                    //}
                    var user = Dal.Acceso.GetUsuario(usuario, password);
                    if (user != null)
                    {
                        Session["usuario"] = user;
                        var urlaction = "";

                        switch (user.idProfile)
                        {
                            case 1:
                                urlaction = Url.Action("Index", "Administrator");
                                break;
                            case 2:
                                urlaction = Url.Action("Report", "Manager");
                                break;
                            case 3:
                                urlaction = Url.Action("Report", "Accountant");
                                break;
                            default:
                                urlaction = Url.Action("Redirect", "Home");
                                break;
                        };

                        return Json(new { resultado = 1, mensaje = "Authorized ", url = urlaction });
                    }
                    else
                    {
                        return Json(new { resultado = 2, mensaje = "Invalid username or password." });
                    }
                }
                else
                {
                    return Json(new { resultado = 2, mensaje = "Enter username and/or password." });
                }
            }
            catch (Exception ex)
            {
                return Json(new { resultado = 3, mensaje = ex.Message });
            }
        }

        public ActionResult Logout()
        {
            //Session["usuario"] = null;
            Session.Abandon();
            Session.Clear();
            Session.RemoveAll();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Redirect()
        {
            var user = Session["usuario"] as Dao.User;

            if (user != null)
            {
                switch (user.idProfile)
                {
                    case 1:
                        return RedirectToAction("Dashboard", "Administrator");
                    case 2:
                        return RedirectToAction("Report", "Manager");
                    case 3:
                        return RedirectToAction("Report", "Accounttant");
                    default:
                        return RedirectToAction("Index", "Home");
                };
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

    }
}