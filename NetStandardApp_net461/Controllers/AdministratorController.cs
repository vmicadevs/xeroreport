﻿using NetStandardApp_net461.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NetStandardApp_net461.Controllers
{
    [UserLogin]
    public class AdministratorController : Controller
    {
        // GET: Administrator
        [ProfileAuthorization]
        [OutputCache(Duration = 0, NoStore = true)]
        public ActionResult Index()
        {
            return RedirectToAction("Dashboard");
        }

        [ProfileAuthorization]
        [OutputCache(Duration = 0, NoStore = true)]
        public ActionResult Dashboard()
        {
            return View();
        }

        [ProfileAuthorization]
        [OutputCache(Duration = 0, NoStore = true)]
        public ActionResult Users()
        {
            return View();
        }

        [ProfileAuthorization]
        [OutputCache(Duration = 0, NoStore = true)]
        public ActionResult Companies()
        {
            return View();
        }


        #region Profiles

        [HttpPost]
        public JsonResult GetListProfilesActive()
        {
            try
            {
                if (Session["usuario"] == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var profiles_ = Dal.Profile.GetProfiles();

                var profiles = (from x in profiles_
                                where x.status == 1
                                select new
                                {
                                    id = x.idProfile,
                                    x.name,
                                }).ToList();

                return Json(new { Cod = 1, profiles });

            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult GetListProfiles()
        {
            try
            {
                if (Session["usuario"] == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var profiles_ = Dal.Profile.GetProfiles();

                var profiles = (from x in profiles_
                                select new
                                {
                                    id = x.idProfile,
                                    x.name,
                                }).ToList();

                return Json(new { Cod = 1, profiles });

            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        #endregion

        #region Companies

        [HttpPost]
        public JsonResult GetListCompanies()
        {
            try
            {
                if (Session["usuario"] == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var companies_ = Dal.Company.GetCompanies();

                var companies = (from x in companies_
                                 select new
                                 {
                                     x.idCompany,
                                     x.name,
                                     x.status
                                 }).ToList();

                return Json(new { Cod = 1, companies });

            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult GetListCompaniesActive()
        {
            try
            {
                if (Session["usuario"] == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var companies_ = Dal.Company.GetCompanies();

                var companies = (from x in companies_
                                 where x.status == 1
                                 select new
                                 {
                                     id = x.idCompany,
                                     x.name,
                                 }).ToList();

                return Json(new { Cod = 1, companies });
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult AddCompany()
        {
            try
            {
                var user = Session["usuario"] as Dao.User;
                if (user == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var name = Request.Form["name"].Trim();
                var status = int.Parse(Request.Form["status"]);

                if (Dal.Company.ExistCompany(name))
                {
                    return Json(new { Cod = 2, Msg = "The name entered is already registered! " });
                }

                if (string.IsNullOrEmpty(name))
                {
                    return Json(new { Cod = 2, Msg = "the company name is invalid!" });
                }

                if (Dal.Company.AddCompany(name, status, user.idUser) > 0)
                {
                    return Json(new { Cod = 1, Msg = "Record created successfully!" });
                }

                return Json(new { Cod = 3, Msg = "Error creating record." });
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult EditCompany()
        {
            try
            {
                var user = Session["usuario"] as Dao.User;
                if (user == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var id = int.Parse(Request.Form["id"]);
                var name = Request.Form["name"].Trim();
                var status = int.Parse(Request.Form["status"]);

                if (Dal.Company.ExistCompany(name, id))
                {
                    return Json(new { Cod = 2, Msg = "The name entered is already registered!" });
                }

                if (string.IsNullOrEmpty(name))
                {
                    return Json(new { Cod = 2, Msg = "the company name is invalid!" });
                }

                var company = Dal.Company.UpdCompany(id, name, status, user.idUser);
                if (company != null)
                {
                    return Json(new { Cod = 1, Msg = "Record edited successfully!" });
                }

                return Json(new { Cod = 3, Msg = "Error editing the registry." });

            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        #endregion

        #region Users

        [HttpPost]
        public JsonResult GetListUsers()
        {
            try
            {
                if (Session["usuario"] == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var users_ = Dal.User.GetUsers();

                var users = (from x in users_
                             select new
                             {
                                 id = x.idUser,
                                 x.username,
                                 x.name,
                                 x.idCompany,
                                 company = x.Company.name,
                                 x.idProfile,
                                 profile = x.Profile.name,
                                 x.status,
                             }).ToList();

                return Json(new { Cod = 1, users });
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult AddUser()
        {
            try
            {
                var user = Session["usuario"] as Dao.User;
                if (user == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var username = Request.Form["username"].Trim();
                var password = Request.Form["password"].Trim();
                var idCompany = int.Parse(Request.Form["company"]);
                var idProfile = int.Parse(Request.Form["profile"]);
                var name = Request.Form["fullname"].Trim();
                var status = int.Parse(Request.Form["status"]);

                if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password) || string.IsNullOrEmpty(name))
                {
                    return Json(new { Cod = 2, Msg = "Some values are invalid!" });
                }

                if (Dal.User.ExistUser(username))
                {
                    return Json(new { Cod = 2, Msg = "The username entered is already registered! " });
                }

                if (!Dal.Utils.validPassword(password))
                {
                    return Json(new { Cod = 2, Msg = "The password must have at least one lowercase letter, one uppercase letter, one number, a minimum length of 8 characters and one special character (@, #, $, %, ^, &, +, =, -, *, /, !, (, ), ?, ¡)." });
                }
                
                if (Dal.User.AddUser(username, password, name, idCompany, idProfile, status, user.idUser) > 0)
                {
                    return Json(new { Cod = 1, Msg = "Record created successfully!" });
                }

                return Json(new { Cod = 3, Msg = "Error creating record." });
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult EditUser()
        {
            try
            {
                Dao.User user = Session["usuario"] as Dao.User;
                if (user == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var id = int.Parse(Request.Form["editid"]);
                var username = Request.Form["editusername"].Trim();
                var password = Request.Form["editpassword"].Trim();
                var idCompany = int.Parse(Request.Form["editcompany"]);
                var idProfile = int.Parse(Request.Form["editprofile"]);
                var name = Request.Form["editfullname"].Trim();
                var status = int.Parse(Request.Form["editstatus"]);

                if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password) || string.IsNullOrEmpty(name))
                {
                    return Json(new { Cod = 2, Msg = "Some values are invalid!" });
                }

                if (Dal.User.ExistUser(username, id))
                {
                    return Json(new { Cod = 2, Msg = "The username entered is already registered!" });
                }

                if (!password.Equals("editpassword"))
                {
                    if (!Dal.Utils.validPassword(password))
                    {
                        return Json(new { Cod = 2, Msg = "The password must have at least one lowercase letter, one uppercase letter, one number, a minimum length of 8 characters and one special character (@, #, $, %, ^, &, +, =, -, *, /, !, (, ), ?, ¡)." });
                    }
                }

                var newUser = Dal.User.UpdUser(id, username, password, name, idCompany, idProfile, status, user.idUser);
                if (newUser != null)
                {
                    return Json(new { Cod = 1, Msg = "Record edited successfully!" });
                }

                return Json(new { Cod = 3, Msg = "Error editing the registry." });

            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        #endregion

        #region Dashboard


        [HttpPost]
        public JsonResult GetDashboard()
        {
            try
            {
                if (Session["usuario"] == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var id = int.Parse(Request.Form["company"]);

                var name = Dal.Company.GetCompanyName(id);

                var trd = Dal.Binnacle.GetBinnacles(id, 1).Count;
                var tnd = Dal.Binnacle.GetBinnacles(id, 2).Count;

                double ar = 0;
                var mr = 0;

                var binnacle = Dal.Binnacle.GetBinnacles(id, 1);

                if (binnacle.Count > 0)
                {
                    ar = Math.Round(binnacle.Average(a => a.quantity), 1);
                    mr = binnacle.Max(s => s.quantity);
                }

                var nr = Dal.Note.GetNotes(id).Where(e => e.status.Equals(1)).Select(s => s).ToList().Count;
                var mu = Dal.User.GetUsers().Where(c => c.idCompany.Equals(id)).Where(p => p.idProfile.Equals(2)).Where(e => e.status.Equals(1)).Select(s => s).ToList().Count;
                var au = Dal.User.GetUsers().Where(c => c.idCompany.Equals(id)).Where(p => p.idProfile.Equals(3)).Where(e => e.status.Equals(1)).Select(s => s).ToList().Count;

                var tc = Dal.Connection.GetConnections(id).Where(e => e.status.Equals(1)).Select(s => s).ToList().Count;
                var tg = Dal.Group.GetGroups(id).Where(e => e.status.Equals(1)).Select(s => s).ToList().Count;
                var tcl = Dal.Client.GetClients(id).Where(e => e.status.Equals(1)).Select(s => s).ToList().Count;

                return Json(new { Cod = 1, name, trd, tnd, ar, mr, nr, mu, au, tc, tg, tcl });

            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        #endregion
    }
}