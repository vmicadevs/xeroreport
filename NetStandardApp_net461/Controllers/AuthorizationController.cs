﻿using System;
using System.Configuration;
using System.Web.Mvc;
using System.Net.Http;
using Xero.NetStandard.OAuth2.Client;
using Xero.NetStandard.OAuth2.Config;
using Microsoft.Extensions.DependencyInjection;
using Xero.NetStandard.OAuth2.Token;
using Xero.NetStandard.OAuth2.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using NetStandardApp_net461.Filters;
using System.Text.Json;

namespace NetStandardApp_net461.Controllers
{
    [UserLogin]
    public class AuthorizationController : Controller
    {

        // GET: /Authorization
        public ActionResult Index()
        {
            var clientid = Session["xci"].ToString();
            var clientsecret = Session["xcs"].ToString();

            var serviceProvider = new ServiceCollection().AddHttpClient().BuildServiceProvider();
            var httpClientFactory = serviceProvider.GetService<IHttpClientFactory>();

            XeroConfiguration XeroConfig = new XeroConfiguration
            {
                ClientId = clientid,
                ClientSecret = clientsecret,
                CallbackUri = new Uri(ConfigurationManager.AppSettings["XeroCallbackUri"]),
                Scope = ConfigurationManager.AppSettings["XeroScope"],
                State = ConfigurationManager.AppSettings["XeroState"]
            };

            var client = new XeroClient(XeroConfig, httpClientFactory);

            return Redirect(client.BuildLoginUri());
        }

        // GET: /Authorization/Callback
        public async Task<ActionResult> Callback(string code, string state)
        {
            var idConnection = int.Parse(Session["ic"].ToString());
            var name = Session["cn"].ToString();
            var clientid = Session["xci"].ToString();
            var clientsecret = Session["xcs"].ToString();

            var storetoken = "";
            long userid = 0;

            try
            {
                if (code != null)
                {
                    Dao.User user = Session["usuario"] as Dao.User;
                    if (user == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }

                    userid = user.idUser;

                    var serviceProvider = new ServiceCollection().AddHttpClient().BuildServiceProvider();
                    var httpClientFactory = serviceProvider.GetService<IHttpClientFactory>();

                    XeroConfiguration XeroConfig = new XeroConfiguration
                    {
                        ClientId = clientid,
                        ClientSecret = clientsecret,
                        CallbackUri = new Uri(ConfigurationManager.AppSettings["XeroCallbackUri"]),
                        Scope = ConfigurationManager.AppSettings["XeroScope"],
                        State = ConfigurationManager.AppSettings["XeroState"]
                    };

                    var client = new XeroClient(XeroConfig, httpClientFactory);
                    var xeroToken = (XeroOAuth2Token)await client.RequestXeroTokenAsync(code);

                    storetoken = JsonSerializer.Serialize(xeroToken);

                    var conn = Dal.Connection.UpdConnection(idConnection, storetoken, 1, userid);
                    if (conn != null)
                    {
                        Dal.Utils.Log("addConnection", string.Format("{0: yyyy/MM/dd} | {1}", DateTime.Now, "The data of the " + name + " connection has been validated in xero correctly."), "Connection");
                    }
                    else
                    {
                        Dal.Utils.Log("addConnection", string.Format("{0: yyyy/MM/dd} | {1}", DateTime.Now, "An error occurred while changing the state of the " + name + " connection."), "Connection");
                    }

                    Session["ic"] = 0;
                    Session["cn"] = "";
                    Session["xci"] = "";
                    Session["xcs"] = "";

                    return RedirectToAction("Connections", "Manager");
                }
                else
                {
                    var conn = Dal.Connection.UpdConnection(idConnection, storetoken, 3, userid);

                    Dal.Utils.Log("addConnection", string.Format("{0: yyyy/MM/dd} | {1}", DateTime.Now, "No validation response got from xerox for " + name + " connection."), "Connection");

                    Session["ic"] = 0;
                    Session["cn"] = "";
                    Session["xci"] = "";
                    Session["xcs"] = "";

                    return RedirectToAction("Connections", "Manager");
                }

            }
            catch (Exception ex)
            {
                var conn = Dal.Connection.UpdConnection(idConnection, storetoken, 3, userid);

                Dal.Utils.Log("addConnection", string.Format("{0: yyyy/MM/dd} | {1}", DateTime.Now, "An error occurred in the validation of the " + name + " connection (" + ex.Message + ")."), "Connection");

                Session["ic"] = 0;
                Session["cn"] = "";
                Session["xci"] = "";
                Session["xcs"] = "";

                return RedirectToAction("Connections", "Manager");
            }

        }

    }
}