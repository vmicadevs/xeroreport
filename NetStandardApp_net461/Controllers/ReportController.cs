﻿using SpreadsheetLight;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Threading.Tasks;
using Xero.NetStandard.OAuth2.Token;
using Xero.NetStandard.OAuth2.Api;
using Xero.NetStandard.OAuth2.Config;
using Xero.NetStandard.OAuth2.Client;
using System.Net.Http;
using Microsoft.Extensions.DependencyInjection;
using NetStandardApp_net461.Filters;
using RestSharp;
using DocumentFormat.OpenXml.Spreadsheet;
using Newtonsoft.Json;

namespace NetStandardApp_net461.Controllers
{
    [UserLogin]
    public class ReportController : Controller
    {
        [HttpPost]
        public JsonResult GetDates()
        {
            try
            {
                Dao.User user = Session["usuario"] as Dao.User;
                if (user == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var dates = Dal.Utils.GetDates();

                if (dates != null)
                {
                    return Json(new { Cod = 1, Msg = "", dates });
                }
                else
                {
                    return Json(new { Cod = 2, Msg = "No date ranges found in the search" });
                }

            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult GetGroups()
        {
            try
            {
                Dao.User user = Session["usuario"] as Dao.User;
                if (user == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var groups_ = Dal.Group.GetGroups(user.idCompany);

                if (groups_ != null)
                {
                    var groups = (from x in groups_
                                  where x.status.Equals(1)
                                  select new
                                  {
                                      id = x.idGroup,
                                      x.name
                                  }).OrderBy(x => x.name).ToList();

                    return Json(new { Cod = 1, Msg = "", groups });
                }
                else
                {
                    return Json(new { Cod = 2, Msg = "No groups found in the search" });
                }

            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult GetClientsByGroup()
        {
            try
            {
                Dao.User user = Session["usuario"] as Dao.User;
                if (user == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var id = int.Parse(Request.Form["id"]);

                var clients_ = Dal.Client.GetClients(user.idCompany);

                if (clients_ != null)
                {
                    var clients = (from x in clients_
                                   where x.idGroup.Equals(id)
                                   where x.status.Equals(1)
                                   select new
                                   {
                                       id = x.idClient,
                                       x.name
                                   }).OrderBy(x => x.name).ToList();

                    return Json(new { Cod = 1, Msg = "", clients });
                }
                else
                {
                    return Json(new { Cod = 2, Msg = "No groups found in the search" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult GetClients()
        {
            try
            {
                Dao.User user = Session["usuario"] as Dao.User;
                if (user == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var clients_ = Dal.Client.GetClients(user.idCompany);

                if (clients_ != null)
                {
                    var clients = (from x in clients_
                                   where x.status.Equals(1)
                                   select new
                                   {
                                       id = x.idClient,
                                       x.name
                                   }).OrderBy(x => x.name).ToList();

                    return Json(new { Cod = 1, Msg = "", clients });
                }
                else
                {
                    return Json(new { Cod = 2, Msg = "No groups found in the search" });
                }

            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult GetNotes()
        {
            try
            {
                Dao.User user = Session["usuario"] as Dao.User;
                if (user == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var notes_ = Dal.Note.GetNotes(user.idCompany).Where(n => n.status.Equals(1)).Select(n => n).ToList();

                if (notes_.Count > 0)
                {
                    var notes = (from x in notes_
                                 select new
                                 {
                                     id = x.idNote,
                                     isSelected = x.isSelected == 1 ? 1 : 0,
                                     option = x.idNote + ". " + x.txtNote
                                 }).ToList();

                    return Json(new { Cod = 1, Msg = "", notes });
                }
                else
                {
                    return Json(new { Cod = 2, Msg = "No notes found in the search" });
                }

            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult SetSelectedNotes()
        {
            try
            {
                Dao.User user = Session["usuario"] as Dao.User;
                if (user == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var idNotes = JsonConvert.DeserializeObject<int[]>(Request.Form["idNotes"]);

                if (idNotes.Length > 0)
                {
                    if (Dal.Note.resetSelected(user.idCompany, user.idUser) == 0)
                    {
                        return Json(new { Cod = 3, Msg = "An error occurred while marking the note selection." });
                    }

                    var notes_ = new List<Dao.Note>();
                    for (int n = 0; n < idNotes.Length; n++)
                    {
                        var note = Dal.Note.GetNote(idNotes[n]);

                        if (Dal.Note.UpdNote(note.idNote, 1, user.idUser) == 0)
                        {
                            return Json(new { Cod = 3, Msg = "An error occurred while marking the note selection." });
                        }

                        notes_.Add(note);
                    }

                    var notes = (from x in notes_
                                 select new
                                 {
                                     id = x.idNote,
                                     option = x.idNote + ". " + x.txtNote
                                 }).ToList();


                    return Json(new { Cod = 1, Msg = "", notes });
                }
                else
                {
                    return Json(new { Cod = 2, Msg = "You must select at least one note to generate the file." });
                }

            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult GetListNotes()
        {
            try
            {
                Dao.User user = Session["usuario"] as Dao.User;
                if (user == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                if (Dal.Note.resetSelected(user.idCompany, user.idUser) == 0)
                {
                    return Json(new { Cod = 3, Msg = "An error occurred while marking the note selection." });
                }

                var idNotes = JsonConvert.DeserializeObject<int[]>(Request.Form["idNotes"]);

                if (idNotes.Length > 0)
                {

                    SLDocument sl = new SLDocument();
                    SLPageSettings ps = new SLPageSettings();

                    var actualYear = Dal.Utils.GetYearFiscalYear();
                    var nameWorksheet1 = "NOTES - " + actualYear;

                    sl.RenameWorksheet(SLDocument.DefaultFirstSheetName, nameWorksheet1);
                    ps.TabColor = System.Drawing.Color.Yellow;
                    sl.SetPageSettings(ps, nameWorksheet1);

                    sl.SetCellValue(2, 1, "GROUP");
                    sl.SetCellValue(4, 1, actualYear + " YEAR TAX PLANNING - NOTES");

                    SLStyle style = sl.CreateStyle();
                    style.Font.FontName = "Arial";
                    style.Font.FontSize = 12;
                    style.Font.Bold = true;
                    style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.LightSkyBlue, System.Drawing.Color.LightSkyBlue);
                    sl.SetCellStyle(2, 1, 4, 6, style);
                    sl.MergeWorksheetCells(2, 1, 2, 6);
                    sl.MergeWorksheetCells(3, 1, 3, 6);
                    sl.MergeWorksheetCells(4, 1, 4, 6);

                    style = sl.CreateStyle();
                    style.Alignment.Horizontal = HorizontalAlignmentValues.Center;
                    sl.SetCellStyle(2, 1, 4, 6, style);

                    var row = 6;

                    for (int n = 0; n < idNotes.Length; n++)
                    {
                        var note = Dal.Note.GetNote(idNotes[n]);

                        if (Dal.Note.UpdNote(note.idNote, 1, user.idUser) == 0)
                        {
                            return Json(new { Cod = 3, Msg = "An error occurred while marking the note selection." });
                        }

                        sl.SetCellValue(row, 1, (n + 1));
                        sl.SetCellValue(row, 2, note.txtNote);

                        style = sl.CreateStyle();
                        style.SetFontBold(true);
                        style.Font.FontName = "Arial";
                        style.Font.FontSize = 8;
                        sl.SetCellStyle(row, 1, style);
                        style.SetFontUnderline(UnderlineValues.Single);
                        sl.SetCellStyle(row, 2, style);

                        var subnotes = Dal.Subnote.GetSubnote(note.idNote).Where(s => s.status == 1).Select(x => x).ToList();

                        row = row + 2;

                        foreach (var sub in subnotes)
                        {
                            sl.SetCellValue(row, 2, sub.txtSubnote);

                            style = sl.CreateStyle();
                            style.Font.FontName = "Arial";
                            style.Font.FontSize = 8;
                            style.SetWrapText(true);
                            style.SetVerticalAlignment(VerticalAlignmentValues.Center);
                            sl.SetCellStyle(row, 2, style);

                            row = row + 1;
                        }

                        row = row + 4;
                    }

                    sl.AutoFitColumn(1, 2);
                    sl.AutoFitRow(1, row);

                    if (Dal.Binnacle.AddBinnacle(user.idCompany, 2, idNotes.Length, "note file generation") == 0)
                    {
                        return Json(new { Cod = 2, Msg = "An error has occurred on the log record." });
                    }

                    MemoryStream mr = new MemoryStream();
                    sl.SaveAs(mr);
                    var handler = Guid.NewGuid().ToString();
                    TempData[handler] = mr.GetBuffer();
                    return Json(new { Cod = 1, Msg = "", Handler = handler });
                }
                else
                {
                    return Json(new { Cod = 2, Msg = "You must select at least one note to generate the file." });
                }

            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        private class OrderNotes
        {
            public int id { get; set; }
        }

        [HttpPost]
        public JsonResult GetListNotesOrder()
        {
            try
            {
                Dao.User user = Session["usuario"] as Dao.User;
                if (user == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var orden = Request.Form["orden"];
                var orderNotes = JsonConvert.DeserializeObject<List<OrderNotes>>(orden);

                if (orderNotes.Count > 0)
                {
                    SLDocument sl = new SLDocument();
                    SLPageSettings ps = new SLPageSettings();

                    var actualYear = Dal.Utils.GetYearFiscalYear();

                    var nameWorksheet1 = "NOTES - " + actualYear;

                    sl.RenameWorksheet(SLDocument.DefaultFirstSheetName, nameWorksheet1);
                    ps.TabColor = System.Drawing.Color.Yellow;
                    sl.SetPageSettings(ps, nameWorksheet1);

                    sl.SetCellValue(2, 1, "GROUP");
                    sl.SetCellValue(4, 1, actualYear + " YEAR TAX PLANNING - NOTES");

                    SLStyle style = sl.CreateStyle();
                    style.Font.FontName = "Arial";
                    style.Font.FontSize = 12;
                    style.Font.Bold = true;
                    style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.LightSkyBlue, System.Drawing.Color.LightSkyBlue);
                    sl.SetCellStyle(2, 1, 4, 6, style);
                    sl.MergeWorksheetCells(2, 1, 2, 6);
                    sl.MergeWorksheetCells(3, 1, 3, 6);
                    sl.MergeWorksheetCells(4, 1, 4, 6);

                    style = sl.CreateStyle();
                    style.Alignment.Horizontal = HorizontalAlignmentValues.Center;
                    sl.SetCellStyle(2, 1, 4, 6, style);

                    var row = 6;
                    int n = 0;

                    foreach (var on in orderNotes)
                    {
                        var note = Dal.Note.GetNote(on.id);

                        if (Dal.Note.UpdNote(note.idNote, 1, user.idUser) == 0)
                        {
                            return Json(new { Cod = 3, Msg = "An error occurred while marking the note selection." });
                        }

                        sl.SetCellValue(row, 1, (n + 1));
                        sl.SetCellValue(row, 2, note.txtNote);

                        style = sl.CreateStyle();
                        style.SetFontBold(true);
                        style.Font.FontName = "Arial";
                        style.Font.FontSize = 8;
                        sl.SetCellStyle(row, 1, style);
                        style.SetFontUnderline(UnderlineValues.Single);
                        sl.SetCellStyle(row, 2, style);

                        var subnotes = Dal.Subnote.GetSubnote(note.idNote).Where(s => s.status == 1).Select(x => x).ToList();

                        row = row + 2;

                        foreach (var sub in subnotes)
                        {
                            sl.SetCellValue(row, 2, sub.txtSubnote);

                            style = sl.CreateStyle();
                            style.Font.FontName = "Arial";
                            style.Font.FontSize = 8;
                            style.SetWrapText(true);
                            style.SetVerticalAlignment(VerticalAlignmentValues.Center);
                            sl.SetCellStyle(row, 2, style);

                            row = row + 1;
                        }

                        row = row + 4;
                        n++;
                    }

                    sl.AutoFitColumn(1, 2);
                    sl.AutoFitRow(1, row);

                    if (Dal.Binnacle.AddBinnacle(user.idCompany, 2, orderNotes.Count, "note order file generation") == 0)
                    {
                        return Json(new { Cod = 2, Msg = "An error has occurred on the log record." });
                    }

                    MemoryStream mr = new MemoryStream();
                    sl.SaveAs(mr);
                    var handler = Guid.NewGuid().ToString();
                    TempData[handler] = mr.GetBuffer();
                    return Json(new { Cod = 1, Msg = "", Handler = handler });
                }
                else
                {
                    return Json(new { Cod = 2, Msg = "You must select at least one note to generate the file." });
                }

            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        public virtual ActionResult DownloadNotes(string Handler)
        {
            if (TempData[Handler] != null)
            {
                var nombreArchivo = "Year tax planning - Notes.xlsx";

                byte[] data = TempData[Handler] as byte[];
                return File(data, "application/vnd.ms-excel", nombreArchivo);
            }
            else
            {
                return new EmptyResult();
            }
        }

        private class ApiResponse
        {
            public int index { get; set; }
            public string rowType { get; set; }
            public string title { get; set; }
            public string rowTypeIn { get; set; }
            public string label { get; set; }
            public string actualValue { get; set; }
            public string pastValue { get; set; }
        }
        private class ApiResponseBase
        {
            public int index { get; set; }
            public string rowType { get; set; }
            public string title { get; set; }
            public string rowTypeIn { get; set; }
            public string label { get; set; }
            public string value { get; set; }
        }
        private class ApiResponsePl
        {
            public int index { get; set; }
            public string rowType { get; set; }
            public string title { get; set; }
            public string rowTypeIn { get; set; }
            public string label { get; set; }
            public string july { get; set; }
            public string august { get; set; }
            public string september { get; set; }
            public string october { get; set; }
            public string november { get; set; }
            public string december { get; set; }
            public string january { get; set; }
            public string february { get; set; }
            public string march { get; set; }
            public string april { get; set; }
            public string may { get; set; }
            public string june { get; set; }
            public string fullyear { get; set; }
        }

        [HttpPost]
        public async Task<JsonResult> GetReport()
        {
            try
            {
                Dao.User user = Session["usuario"] as Dao.User;
                if (user == null)
                {
                    return Json(new { Cod = 0, Msg = "Access denied" });
                }

                var dateid = Request.Form["dateid"];
                var groupid = int.Parse(Request.Form["groupid"]);
                var _clientid = Request.Form["clientid[]"];

                char[] delimiterChars = { ' ', ',', '.', ':', '-', '\n', '\t' };
                var clientids = _clientid.Split(delimiterChars);

                var groupName = "Custom";

                if (groupid != 0)
                {
                    var group = Dal.Group.GetGroup(groupid);
                    if (group.idCompany != user.idCompany)
                    {
                        return Json(new { Cod = 0, Msg = "Access denied" });
                    }
                    groupName = group != null ? group.name : "";
                }

                var fiscal = Dal.Utils.GetFiscalYear(dateid);
                var datePeriods = Dal.Utils.GetDatePeriods(dateid);

                System.Drawing.Color[] colors = {
                    System.Drawing.Color.Yellow,
                    System.Drawing.Color.Orchid,
                    System.Drawing.Color.SpringGreen,
                    System.Drawing.Color.DeepSkyBlue,
                    System.Drawing.Color.Orange,
                    System.Drawing.Color.Gainsboro,
                    System.Drawing.Color.PaleVioletRed,
                    System.Drawing.Color.MediumSpringGreen,
                    System.Drawing.Color.Aqua,
                    System.Drawing.Color.Tomato,
                    System.Drawing.Color.PowderBlue,
                    System.Drawing.Color.GreenYellow,
                    System.Drawing.Color.DarkTurquoise,
                    System.Drawing.Color.LightCoral,
                    System.Drawing.Color.LightSkyBlue,
                    System.Drawing.Color.Goldenrod,
                    System.Drawing.Color.LightGray,
                    System.Drawing.Color.Violet,
                    System.Drawing.Color.SteelBlue,
                    System.Drawing.Color.Aquamarine,
                    System.Drawing.Color.Orchid,
                    System.Drawing.Color.SpringGreen,
                    System.Drawing.Color.DeepSkyBlue,
                    System.Drawing.Color.Orange,
                    System.Drawing.Color.Gainsboro,
                    System.Drawing.Color.PaleVioletRed,
                    System.Drawing.Color.MediumSpringGreen,
                    System.Drawing.Color.Aqua,
                    System.Drawing.Color.Tomato,
                    System.Drawing.Color.PowderBlue,
                    System.Drawing.Color.GreenYellow,
                    System.Drawing.Color.DarkTurquoise,
                    System.Drawing.Color.LightCoral,
                    System.Drawing.Color.LightSkyBlue,
                    System.Drawing.Color.Goldenrod,
                    System.Drawing.Color.LightGray,
                    System.Drawing.Color.Violet,
                    System.Drawing.Color.SteelBlue,
                    System.Drawing.Color.Aquamarine,
                    System.Drawing.Color.Orchid,
                    System.Drawing.Color.SpringGreen,
                    System.Drawing.Color.DeepSkyBlue,
                    System.Drawing.Color.Orange,
                    System.Drawing.Color.Gainsboro,
                    System.Drawing.Color.PaleVioletRed,
                    System.Drawing.Color.MediumSpringGreen,
                    System.Drawing.Color.Aqua,
                    System.Drawing.Color.Tomato,
                    System.Drawing.Color.PowderBlue,
                    System.Drawing.Color.GreenYellow,
                    System.Drawing.Color.DarkTurquoise,
                    System.Drawing.Color.LightCoral,
                    System.Drawing.Color.LightSkyBlue,
                    System.Drawing.Color.Goldenrod,
                    System.Drawing.Color.LightGray,
                    System.Drawing.Color.Violet,
                    System.Drawing.Color.SteelBlue,
                    System.Drawing.Color.Aquamarine
                };

                var txtyear = fiscal.yearToShow;

                SLDocument sl = new SLDocument();
                SLPageSettings ps = new SLPageSettings();
                SLStyle style = sl.CreateStyle();

                //// Worksheet 1
                //var nameWorksheet1 = "9- TO DO LIST - " + txtyear;
                //sl.RenameWorksheet(SLDocument.DefaultFirstSheetName, nameWorksheet1);
                //ps.TabColor = System.Drawing.Color.Gray;
                //sl.SetPageSettings(ps, nameWorksheet1);

                //// Worksheet 2
                //var nameWorksheet2 = "5- Other Information";
                //sl.AddWorksheet(nameWorksheet2);
                //ps.TabColor = System.Drawing.Color.Gray;
                //sl.SetPageSettings(ps, nameWorksheet2);

                //// Worksheet 3
                //var nameWorksheet3 = "10- Cover Page";
                //sl.AddWorksheet(nameWorksheet3);
                //ps.TabColor = colors[0];
                //sl.SetPageSettings(ps, nameWorksheet3);

                //// Worksheet 4
                //var nameWorksheet4 = "11- Disclaimer";
                //sl.AddWorksheet(nameWorksheet4);
                //ps.TabColor = colors[0];
                //sl.SetPageSettings(ps, nameWorksheet4);

                //// Worksheet 5
                //var nameWorksheet5 = "8- NOTES - " + txtyear;
                //sl.AddWorksheet(nameWorksheet5);
                //ps.TabColor = colors[0];
                //sl.SetPageSettings(ps, nameWorksheet5);

                //// Worksheet 6
                //var nameWorksheet6 = "6- Tax Estimate Summary " + (txtyear - 1);
                //sl.AddWorksheet(nameWorksheet6);
                //ps.TabColor = colors[0];
                //sl.SetPageSettings(ps, nameWorksheet6);


                ////Worksheet 7
                //var nameWorksheet7 = "7- Tax Instalment Summary " + txtyear;
                //sl.AddWorksheet(nameWorksheet7);
                //ps.TabColor = colors[0];
                //sl.SetPageSettings(ps, nameWorksheet7);

                var nameWorksheetNotes = "NOTES - " + txtyear;
                sl.RenameWorksheet(SLDocument.DefaultFirstSheetName, nameWorksheetNotes);
                ps.TabColor = colors[0];
                sl.SetPageSettings(ps, nameWorksheetNotes);

                sl.SetCellValue(2, 1, groupName.ToUpper() + " GROUP");
                sl.SetCellValue(4, 1, txtyear + " YEAR TAX PLANNING - NOTES");

                style = sl.CreateStyle();
                style.Font.FontName = "Arial";
                style.Font.FontSize = 12;
                style.Font.Bold = true;
                style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.LightSkyBlue, System.Drawing.Color.LightSkyBlue);
                sl.SetCellStyle(2, 1, 4, 6, style);
                sl.MergeWorksheetCells(2, 1, 2, 6);
                sl.MergeWorksheetCells(3, 1, 3, 6);
                sl.MergeWorksheetCells(4, 1, 4, 6);

                style = sl.CreateStyle();
                style.Alignment.Horizontal = HorizontalAlignmentValues.Center;
                sl.SetCellStyle(2, 1, 4, 6, style);

                int position = 1;
                foreach (var idclient in clientids)
                {
                    var dataClient = Dal.Client.GetClient(int.Parse(idclient));

                    if (dataClient.idCompany != user.idCompany)
                    {
                        return Json(new { Cod = 0, Msg = "Access denied" });
                    }

                    // Worksheet company
                    var nameWorksheetclient = "4 CO. - " + dataClient.idClient + "-" + Dal.Utils.quitarCaracteres(dataClient.name) + "  PL " + txtyear;
                    sl.AddWorksheet(nameWorksheetclient);
                    ps.TabColor = colors[0];
                    sl.SetPageSettings(ps, nameWorksheetclient);

                    //------ get report P&L actual v/s past year -----------

                    //get report P&L past year
                    var conn = Dal.Connection.GetConnection(dataClient.idConnection.Value);
                    var xeroToken = TokenUtilities.GetStoredToken(conn.storedToken, conn.name);
                    var utcTimeNow = DateTime.UtcNow;
                    var serviceProvider = new ServiceCollection().AddHttpClient().BuildServiceProvider();
                    var httpClientFactory = serviceProvider.GetService<IHttpClientFactory>();
                    XeroConfiguration XeroConfig = new XeroConfiguration
                    {
                        ClientId = conn.xeroClientId,
                        ClientSecret = conn.xeroClientSecret,
                        CallbackUri = new Uri(conn.xeroCallbackUri),
                        Scope = conn.xeroScope,
                        State = conn.xeroState
                    };
                    if (utcTimeNow > xeroToken.ExpiresAtUtc)
                    {
                        var client = new XeroClient(XeroConfig, httpClientFactory);
                        xeroToken = (XeroOAuth2Token)await client.RefreshAccessTokenAsync(xeroToken);

                        var txtXeroToken = TokenUtilities.StoreToken(xeroToken, conn.name);

                        var conn_ = Dal.Connection.UpdConnection(conn.idConnection, conn.name, conn.xeroClientId, conn.xeroClientSecret, conn.xeroCallbackUri, conn.xeroScope, conn.xeroState, txtXeroToken, conn.status, user.idUser);
                        conn.storedToken = conn_.storedToken;

                    }
                    string accessToken = xeroToken.AccessToken;
                    string xeroTenantId = xeroToken.Tenants.ToList().Where(t => t.TenantId.ToString() == dataClient.idTenant.ToString()).Select(t => t.TenantId.ToString()).FirstOrDefault();
                    if (string.IsNullOrEmpty(xeroTenantId))
                    {
                        return Json(new { Cod = 0, Msg = "Could not get the " + dataClient.name + " client information" });
                    }
                    var AccountingApi = new AccountingApi();

                    var dt1 = fiscal.startFiscalYear;
                    var dt2 = fiscal.endFiscalYear;
                    var response = await AccountingApi.GetReportProfitAndLossAsync(accessToken, xeroTenantId, dt1, dt2, 1, "YEAR", null, null, null, null, true, null);
                    var reportPandLActual = response.Reports[0];

                    var listaPandL = new List<ApiResponse>();

                    if (reportPandLActual != null)
                    {
                        var index = 1;
                        foreach (var f in reportPandLActual.Rows)
                        {
                            if (f.RowType.ToString() == "Header")
                            {
                                var apiResponse = new ApiResponse();
                                apiResponse.index = index;
                                apiResponse.rowType = "Header";
                                apiResponse.pastValue = f.Cells[2].Value;
                                listaPandL.Add(apiResponse);
                            }
                            else
                            {
                                if (f.Rows.Count != 0)
                                {
                                    foreach (var r in f.Rows)
                                    {
                                        var apiResponse = new ApiResponse();
                                        apiResponse.index = index;
                                        apiResponse.rowType = f.RowType.ToString();
                                        apiResponse.title = f.Title.ToString();
                                        if (r.Cells != null)
                                        {
                                            apiResponse.rowTypeIn = r.RowType.ToString();
                                            apiResponse.label = r.Cells[0].Value;
                                            apiResponse.pastValue = r.Cells[2].Value;
                                        }
                                        listaPandL.Add(apiResponse);
                                    }
                                }
                            }
                            index++;
                        }
                    }

                    //get report P&L actual
                    conn = Dal.Connection.GetConnection(dataClient.idConnection.Value);
                    xeroToken = TokenUtilities.GetStoredToken(conn.storedToken, conn.name);
                    utcTimeNow = DateTime.UtcNow;
                    serviceProvider = new ServiceCollection().AddHttpClient().BuildServiceProvider();
                    httpClientFactory = serviceProvider.GetService<IHttpClientFactory>();
                    XeroConfig = new XeroConfiguration
                    {
                        ClientId = conn.xeroClientId,
                        ClientSecret = conn.xeroClientSecret,
                        CallbackUri = new Uri(conn.xeroCallbackUri),
                        Scope = conn.xeroScope,
                        State = conn.xeroState
                    };
                    if (utcTimeNow > xeroToken.ExpiresAtUtc)
                    {
                        var client = new XeroClient(XeroConfig, httpClientFactory);
                        xeroToken = (XeroOAuth2Token)await client.RefreshAccessTokenAsync(xeroToken);

                        var txtXeroToken = TokenUtilities.StoreToken(xeroToken, conn.name);

                        var conn_ = Dal.Connection.UpdConnection(conn.idConnection, conn.name, conn.xeroClientId, conn.xeroClientSecret, conn.xeroCallbackUri, conn.xeroScope, conn.xeroState, txtXeroToken, conn.status, user.idUser);
                        conn.storedToken = conn_.storedToken;
                    }
                    accessToken = xeroToken.AccessToken;
                    xeroTenantId = xeroToken.Tenants.ToList().Where(t => t.TenantId.ToString() == dataClient.idTenant.ToString()).Select(t => t.TenantId.ToString()).FirstOrDefault();
                    if (string.IsNullOrEmpty(xeroTenantId))
                    {
                        return Json(new { Cod = 0, Msg = "Could not get the " + dataClient.name + " client information" });
                    }
                    AccountingApi = new AccountingApi();

                    dt1 = fiscal.startFiscalYear;
                    dt2 = fiscal.endReportFiscalYear;
                    response = await AccountingApi.GetReportProfitAndLossAsync(accessToken, xeroTenantId, dt1, dt2, null, null, null, null, null, null, true, null);
                    var reportPandLPast = response.Reports[0];

                    var listaPandL2 = new List<ApiResponse>();

                    if (reportPandLPast != null)
                    {
                        var index = 1;
                        foreach (var f in reportPandLPast.Rows)
                        {
                            if (f.RowType.ToString() == "Header")
                            {
                                var apiResponse = new ApiResponse();
                                apiResponse.index = index;
                                apiResponse.rowType = "Header";
                                apiResponse.label = f.Cells[0].Value;
                                apiResponse.actualValue = f.Cells[1].Value;
                                listaPandL2.Add(apiResponse);
                            }
                            else
                            {
                                if (f.Rows.Count != 0)
                                {
                                    foreach (var r in f.Rows)
                                    {
                                        var apiResponse = new ApiResponse();
                                        apiResponse.index = index;
                                        apiResponse.rowType = f.RowType.ToString();
                                        apiResponse.title = f.Title.ToString();
                                        if (r.Cells != null)
                                        {
                                            apiResponse.rowTypeIn = r.RowType.ToString();
                                            apiResponse.label = r.Cells[0].Value;
                                            apiResponse.actualValue = r.Cells[1].Value;
                                        }
                                        listaPandL2.Add(apiResponse);
                                    }
                                }
                            }
                            index++;
                        }
                    }

                    foreach (var pl in listaPandL)
                    {
                        pl.actualValue = "0";
                        foreach (var pl2 in listaPandL2)
                        {
                            if (pl.rowType == pl2.rowType && pl.title == pl2.title && pl.rowTypeIn == pl2.rowTypeIn && pl.label == pl2.label)
                            {
                                pl.actualValue = pl2.actualValue;
                            }
                        }
                    }

                    var listPandLFinal = listaPandL;

                    var plStartIncome = 0;
                    var plEndTotalIncome = 0;
                    var plStartLessCostofSales = 0;
                    var plEndTotalCostofSales = 0;
                    var plStarPlusOtherIncome = 0;
                    var plEndTotalOtherIncome = 0;
                    var plGrossProfit = 0;
                    var plStartLessOperatingExpenses = 0;
                    var plEndTotalOperatingExpenses = 0;

                    if (listPandLFinal != null)
                    {

                        sl.SetCellValue(1, 1, " ");
                        sl.SetCellValue(2, 1, dataClient.name);
                        sl.SetCellValue(3, 1, "Estimated Tax Position for the Year Ended " + fiscal.endFiscalYear.ToString("dd MMMM yyyy"));

                        sl.MergeWorksheetCells(1, 1, 1, 6);
                        sl.MergeWorksheetCells(2, 1, 2, 6);
                        sl.MergeWorksheetCells(3, 1, 3, 6);

                        sl.SetCellValue(5, 2, fiscal.startFiscalYear.ToString("dd/MM/yyyy") + " - " + fiscal.endReportFiscalYear.ToString("dd/MM/yyyy"));
                        sl.SetCellValue(5, 3, fiscal.endReportFiscalYear.AddDays(1).ToString("dd/MM/yyyy") + " - " + fiscal.endFiscalYear.ToString("dd/MM/yyyy"));
                        sl.SetCellValue(5, 4, "EOY " + fiscal.endFiscalYear.ToString("dd/MM/yyyy"));
                        sl.SetCellValue(5, 6, "EOY " + fiscal.endFiscalYear.AddYears(-1).ToString("dd/MM/yyyy"));

                        sl.SetCellValue(6, 2, "Actual Results");
                        sl.SetCellValue(6, 3, "Estimated Results");
                        sl.SetCellValue(6, 4, "Estimated Results");
                        sl.SetCellValue(6, 6, "Actual Results");

                        style = sl.CreateStyle();
                        style.Font.Bold = true;
                        style.Alignment.Horizontal = HorizontalAlignmentValues.Center;
                        sl.SetCellStyle(1, 1, 6, 6, style);

                        style = sl.CreateStyle();
                        style.Border.BottomBorder.BorderStyle = BorderStyleValues.Medium;
                        sl.SetCellStyle(7, 2, 7, 6, style);

                        int fila = 9;
                        var index = 1;
                        foreach (var pl in listPandLFinal)
                        {
                            if (!string.IsNullOrEmpty(pl.label))
                            {
                                if (index != pl.index)
                                {
                                    if (!string.IsNullOrEmpty(pl.title))
                                    {
                                        sl.SetCellValue(fila, 1, pl.title);

                                        style = sl.CreateStyle();
                                        style.Font.Bold = true;
                                        sl.SetCellStyle(fila, 1, style);

                                        switch (pl.title)
                                        {
                                            case "Income":
                                                plStartIncome = fila;
                                                break;
                                            case "Less Cost of Sales":
                                                plStartLessCostofSales = fila;
                                                break;
                                            case "Plus Other Income":
                                                plStarPlusOtherIncome = fila;
                                                break;
                                            case "Less Operating Expenses":
                                                plStartLessOperatingExpenses = fila;
                                                break;
                                            default:
                                                break;
                                        }

                                        fila++;
                                    }
                                }

                                switch (pl.label)
                                {
                                    case "Total Income":
                                        plEndTotalIncome = fila;
                                        sl.SetCellValue(fila, 2, "=SUM(B" + (plStartIncome + 1) + ":B" + (plEndTotalIncome - 1) + ")");
                                        sl.SetCellValue(fila, 3, "=SUM(C" + (plStartIncome + 1) + ":C" + (plEndTotalIncome - 1) + ")");
                                        sl.SetCellValue(fila, 4, "=SUM(D" + (plStartIncome + 1) + ":D" + (plEndTotalIncome - 1) + ")");
                                        sl.SetCellValue(fila, 6, "=SUM(F" + (plStartIncome + 1) + ":F" + (plEndTotalIncome - 1) + ")");
                                        break;
                                    case "Total Cost of Sales":
                                        plEndTotalCostofSales = fila;
                                        sl.SetCellValue(fila, 2, "=SUM(B" + (plStartLessCostofSales + 1) + ":B" + (plEndTotalCostofSales - 1) + ")");
                                        sl.SetCellValue(fila, 3, "=SUM(C" + (plStartLessCostofSales + 1) + ":C" + (plEndTotalCostofSales - 1) + ")");
                                        sl.SetCellValue(fila, 4, "=SUM(D" + (plStartLessCostofSales + 1) + ":D" + (plEndTotalCostofSales - 1) + ")");
                                        sl.SetCellValue(fila, 6, "=SUM(F" + (plStartLessCostofSales + 1) + ":F" + (plEndTotalCostofSales - 1) + ")");
                                        break;
                                    case "Total Other Income":
                                        plEndTotalOtherIncome = fila;
                                        sl.SetCellValue(fila, 2, "=SUM(B" + (plStarPlusOtherIncome + 1) + ":B" + (plEndTotalOtherIncome - 1) + ")");
                                        sl.SetCellValue(fila, 3, "=SUM(C" + (plStarPlusOtherIncome + 1) + ":C" + (plEndTotalOtherIncome - 1) + ")");
                                        sl.SetCellValue(fila, 4, "=SUM(D" + (plStarPlusOtherIncome + 1) + ":D" + (plEndTotalOtherIncome - 1) + ")");
                                        sl.SetCellValue(fila, 6, "=SUM(F" + (plStarPlusOtherIncome + 1) + ":F" + (plEndTotalOtherIncome - 1) + ")");
                                        break;
                                    case "Total Operating Expenses":
                                        plEndTotalOperatingExpenses = fila;
                                        sl.SetCellValue(fila, 2, "=SUM(B" + (plStartLessOperatingExpenses + 1) + ":B" + (plEndTotalOperatingExpenses - 1) + ")");
                                        sl.SetCellValue(fila, 3, "=SUM(C" + (plStartLessOperatingExpenses + 1) + ":C" + (plEndTotalOperatingExpenses - 1) + ")");
                                        sl.SetCellValue(fila, 4, "=SUM(D" + (plStartLessOperatingExpenses + 1) + ":D" + (plEndTotalOperatingExpenses - 1) + ")");
                                        sl.SetCellValue(fila, 6, "=SUM(F" + (plStartLessOperatingExpenses + 1) + ":F" + (plEndTotalOperatingExpenses - 1) + ")");
                                        break;
                                    case "Gross Profit":
                                        plGrossProfit = fila;
                                        var totalIncome = plEndTotalIncome == 0 ? "0" : "B" + plEndTotalIncome;
                                        var totalCostofSales = plEndTotalCostofSales == 0 ? "0" : "B" + plEndTotalCostofSales;
                                        sl.SetCellValue(fila, 2, "=(" + totalIncome + ")-(" + totalCostofSales + ")");

                                        totalIncome = plEndTotalIncome == 0 ? "0" : "C" + plEndTotalIncome;
                                        totalCostofSales = plEndTotalCostofSales == 0 ? "0" : "C" + plEndTotalCostofSales;
                                        sl.SetCellValue(fila, 3, "=(" + totalIncome + ")-(" + totalCostofSales + ")");

                                        totalIncome = plEndTotalIncome == 0 ? "0" : "D" + plEndTotalIncome;
                                        totalCostofSales = plEndTotalCostofSales == 0 ? "0" : "D" + plEndTotalCostofSales;
                                        sl.SetCellValue(fila, 4, "=(" + totalIncome + ")-(" + totalCostofSales + ")");

                                        totalIncome = plEndTotalIncome == 0 ? "0" : "F" + plEndTotalIncome;
                                        totalCostofSales = plEndTotalCostofSales == 0 ? "0" : "F" + plEndTotalCostofSales;
                                        sl.SetCellValue(fila, 6, "=(" + totalIncome + ")-(" + totalCostofSales + ")");
                                        break;
                                    case "Net Profit":
                                        var grossProfit = plGrossProfit == 0 ? "0" : "B" + plGrossProfit;
                                        var totalOtherIncome = plEndTotalOtherIncome == 0 ? "0" : "B" + plEndTotalOtherIncome;
                                        var totalOperatingExpenses = plEndTotalOperatingExpenses == 0 ? "0" : "B" + plEndTotalOperatingExpenses;
                                        sl.SetCellValue(fila, 2, "=(" + grossProfit + ")+(" + totalOtherIncome + ")-(" + totalOperatingExpenses + ")+(0)-(0)");

                                        grossProfit = plGrossProfit == 0 ? "0" : "C" + plGrossProfit;
                                        totalOtherIncome = plEndTotalOtherIncome == 0 ? "0" : "C" + plEndTotalOtherIncome;
                                        totalOperatingExpenses = plEndTotalOperatingExpenses == 0 ? "0" : "C" + plEndTotalOperatingExpenses;
                                        sl.SetCellValue(fila, 3, "=(" + grossProfit + ")+(" + totalOtherIncome + ")-(" + totalOperatingExpenses + ")+(0)-(0)");

                                        grossProfit = plGrossProfit == 0 ? "0" : "D" + plGrossProfit;
                                        totalOtherIncome = plEndTotalOtherIncome == 0 ? "0" : "D" + plEndTotalOtherIncome;
                                        totalOperatingExpenses = plEndTotalOperatingExpenses == 0 ? "0" : "D" + plEndTotalOperatingExpenses;
                                        sl.SetCellValue(fila, 4, "=(" + grossProfit + ")+(" + totalOtherIncome + ")-(" + totalOperatingExpenses + ")+(0)-(0)");

                                        grossProfit = plGrossProfit == 0 ? "0" : "B" + plGrossProfit;
                                        totalOtherIncome = plEndTotalOtherIncome == 0 ? "0" : "F" + plEndTotalOtherIncome;
                                        totalOperatingExpenses = plEndTotalOperatingExpenses == 0 ? "0" : "F" + plEndTotalOperatingExpenses;
                                        sl.SetCellValue(fila, 6, "=(" + grossProfit + ")+(" + totalOtherIncome + ")-(" + totalOperatingExpenses + ")+(0)-(0)");
                                        break;
                                    default:
                                        if (pl.actualValue != null)
                                        {
                                            if (double.TryParse(pl.actualValue.Replace(".", ","), out double num))
                                            {
                                                sl.SetCellValue(fila, 2, (num / 100));
                                                //var diff = (num / fiscal.qMonthsProgress) * fiscal.qMonthsPending;
                                                //sl.SetCellValue(fila, 3, (diff / 100));

                                                sl.SetCellValue(fila, 3, "=+B" + fila + "/" + fiscal.qMonthsProgress + "*" + fiscal.qMonthsPending);


                                                //sl.SetCellValue(fila, 4, ((num + diff) / 100));

                                                sl.SetCellValue(fila, 4, "=+B" + fila + "+C" + fila);

                                                style = sl.CreateStyle();
                                                style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.MediumAquamarine, System.Drawing.Color.MediumAquamarine);
                                                sl.SetCellStyle(fila, 2, style);
                                            }
                                            else
                                            {
                                                sl.SetCellValue(fila, 2, pl.actualValue);
                                            }
                                        }
                                        else
                                        {
                                            sl.SetCellValue(fila, 2, "-");
                                            style = sl.CreateStyle();
                                            style.Alignment.Horizontal = HorizontalAlignmentValues.Center;
                                            style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.MediumAquamarine, System.Drawing.Color.MediumAquamarine);
                                            sl.SetCellStyle(fila, 2, style);
                                        }

                                        if (pl.pastValue != null)
                                        {
                                            if (double.TryParse(pl.pastValue.Replace(".", ","), out double num2))
                                            {
                                                sl.SetCellValue(fila, 6, (num2 / 100));

                                                style = sl.CreateStyle();
                                                style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.MediumAquamarine, System.Drawing.Color.MediumAquamarine);
                                                sl.SetCellStyle(fila, 6, style);
                                            }
                                        }
                                        else
                                        {
                                            sl.SetCellValue(fila, 6, "-");
                                            style = sl.CreateStyle();
                                            style.Alignment.Horizontal = HorizontalAlignmentValues.Center;
                                            style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.MediumAquamarine, System.Drawing.Color.MediumAquamarine);
                                            sl.SetCellStyle(fila, 6, style);
                                        }

                                        break;
                                }

                                if (pl.rowTypeIn.ToLower().Equals("summaryrow"))
                                {
                                    sl.SetCellValue(fila, 1, pl.label);

                                    style = sl.CreateStyle();
                                    style.Font.Bold = true;
                                    sl.SetCellStyle(fila, 1, style);

                                    style = sl.CreateStyle();
                                    style.Border.BottomBorder.BorderStyle = BorderStyleValues.Thin;
                                    style.Fill.SetPattern(PatternValues.None, System.Drawing.Color.White, System.Drawing.Color.White);
                                    sl.SetCellStyle(fila, 1, fila, 6, style);

                                    fila++;
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(pl.title))
                                    {
                                        sl.SetCellValue(fila, 1, pl.label);

                                        style = sl.CreateStyle();
                                        style.Font.Bold = true;
                                        sl.SetCellStyle(fila, 1, style);

                                        style = sl.CreateStyle();
                                        style.Border.BottomBorder.BorderStyle = BorderStyleValues.Double;
                                        style.Fill.SetPattern(PatternValues.None, System.Drawing.Color.White, System.Drawing.Color.White);
                                        sl.SetCellStyle(fila, 1, fila, 6, style);

                                        fila++;
                                    }
                                    else
                                    {
                                        sl.SetCellValue(fila, 1, " " + pl.label);
                                    }
                                }
                                fila++;
                            }
                            index = pl.index;
                        }

                        style = sl.CreateStyle();
                        style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.Bisque, System.Drawing.Color.Bisque);
                        sl.SetCellStyle(5, 4, (fila - 2), 4, style);

                        style = sl.CreateStyle();
                        style.FormatCode = "$#,##0.00;($#,##0.00)";
                        sl.SetCellStyle(8, 2, fila, 6, style);

                        style = sl.CreateStyle();
                        style.Font.FontName = "Arial";
                        style.Font.FontSize = 12;
                        sl.SetCellStyle(1, 1, 3, 6, style);

                        sl.AutoFitColumn(1, 6);
                        style = sl.CreateStyle();
                        style.Font.FontName = "Arial";
                        style.Font.FontSize = 8;
                        sl.SetCellStyle(4, 1, fila, 6, style);

                    }
                    else
                    {
                        sl.SetCellValue(3, 1, "Could not get Profit and Loss report for " + dataClient.name + " client");
                    }













                    // Worksheet P&L
                    var nameWorksheetPandL = dataClient.idClient + "-" + Dal.Utils.quitarCaracteres(dataClient.name) + " Xero P&L " + txtyear + " YTD";
                    sl.AddWorksheet(nameWorksheetPandL);
                    ps.TabColor = colors[position];
                    sl.SetPageSettings(ps, nameWorksheetPandL);

                    plStartIncome = 0;
                    plEndTotalIncome = 0;
                    plStartLessCostofSales = 0;
                    plEndTotalCostofSales = 0;
                    plStarPlusOtherIncome = 0;
                    plEndTotalOtherIncome = 0;
                    plGrossProfit = 0;
                    plStartLessOperatingExpenses = 0;
                    plEndTotalOperatingExpenses = 0;

                    conn = Dal.Connection.GetConnection(dataClient.idConnection.Value);
                    xeroToken = TokenUtilities.GetStoredToken(conn.storedToken, conn.name);
                    utcTimeNow = DateTime.UtcNow;
                    serviceProvider = new ServiceCollection().AddHttpClient().BuildServiceProvider();
                    httpClientFactory = serviceProvider.GetService<IHttpClientFactory>();
                    XeroConfig = new XeroConfiguration
                    {
                        ClientId = conn.xeroClientId,
                        ClientSecret = conn.xeroClientSecret,
                        CallbackUri = new Uri(conn.xeroCallbackUri),
                        Scope = conn.xeroScope,
                        State = conn.xeroState
                    };
                    if (utcTimeNow > xeroToken.ExpiresAtUtc)
                    {
                        var client = new XeroClient(XeroConfig, httpClientFactory);
                        xeroToken = (XeroOAuth2Token)await client.RefreshAccessTokenAsync(xeroToken);

                        var txtXeroToken = TokenUtilities.StoreToken(xeroToken, conn.name);

                        var conn_ = Dal.Connection.UpdConnection(conn.idConnection, conn.name, conn.xeroClientId, conn.xeroClientSecret, conn.xeroCallbackUri, conn.xeroScope, conn.xeroState, txtXeroToken, conn.status, user.idUser);
                        conn.storedToken = conn_.storedToken;

                    }
                    accessToken = xeroToken.AccessToken;
                    xeroTenantId = xeroToken.Tenants.ToList().Where(t => t.TenantId.ToString() == dataClient.idTenant.ToString()).Select(t => t.TenantId.ToString()).FirstOrDefault();
                    if (string.IsNullOrEmpty(xeroTenantId))
                    {
                        return Json(new { Cod = 0, Msg = "Could not get the " + dataClient.name + " client information" });
                    }
                    AccountingApi = new AccountingApi();

                    dt1 = fiscal.startFiscalYear;
                    dt2 = fiscal.endReportFiscalYear;
                    response = await AccountingApi.GetReportProfitAndLossAsync(accessToken, xeroTenantId, dt1, dt2, null, null, null, null, null, null, true, null);
                    reportPandLActual = response.Reports[0];

                    var listaPandLFull = new List<ApiResponsePl>();

                    if (reportPandLActual != null)
                    {
                        var index = 1;
                        foreach (var f in reportPandLActual.Rows)
                        {
                            if (f.RowType.ToString() == "Header")
                            {
                                var apiResponse = new ApiResponsePl();
                                apiResponse.index = index;
                                apiResponse.rowType = "Header";
                                apiResponse.fullyear = f.Cells[1].Value;
                                listaPandLFull.Add(apiResponse);
                            }
                            else
                            {
                                if (f.Rows.Count != 0)
                                {
                                    foreach (var r in f.Rows)
                                    {
                                        var apiResponse = new ApiResponsePl();
                                        apiResponse.index = index;
                                        apiResponse.rowType = f.RowType.ToString();
                                        apiResponse.title = f.Title.ToString();
                                        if (r.Cells != null)
                                        {
                                            apiResponse.rowTypeIn = r.RowType.ToString();
                                            apiResponse.label = r.Cells[0].Value;
                                            apiResponse.fullyear = r.Cells[1].Value;
                                        }
                                        listaPandLFull.Add(apiResponse);
                                    }
                                }
                            }
                            index++;
                        }
                    }

                    foreach (var period in datePeriods)
                    {

                        //get report P&L actual
                        conn = Dal.Connection.GetConnection(dataClient.idConnection.Value);
                        xeroToken = TokenUtilities.GetStoredToken(conn.storedToken, conn.name);
                        utcTimeNow = DateTime.UtcNow;
                        serviceProvider = new ServiceCollection().AddHttpClient().BuildServiceProvider();
                        httpClientFactory = serviceProvider.GetService<IHttpClientFactory>();
                        XeroConfig = new XeroConfiguration
                        {
                            ClientId = conn.xeroClientId,
                            ClientSecret = conn.xeroClientSecret,
                            CallbackUri = new Uri(conn.xeroCallbackUri),
                            Scope = conn.xeroScope,
                            State = conn.xeroState
                        };
                        if (utcTimeNow > xeroToken.ExpiresAtUtc)
                        {
                            var client = new XeroClient(XeroConfig, httpClientFactory);
                            xeroToken = (XeroOAuth2Token)await client.RefreshAccessTokenAsync(xeroToken);

                            var txtXeroToken = TokenUtilities.StoreToken(xeroToken, conn.name);

                            var conn_ = Dal.Connection.UpdConnection(conn.idConnection, conn.name, conn.xeroClientId, conn.xeroClientSecret, conn.xeroCallbackUri, conn.xeroScope, conn.xeroState, txtXeroToken, conn.status, user.idUser);
                            conn.storedToken = conn_.storedToken;
                        }
                        accessToken = xeroToken.AccessToken;
                        xeroTenantId = xeroToken.Tenants.ToList().Where(t => t.TenantId.ToString() == dataClient.idTenant.ToString()).Select(t => t.TenantId.ToString()).FirstOrDefault();
                        if (string.IsNullOrEmpty(xeroTenantId))
                        {
                            return Json(new { Cod = 0, Msg = "Could not get the " + dataClient.name + " client information" });
                        }
                        AccountingApi = new AccountingApi();

                        dt1 = period.start;
                        dt2 = period.end;
                        response = await AccountingApi.GetReportProfitAndLossAsync(accessToken, xeroTenantId, dt1, dt2, null, null, null, null, null, null, true, null);
                        reportPandLPast = response.Reports[0];

                        var listaPandLmonthly = new List<ApiResponseBase>();

                        if (reportPandLPast != null)
                        {
                            var index = 1;
                            foreach (var f in reportPandLPast.Rows)
                            {
                                if (f.RowType.ToString() == "Header")
                                {
                                    var apiResponse = new ApiResponseBase();
                                    apiResponse.index = index;
                                    apiResponse.rowType = "Header";
                                    apiResponse.label = f.Cells[0].Value;
                                    apiResponse.value = f.Cells[1].Value;
                                    listaPandLmonthly.Add(apiResponse);
                                }
                                else
                                {
                                    if (f.Rows.Count != 0)
                                    {
                                        foreach (var r in f.Rows)
                                        {
                                            var apiResponse = new ApiResponseBase();
                                            apiResponse.index = index;
                                            apiResponse.rowType = f.RowType.ToString();
                                            apiResponse.title = f.Title.ToString();
                                            if (r.Cells != null)
                                            {
                                                apiResponse.rowTypeIn = r.RowType.ToString();
                                                apiResponse.label = r.Cells[0].Value;
                                                apiResponse.value = r.Cells[1].Value;
                                            }
                                            listaPandLmonthly.Add(apiResponse);
                                        }
                                    }
                                }
                                index++;
                            }
                        }

                        foreach (var pl in listaPandLFull)
                        {
                            foreach (var pl2 in listaPandLmonthly)
                            {
                                pl.label = pl.label == "" ? null : pl.label;
                                pl2.label = pl2.label == "" ? null : pl2.label;
                                if (pl.rowType == pl2.rowType && pl.title == pl2.title && pl.rowTypeIn == pl2.rowTypeIn && pl.label == pl2.label)
                                {
                                    switch (period.id)
                                    {
                                        case 0:
                                            pl.july = pl2.value;
                                            break;
                                        case 1:
                                            pl.august = pl2.value;
                                            break;
                                        case 2:
                                            pl.september = pl2.value;
                                            break;
                                        case 3:
                                            pl.october = pl2.value;
                                            break;
                                        case 4:
                                            pl.november = pl2.value;
                                            break;
                                        case 5:
                                            pl.december = pl2.value;
                                            break;
                                        case 6:
                                            pl.january = pl2.value;
                                            break;
                                        case 7:
                                            pl.february = pl2.value;
                                            break;
                                        case 8:
                                            pl.march = pl2.value;
                                            break;
                                        case 9:
                                            pl.april = pl2.value;
                                            break;
                                        case 10:
                                            pl.may = pl2.value;
                                            break;
                                        case 11:
                                            pl.june = pl2.value;
                                            break;
                                        default:

                                            break;
                                    }
                                }
                            }
                        }
                    }

                    if (listaPandLFull != null)
                    {
                        sl.SetCellValue(1, 1, "Profit & Loss");
                        sl.SetCellValue(2, 1, dataClient.name);
                        sl.SetCellValue(3, 1, "For the month ended " + fiscal.endReportFiscalYear.ToString("dd MMMM yyyy"));

                        int fila = 5;
                        var index = 1;
                        var columnstart = 0;
                        foreach (var pl in listaPandLFull)
                        {

                            if (index != pl.index)
                            {
                                if (!string.IsNullOrEmpty(pl.title))
                                {
                                    sl.SetCellValue(fila, 1, pl.title);

                                    style = sl.CreateStyle();
                                    style.Font.Bold = true;
                                    sl.SetCellStyle(fila, 1, style);

                                    switch (pl.title)
                                    {
                                        case "Income":
                                            plStartIncome = fila;
                                            break;
                                        case "Less Cost of Sales":
                                            plStartLessCostofSales = fila;
                                            break;
                                        case "Plus Other Income":
                                            plStarPlusOtherIncome = fila;
                                            break;
                                        case "Less Operating Expenses":
                                            plStartLessOperatingExpenses = fila;
                                            break;
                                        default:
                                            break;
                                    }

                                    fila++;
                                }
                            }

                            var qMonthsProgress = fiscal.qMonthsProgress;
                            columnstart = 0;

                            if (qMonthsProgress >= 12)
                            {
                                columnstart = 2;
                                var charColumnTo = Dal.Utils.ColumnTo(columnstart - 1);
                                switch (pl.label)
                                {
                                    case "Total Income":
                                        plEndTotalIncome = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStartIncome + 1) + ":" + charColumnTo + (plEndTotalIncome - 1) + ")");
                                        break;
                                    case "Total Cost of Sales":
                                        plEndTotalCostofSales = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStartLessCostofSales + 1) + ":" + charColumnTo + (plEndTotalCostofSales - 1) + ")");
                                        break;
                                    case "Total Other Income":
                                        plEndTotalOtherIncome = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStarPlusOtherIncome + 1) + ":" + charColumnTo + (plEndTotalOtherIncome - 1) + ")");
                                        break;
                                    case "Total Operating Expenses":
                                        plEndTotalOperatingExpenses = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStartLessOperatingExpenses + 1) + ":" + charColumnTo + (plEndTotalOperatingExpenses - 1) + ")");
                                        break;
                                    case "Gross Profit":
                                        plGrossProfit = fila;
                                        var totalIncome = plEndTotalIncome == 0 ? "0" : charColumnTo + plEndTotalIncome;
                                        var totalCostofSales = plEndTotalCostofSales == 0 ? "0" : charColumnTo + plEndTotalCostofSales;
                                        sl.SetCellValue(fila, columnstart, "=(" + totalIncome + ")-(" + totalCostofSales + ")");
                                        break;
                                    case "Net Profit":
                                        var grossProfit = plGrossProfit == 0 ? "0" : charColumnTo + plGrossProfit;
                                        var totalOtherIncome = plEndTotalOtherIncome == 0 ? "0" : charColumnTo + plEndTotalOtherIncome;
                                        var totalOperatingExpenses = plEndTotalOperatingExpenses == 0 ? "0" : charColumnTo + plEndTotalOperatingExpenses;
                                        sl.SetCellValue(fila, columnstart, "=(" + grossProfit + ")+(" + totalOtherIncome + ")-(" + totalOperatingExpenses + ")+(0)-(0)");
                                        break;
                                    default:
                                        if (pl.june != null)
                                        {
                                            if (double.TryParse(pl.june.Replace(".", ","), out double num))
                                            {
                                                sl.SetCellValue(fila, columnstart, (num / 100));

                                                style = sl.CreateStyle();
                                                style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.MediumAquamarine, System.Drawing.Color.MediumAquamarine);
                                                sl.SetCellStyle(fila, columnstart, style);
                                            }
                                            else
                                            {
                                                sl.SetCellValue(fila, columnstart, pl.june);
                                            }
                                        }
                                        else
                                        {
                                            sl.SetCellValue(fila, columnstart, 0);

                                            style = sl.CreateStyle();
                                            style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.MediumAquamarine, System.Drawing.Color.MediumAquamarine);
                                            sl.SetCellStyle(fila, columnstart, style);
                                        }
                                        break;
                                }
                            }

                            if (qMonthsProgress >= 11)
                            {
                                columnstart = (columnstart == 0 ? 2 : columnstart + 1);
                                var charColumnTo = Dal.Utils.ColumnTo(columnstart - 1);
                                switch (pl.label)
                                {
                                    case "Total Income":
                                        plEndTotalIncome = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStartIncome + 1) + ":" + charColumnTo + (plEndTotalIncome - 1) + ")");
                                        break;
                                    case "Total Cost of Sales":
                                        plEndTotalCostofSales = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStartLessCostofSales + 1) + ":" + charColumnTo + (plEndTotalCostofSales - 1) + ")");
                                        break;
                                    case "Total Other Income":
                                        plEndTotalOtherIncome = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStarPlusOtherIncome + 1) + ":" + charColumnTo + (plEndTotalOtherIncome - 1) + ")");
                                        break;
                                    case "Total Operating Expenses":
                                        plEndTotalOperatingExpenses = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStartLessOperatingExpenses + 1) + ":" + charColumnTo + (plEndTotalOperatingExpenses - 1) + ")");
                                        break;
                                    case "Gross Profit":
                                        plGrossProfit = fila;
                                        var totalIncome = plEndTotalIncome == 0 ? "0" : charColumnTo + plEndTotalIncome;
                                        var totalCostofSales = plEndTotalCostofSales == 0 ? "0" : charColumnTo + plEndTotalCostofSales;
                                        sl.SetCellValue(fila, columnstart, "=(" + totalIncome + ")-(" + totalCostofSales + ")");
                                        break;
                                    case "Net Profit":
                                        var grossProfit = plGrossProfit == 0 ? "0" : charColumnTo + plGrossProfit;
                                        var totalOtherIncome = plEndTotalOtherIncome == 0 ? "0" : charColumnTo + plEndTotalOtherIncome;
                                        var totalOperatingExpenses = plEndTotalOperatingExpenses == 0 ? "0" : charColumnTo + plEndTotalOperatingExpenses;
                                        sl.SetCellValue(fila, columnstart, "=(" + grossProfit + ")+(" + totalOtherIncome + ")-(" + totalOperatingExpenses + ")+(0)-(0)");
                                        break;
                                    default:
                                        if (pl.may != null)
                                        {
                                            if (double.TryParse(pl.may.Replace(".", ","), out double num))
                                            {
                                                sl.SetCellValue(fila, columnstart, (num / 100));

                                                style = sl.CreateStyle();
                                                style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.MediumAquamarine, System.Drawing.Color.MediumAquamarine);
                                                sl.SetCellStyle(fila, columnstart, style);
                                            }
                                            else
                                            {
                                                sl.SetCellValue(fila, columnstart, pl.may);
                                            }
                                        }
                                        else
                                        {
                                            sl.SetCellValue(fila, columnstart, 0);

                                            style = sl.CreateStyle();
                                            style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.MediumAquamarine, System.Drawing.Color.MediumAquamarine);
                                            sl.SetCellStyle(fila, columnstart, style);
                                        }
                                        break;
                                }
                            }

                            if (qMonthsProgress >= 10)
                            {
                                columnstart = (columnstart == 0 ? 2 : columnstart + 1);
                                var charColumnTo = Dal.Utils.ColumnTo(columnstart - 1);
                                switch (pl.label)
                                {
                                    case "Total Income":
                                        plEndTotalIncome = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStartIncome + 1) + ":" + charColumnTo + (plEndTotalIncome - 1) + ")");
                                        break;
                                    case "Total Cost of Sales":
                                        plEndTotalCostofSales = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStartLessCostofSales + 1) + ":" + charColumnTo + (plEndTotalCostofSales - 1) + ")");
                                        break;
                                    case "Total Other Income":
                                        plEndTotalOtherIncome = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStarPlusOtherIncome + 1) + ":" + charColumnTo + (plEndTotalOtherIncome - 1) + ")");
                                        break;
                                    case "Total Operating Expenses":
                                        plEndTotalOperatingExpenses = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStartLessOperatingExpenses + 1) + ":" + charColumnTo + (plEndTotalOperatingExpenses - 1) + ")");
                                        break;
                                    case "Gross Profit":
                                        plGrossProfit = fila;
                                        var totalIncome = plEndTotalIncome == 0 ? "0" : charColumnTo + plEndTotalIncome;
                                        var totalCostofSales = plEndTotalCostofSales == 0 ? "0" : charColumnTo + plEndTotalCostofSales;
                                        sl.SetCellValue(fila, columnstart, "=(" + totalIncome + ")-(" + totalCostofSales + ")");
                                        break;
                                    case "Net Profit":
                                        var grossProfit = plGrossProfit == 0 ? "0" : charColumnTo + plGrossProfit;
                                        var totalOtherIncome = plEndTotalOtherIncome == 0 ? "0" : charColumnTo + plEndTotalOtherIncome;
                                        var totalOperatingExpenses = plEndTotalOperatingExpenses == 0 ? "0" : charColumnTo + plEndTotalOperatingExpenses;
                                        sl.SetCellValue(fila, columnstart, "=(" + grossProfit + ")+(" + totalOtherIncome + ")-(" + totalOperatingExpenses + ")+(0)-(0)");
                                        break;
                                    default:
                                        if (pl.april != null)
                                        {
                                            if (double.TryParse(pl.april.Replace(".", ","), out double num))
                                            {
                                                sl.SetCellValue(fila, columnstart, (num / 100));

                                                style = sl.CreateStyle();
                                                style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.MediumAquamarine, System.Drawing.Color.MediumAquamarine);
                                                sl.SetCellStyle(fila, columnstart, style);
                                            }
                                            else
                                            {
                                                sl.SetCellValue(fila, columnstart, pl.april);
                                            }
                                        }
                                        else
                                        {
                                            sl.SetCellValue(fila, columnstart, 0);

                                            style = sl.CreateStyle();
                                            style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.MediumAquamarine, System.Drawing.Color.MediumAquamarine);
                                            sl.SetCellStyle(fila, columnstart, style);
                                        }
                                        break;
                                }
                            }

                            if (qMonthsProgress >= 9)
                            {
                                columnstart = (columnstart == 0 ? 2 : columnstart + 1);
                                var charColumnTo = Dal.Utils.ColumnTo(columnstart - 1);
                                switch (pl.label)
                                {
                                    case "Total Income":
                                        plEndTotalIncome = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStartIncome + 1) + ":" + charColumnTo + (plEndTotalIncome - 1) + ")");
                                        break;
                                    case "Total Cost of Sales":
                                        plEndTotalCostofSales = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStartLessCostofSales + 1) + ":" + charColumnTo + (plEndTotalCostofSales - 1) + ")");
                                        break;
                                    case "Total Other Income":
                                        plEndTotalOtherIncome = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStarPlusOtherIncome + 1) + ":" + charColumnTo + (plEndTotalOtherIncome - 1) + ")");
                                        break;
                                    case "Total Operating Expenses":
                                        plEndTotalOperatingExpenses = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStartLessOperatingExpenses + 1) + ":" + charColumnTo + (plEndTotalOperatingExpenses - 1) + ")");
                                        break;
                                    case "Gross Profit":
                                        plGrossProfit = fila;
                                        var totalIncome = plEndTotalIncome == 0 ? "0" : charColumnTo + plEndTotalIncome;
                                        var totalCostofSales = plEndTotalCostofSales == 0 ? "0" : charColumnTo + plEndTotalCostofSales;
                                        sl.SetCellValue(fila, columnstart, "=(" + totalIncome + ")-(" + totalCostofSales + ")");
                                        break;
                                    case "Net Profit":
                                        var grossProfit = plGrossProfit == 0 ? "0" : charColumnTo + plGrossProfit;
                                        var totalOtherIncome = plEndTotalOtherIncome == 0 ? "0" : charColumnTo + plEndTotalOtherIncome;
                                        var totalOperatingExpenses = plEndTotalOperatingExpenses == 0 ? "0" : charColumnTo + plEndTotalOperatingExpenses;
                                        sl.SetCellValue(fila, columnstart, "=(" + grossProfit + ")+(" + totalOtherIncome + ")-(" + totalOperatingExpenses + ")+(0)-(0)");
                                        break;
                                    default:
                                        if (pl.march != null)
                                        {
                                            if (double.TryParse(pl.march.Replace(".", ","), out double num))
                                            {
                                                sl.SetCellValue(fila, columnstart, (num / 100));

                                                style = sl.CreateStyle();
                                                style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.MediumAquamarine, System.Drawing.Color.MediumAquamarine);
                                                sl.SetCellStyle(fila, columnstart, style);
                                            }
                                            else
                                            {
                                                sl.SetCellValue(fila, columnstart, pl.march);
                                            }
                                        }
                                        else
                                        {
                                            sl.SetCellValue(fila, columnstart, 0);

                                            style = sl.CreateStyle();
                                            style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.MediumAquamarine, System.Drawing.Color.MediumAquamarine);
                                            sl.SetCellStyle(fila, columnstart, style);
                                        }
                                        break;
                                }
                            }

                            if (qMonthsProgress >= 8)
                            {
                                columnstart = (columnstart == 0 ? 2 : columnstart + 1);
                                var charColumnTo = Dal.Utils.ColumnTo(columnstart - 1);
                                switch (pl.label)
                                {
                                    case "Total Income":
                                        plEndTotalIncome = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStartIncome + 1) + ":" + charColumnTo + (plEndTotalIncome - 1) + ")");
                                        break;
                                    case "Total Cost of Sales":
                                        plEndTotalCostofSales = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStartLessCostofSales + 1) + ":" + charColumnTo + (plEndTotalCostofSales - 1) + ")");
                                        break;
                                    case "Total Other Income":
                                        plEndTotalOtherIncome = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStarPlusOtherIncome + 1) + ":" + charColumnTo + (plEndTotalOtherIncome - 1) + ")");
                                        break;
                                    case "Total Operating Expenses":
                                        plEndTotalOperatingExpenses = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStartLessOperatingExpenses + 1) + ":" + charColumnTo + (plEndTotalOperatingExpenses - 1) + ")");
                                        break;
                                    case "Gross Profit":
                                        plGrossProfit = fila;
                                        var totalIncome = plEndTotalIncome == 0 ? "0" : charColumnTo + plEndTotalIncome;
                                        var totalCostofSales = plEndTotalCostofSales == 0 ? "0" : charColumnTo + plEndTotalCostofSales;
                                        sl.SetCellValue(fila, columnstart, "=(" + totalIncome + ")-(" + totalCostofSales + ")");
                                        break;
                                    case "Net Profit":
                                        var grossProfit = plGrossProfit == 0 ? "0" : charColumnTo + plGrossProfit;
                                        var totalOtherIncome = plEndTotalOtherIncome == 0 ? "0" : charColumnTo + plEndTotalOtherIncome;
                                        var totalOperatingExpenses = plEndTotalOperatingExpenses == 0 ? "0" : charColumnTo + plEndTotalOperatingExpenses;
                                        sl.SetCellValue(fila, columnstart, "=(" + grossProfit + ")+(" + totalOtherIncome + ")-(" + totalOperatingExpenses + ")+(0)-(0)");
                                        break;
                                    default:
                                        if (pl.february != null)
                                        {
                                            if (double.TryParse(pl.february.Replace(".", ","), out double num))
                                            {
                                                sl.SetCellValue(fila, columnstart, (num / 100));

                                                style = sl.CreateStyle();
                                                style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.MediumAquamarine, System.Drawing.Color.MediumAquamarine);
                                                sl.SetCellStyle(fila, columnstart, style);
                                            }
                                            else
                                            {
                                                sl.SetCellValue(fila, columnstart, pl.february);
                                            }
                                        }
                                        else
                                        {
                                            sl.SetCellValue(fila, columnstart, 0);

                                            style = sl.CreateStyle();
                                            style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.MediumAquamarine, System.Drawing.Color.MediumAquamarine);
                                            sl.SetCellStyle(fila, columnstart, style);
                                        }
                                        break;
                                }
                            }

                            if (qMonthsProgress >= 7)
                            {
                                columnstart = (columnstart == 0 ? 2 : columnstart + 1);
                                var charColumnTo = Dal.Utils.ColumnTo(columnstart - 1);
                                switch (pl.label)
                                {
                                    case "Total Income":
                                        plEndTotalIncome = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStartIncome + 1) + ":" + charColumnTo + (plEndTotalIncome - 1) + ")");
                                        break;
                                    case "Total Cost of Sales":
                                        plEndTotalCostofSales = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStartLessCostofSales + 1) + ":" + charColumnTo + (plEndTotalCostofSales - 1) + ")");
                                        break;
                                    case "Total Other Income":
                                        plEndTotalOtherIncome = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStarPlusOtherIncome + 1) + ":" + charColumnTo + (plEndTotalOtherIncome - 1) + ")");
                                        break;
                                    case "Total Operating Expenses":
                                        plEndTotalOperatingExpenses = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStartLessOperatingExpenses + 1) + ":" + charColumnTo + (plEndTotalOperatingExpenses - 1) + ")");
                                        break;
                                    case "Gross Profit":
                                        plGrossProfit = fila;
                                        var totalIncome = plEndTotalIncome == 0 ? "0" : charColumnTo + plEndTotalIncome;
                                        var totalCostofSales = plEndTotalCostofSales == 0 ? "0" : charColumnTo + plEndTotalCostofSales;
                                        sl.SetCellValue(fila, columnstart, "=(" + totalIncome + ")-(" + totalCostofSales + ")");
                                        break;
                                    case "Net Profit":
                                        var grossProfit = plGrossProfit == 0 ? "0" : charColumnTo + plGrossProfit;
                                        var totalOtherIncome = plEndTotalOtherIncome == 0 ? "0" : charColumnTo + plEndTotalOtherIncome;
                                        var totalOperatingExpenses = plEndTotalOperatingExpenses == 0 ? "0" : charColumnTo + plEndTotalOperatingExpenses;
                                        sl.SetCellValue(fila, columnstart, "=(" + grossProfit + ")+(" + totalOtherIncome + ")-(" + totalOperatingExpenses + ")+(0)-(0)");
                                        break;
                                    default:
                                        if (pl.january != null)
                                        {
                                            if (double.TryParse(pl.january.Replace(".", ","), out double num))
                                            {
                                                sl.SetCellValue(fila, columnstart, (num / 100));

                                                style = sl.CreateStyle();
                                                style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.MediumAquamarine, System.Drawing.Color.MediumAquamarine);
                                                sl.SetCellStyle(fila, columnstart, style);
                                            }
                                            else
                                            {
                                                sl.SetCellValue(fila, columnstart, pl.january);
                                            }
                                        }
                                        else
                                        {
                                            sl.SetCellValue(fila, columnstart, 0);

                                            style = sl.CreateStyle();
                                            style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.MediumAquamarine, System.Drawing.Color.MediumAquamarine);
                                            sl.SetCellStyle(fila, columnstart, style);
                                        }
                                        break;
                                }
                            }

                            if (qMonthsProgress >= 6)
                            {
                                columnstart = (columnstart == 0 ? 2 : columnstart + 1);
                                var charColumnTo = Dal.Utils.ColumnTo(columnstart - 1);
                                switch (pl.label)
                                {
                                    case "Total Income":
                                        plEndTotalIncome = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStartIncome + 1) + ":" + charColumnTo + (plEndTotalIncome - 1) + ")");
                                        break;
                                    case "Total Cost of Sales":
                                        plEndTotalCostofSales = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStartLessCostofSales + 1) + ":" + charColumnTo + (plEndTotalCostofSales - 1) + ")");
                                        break;
                                    case "Total Other Income":
                                        plEndTotalOtherIncome = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStarPlusOtherIncome + 1) + ":" + charColumnTo + (plEndTotalOtherIncome - 1) + ")");
                                        break;
                                    case "Total Operating Expenses":
                                        plEndTotalOperatingExpenses = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStartLessOperatingExpenses + 1) + ":" + charColumnTo + (plEndTotalOperatingExpenses - 1) + ")");
                                        break;
                                    case "Gross Profit":
                                        plGrossProfit = fila;
                                        var totalIncome = plEndTotalIncome == 0 ? "0" : charColumnTo + plEndTotalIncome;
                                        var totalCostofSales = plEndTotalCostofSales == 0 ? "0" : charColumnTo + plEndTotalCostofSales;
                                        sl.SetCellValue(fila, columnstart, "=(" + totalIncome + ")-(" + totalCostofSales + ")");
                                        break;
                                    case "Net Profit":
                                        var grossProfit = plGrossProfit == 0 ? "0" : charColumnTo + plGrossProfit;
                                        var totalOtherIncome = plEndTotalOtherIncome == 0 ? "0" : charColumnTo + plEndTotalOtherIncome;
                                        var totalOperatingExpenses = plEndTotalOperatingExpenses == 0 ? "0" : charColumnTo + plEndTotalOperatingExpenses;
                                        sl.SetCellValue(fila, columnstart, "=(" + grossProfit + ")+(" + totalOtherIncome + ")-(" + totalOperatingExpenses + ")+(0)-(0)");
                                        break;
                                    default:
                                        if (pl.december != null)
                                        {
                                            if (double.TryParse(pl.december.Replace(".", ","), out double num))
                                            {
                                                sl.SetCellValue(fila, columnstart, (num / 100));

                                                style = sl.CreateStyle();
                                                style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.MediumAquamarine, System.Drawing.Color.MediumAquamarine);
                                                sl.SetCellStyle(fila, columnstart, style);
                                            }
                                            else
                                            {
                                                sl.SetCellValue(fila, columnstart, pl.december);
                                            }
                                        }
                                        else
                                        {
                                            sl.SetCellValue(fila, columnstart, 0);

                                            style = sl.CreateStyle();
                                            style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.MediumAquamarine, System.Drawing.Color.MediumAquamarine);
                                            sl.SetCellStyle(fila, columnstart, style);
                                        }
                                        break;
                                }
                            }

                            if (qMonthsProgress >= 5)
                            {
                                columnstart = (columnstart == 0 ? 2 : columnstart + 1);
                                var charColumnTo = Dal.Utils.ColumnTo(columnstart - 1);
                                switch (pl.label)
                                {
                                    case "Total Income":
                                        plEndTotalIncome = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStartIncome + 1) + ":" + charColumnTo + (plEndTotalIncome - 1) + ")");
                                        break;
                                    case "Total Cost of Sales":
                                        plEndTotalCostofSales = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStartLessCostofSales + 1) + ":" + charColumnTo + (plEndTotalCostofSales - 1) + ")");
                                        break;
                                    case "Total Other Income":
                                        plEndTotalOtherIncome = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStarPlusOtherIncome + 1) + ":" + charColumnTo + (plEndTotalOtherIncome - 1) + ")");
                                        break;
                                    case "Total Operating Expenses":
                                        plEndTotalOperatingExpenses = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStartLessOperatingExpenses + 1) + ":" + charColumnTo + (plEndTotalOperatingExpenses - 1) + ")");
                                        break;
                                    case "Gross Profit":
                                        plGrossProfit = fila;
                                        var totalIncome = plEndTotalIncome == 0 ? "0" : charColumnTo + plEndTotalIncome;
                                        var totalCostofSales = plEndTotalCostofSales == 0 ? "0" : charColumnTo + plEndTotalCostofSales;
                                        sl.SetCellValue(fila, columnstart, "=(" + totalIncome + ")-(" + totalCostofSales + ")");
                                        break;
                                    case "Net Profit":
                                        var grossProfit = plGrossProfit == 0 ? "0" : charColumnTo + plGrossProfit;
                                        var totalOtherIncome = plEndTotalOtherIncome == 0 ? "0" : charColumnTo + plEndTotalOtherIncome;
                                        var totalOperatingExpenses = plEndTotalOperatingExpenses == 0 ? "0" : charColumnTo + plEndTotalOperatingExpenses;
                                        sl.SetCellValue(fila, columnstart, "=(" + grossProfit + ")+(" + totalOtherIncome + ")-(" + totalOperatingExpenses + ")+(0)-(0)");
                                        break;
                                    default:
                                        if (pl.november != null)
                                        {
                                            if (double.TryParse(pl.november.Replace(".", ","), out double num))
                                            {
                                                sl.SetCellValue(fila, columnstart, (num / 100));

                                                style = sl.CreateStyle();
                                                style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.MediumAquamarine, System.Drawing.Color.MediumAquamarine);
                                                sl.SetCellStyle(fila, columnstart, style);
                                            }
                                            else
                                            {
                                                sl.SetCellValue(fila, columnstart, pl.november);
                                            }
                                        }
                                        else
                                        {
                                            sl.SetCellValue(fila, columnstart, 0);

                                            style = sl.CreateStyle();
                                            style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.MediumAquamarine, System.Drawing.Color.MediumAquamarine);
                                            sl.SetCellStyle(fila, columnstart, style);
                                        }
                                        break;
                                }
                            }

                            if (qMonthsProgress >= 4)
                            {
                                columnstart = (columnstart == 0 ? 2 : columnstart + 1);
                                var charColumnTo = Dal.Utils.ColumnTo(columnstart - 1);
                                switch (pl.label)
                                {
                                    case "Total Income":
                                        plEndTotalIncome = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStartIncome + 1) + ":" + charColumnTo + (plEndTotalIncome - 1) + ")");
                                        break;
                                    case "Total Cost of Sales":
                                        plEndTotalCostofSales = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStartLessCostofSales + 1) + ":" + charColumnTo + (plEndTotalCostofSales - 1) + ")");
                                        break;
                                    case "Total Other Income":
                                        plEndTotalOtherIncome = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStarPlusOtherIncome + 1) + ":" + charColumnTo + (plEndTotalOtherIncome - 1) + ")");
                                        break;
                                    case "Total Operating Expenses":
                                        plEndTotalOperatingExpenses = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStartLessOperatingExpenses + 1) + ":" + charColumnTo + (plEndTotalOperatingExpenses - 1) + ")");
                                        break;
                                    case "Gross Profit":
                                        plGrossProfit = fila;
                                        var totalIncome = plEndTotalIncome == 0 ? "0" : charColumnTo + plEndTotalIncome;
                                        var totalCostofSales = plEndTotalCostofSales == 0 ? "0" : charColumnTo + plEndTotalCostofSales;
                                        sl.SetCellValue(fila, columnstart, "=(" + totalIncome + ")-(" + totalCostofSales + ")");
                                        break;
                                    case "Net Profit":
                                        var grossProfit = plGrossProfit == 0 ? "0" : charColumnTo + plGrossProfit;
                                        var totalOtherIncome = plEndTotalOtherIncome == 0 ? "0" : charColumnTo + plEndTotalOtherIncome;
                                        var totalOperatingExpenses = plEndTotalOperatingExpenses == 0 ? "0" : charColumnTo + plEndTotalOperatingExpenses;
                                        sl.SetCellValue(fila, columnstart, "=(" + grossProfit + ")+(" + totalOtherIncome + ")-(" + totalOperatingExpenses + ")+(0)-(0)");
                                        break;
                                    default:
                                        if (pl.october != null)
                                        {
                                            if (double.TryParse(pl.october.Replace(".", ","), out double num))
                                            {
                                                sl.SetCellValue(fila, columnstart, (num / 100));

                                                style = sl.CreateStyle();
                                                style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.MediumAquamarine, System.Drawing.Color.MediumAquamarine);
                                                sl.SetCellStyle(fila, columnstart, style);
                                            }
                                            else
                                            {
                                                sl.SetCellValue(fila, columnstart, pl.october);
                                            }
                                        }
                                        else
                                        {
                                            sl.SetCellValue(fila, columnstart, 0);

                                            style = sl.CreateStyle();
                                            style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.MediumAquamarine, System.Drawing.Color.MediumAquamarine);
                                            sl.SetCellStyle(fila, columnstart, style);
                                        }
                                        break;
                                }
                            }

                            if (qMonthsProgress >= 3)
                            {
                                columnstart = (columnstart == 0 ? 2 : columnstart + 1);
                                var charColumnTo = Dal.Utils.ColumnTo(columnstart - 1);
                                switch (pl.label)
                                {
                                    case "Total Income":
                                        plEndTotalIncome = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStartIncome + 1) + ":" + charColumnTo + (plEndTotalIncome - 1) + ")");
                                        break;
                                    case "Total Cost of Sales":
                                        plEndTotalCostofSales = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStartLessCostofSales + 1) + ":" + charColumnTo + (plEndTotalCostofSales - 1) + ")");
                                        break;
                                    case "Total Other Income":
                                        plEndTotalOtherIncome = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStarPlusOtherIncome + 1) + ":" + charColumnTo + (plEndTotalOtherIncome - 1) + ")");
                                        break;
                                    case "Total Operating Expenses":
                                        plEndTotalOperatingExpenses = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStartLessOperatingExpenses + 1) + ":" + charColumnTo + (plEndTotalOperatingExpenses - 1) + ")");
                                        break;
                                    case "Gross Profit":
                                        plGrossProfit = fila;
                                        var totalIncome = plEndTotalIncome == 0 ? "0" : charColumnTo + plEndTotalIncome;
                                        var totalCostofSales = plEndTotalCostofSales == 0 ? "0" : charColumnTo + plEndTotalCostofSales;
                                        sl.SetCellValue(fila, columnstart, "=(" + totalIncome + ")-(" + totalCostofSales + ")");
                                        break;
                                    case "Net Profit":
                                        var grossProfit = plGrossProfit == 0 ? "0" : charColumnTo + plGrossProfit;
                                        var totalOtherIncome = plEndTotalOtherIncome == 0 ? "0" : charColumnTo + plEndTotalOtherIncome;
                                        var totalOperatingExpenses = plEndTotalOperatingExpenses == 0 ? "0" : charColumnTo + plEndTotalOperatingExpenses;
                                        sl.SetCellValue(fila, columnstart, "=(" + grossProfit + ")+(" + totalOtherIncome + ")-(" + totalOperatingExpenses + ")+(0)-(0)");
                                        break;
                                    default:
                                        if (pl.september != null)
                                        {
                                            if (double.TryParse(pl.september.Replace(".", ","), out double num))
                                            {
                                                sl.SetCellValue(fila, columnstart, (num / 100));

                                                style = sl.CreateStyle();
                                                style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.MediumAquamarine, System.Drawing.Color.MediumAquamarine);
                                                sl.SetCellStyle(fila, columnstart, style);
                                            }
                                            else
                                            {
                                                sl.SetCellValue(fila, columnstart, pl.september);
                                            }
                                        }
                                        else
                                        {
                                            sl.SetCellValue(fila, columnstart, 0);

                                            style = sl.CreateStyle();
                                            style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.MediumAquamarine, System.Drawing.Color.MediumAquamarine);
                                            sl.SetCellStyle(fila, columnstart, style);
                                        }
                                        break;
                                }
                            }

                            if (qMonthsProgress >= 2)
                            {
                                columnstart = (columnstart == 0 ? 2 : columnstart + 1);
                                var charColumnTo = Dal.Utils.ColumnTo(columnstart - 1);
                                switch (pl.label)
                                {
                                    case "Total Income":
                                        plEndTotalIncome = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStartIncome + 1) + ":" + charColumnTo + (plEndTotalIncome - 1) + ")");
                                        break;
                                    case "Total Cost of Sales":
                                        plEndTotalCostofSales = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStartLessCostofSales + 1) + ":" + charColumnTo + (plEndTotalCostofSales - 1) + ")");
                                        break;
                                    case "Total Other Income":
                                        plEndTotalOtherIncome = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStarPlusOtherIncome + 1) + ":" + charColumnTo + (plEndTotalOtherIncome - 1) + ")");
                                        break;
                                    case "Total Operating Expenses":
                                        plEndTotalOperatingExpenses = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStartLessOperatingExpenses + 1) + ":" + charColumnTo + (plEndTotalOperatingExpenses - 1) + ")");
                                        break;
                                    case "Gross Profit":
                                        plGrossProfit = fila;
                                        var totalIncome = plEndTotalIncome == 0 ? "0" : charColumnTo + plEndTotalIncome;
                                        var totalCostofSales = plEndTotalCostofSales == 0 ? "0" : charColumnTo + plEndTotalCostofSales;
                                        sl.SetCellValue(fila, columnstart, "=(" + totalIncome + ")-(" + totalCostofSales + ")");
                                        break;
                                    case "Net Profit":
                                        var grossProfit = plGrossProfit == 0 ? "0" : charColumnTo + plGrossProfit;
                                        var totalOtherIncome = plEndTotalOtherIncome == 0 ? "0" : charColumnTo + plEndTotalOtherIncome;
                                        var totalOperatingExpenses = plEndTotalOperatingExpenses == 0 ? "0" : charColumnTo + plEndTotalOperatingExpenses;
                                        sl.SetCellValue(fila, columnstart, "=(" + grossProfit + ")+(" + totalOtherIncome + ")-(" + totalOperatingExpenses + ")+(0)-(0)");
                                        break;
                                    default:
                                        if (pl.august != null)
                                        {
                                            if (double.TryParse(pl.august.Replace(".", ","), out double num))
                                            {
                                                sl.SetCellValue(fila, columnstart, (num / 100));

                                                style = sl.CreateStyle();
                                                style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.MediumAquamarine, System.Drawing.Color.MediumAquamarine);
                                                sl.SetCellStyle(fila, columnstart, style);
                                            }
                                            else
                                            {
                                                sl.SetCellValue(fila, columnstart, pl.august);
                                            }
                                        }
                                        else
                                        {
                                            sl.SetCellValue(fila, columnstart, 0);

                                            style = sl.CreateStyle();
                                            style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.MediumAquamarine, System.Drawing.Color.MediumAquamarine);
                                            sl.SetCellStyle(fila, columnstart, style);
                                        }
                                        break;
                                }
                            }

                            if (qMonthsProgress >= 1)
                            {
                                columnstart = (columnstart == 0 ? 2 : columnstart + 1);
                                var charColumnTo = Dal.Utils.ColumnTo(columnstart - 1);
                                switch (pl.label)
                                {
                                    case "Total Income":
                                        plEndTotalIncome = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStartIncome + 1) + ":" + charColumnTo + (plEndTotalIncome - 1) + ")");
                                        break;
                                    case "Total Cost of Sales":
                                        plEndTotalCostofSales = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStartLessCostofSales + 1) + ":" + charColumnTo + (plEndTotalCostofSales - 1) + ")");
                                        break;
                                    case "Total Other Income":
                                        plEndTotalOtherIncome = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStarPlusOtherIncome + 1) + ":" + charColumnTo + (plEndTotalOtherIncome - 1) + ")");
                                        break;
                                    case "Total Operating Expenses":
                                        plEndTotalOperatingExpenses = fila;
                                        sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnTo + (plStartLessOperatingExpenses + 1) + ":" + charColumnTo + (plEndTotalOperatingExpenses - 1) + ")");
                                        break;
                                    case "Gross Profit":
                                        plGrossProfit = fila;
                                        var totalIncome = plEndTotalIncome == 0 ? "0" : charColumnTo + plEndTotalIncome;
                                        var totalCostofSales = plEndTotalCostofSales == 0 ? "0" : charColumnTo + plEndTotalCostofSales;
                                        sl.SetCellValue(fila, columnstart, "=(" + totalIncome + ")-(" + totalCostofSales + ")");
                                        break;
                                    case "Net Profit":
                                        var grossProfit = plGrossProfit == 0 ? "0" : charColumnTo + plGrossProfit;
                                        var totalOtherIncome = plEndTotalOtherIncome == 0 ? "0" : charColumnTo + plEndTotalOtherIncome;
                                        var totalOperatingExpenses = plEndTotalOperatingExpenses == 0 ? "0" : charColumnTo + plEndTotalOperatingExpenses;
                                        sl.SetCellValue(fila, columnstart, "=(" + grossProfit + ")+(" + totalOtherIncome + ")-(" + totalOperatingExpenses + ")+(0)-(0)");
                                        break;
                                    default:
                                        if (pl.july != null)
                                        {
                                            if (double.TryParse(pl.july.Replace(".", ","), out double num))
                                            {
                                                sl.SetCellValue(fila, columnstart, (num / 100));

                                                style = sl.CreateStyle();
                                                style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.MediumAquamarine, System.Drawing.Color.MediumAquamarine);
                                                sl.SetCellStyle(fila, columnstart, style);
                                            }
                                            else
                                            {
                                                sl.SetCellValue(fila, columnstart, pl.july);
                                            }
                                        }
                                        else
                                        {
                                            sl.SetCellValue(fila, columnstart, 0);

                                            style = sl.CreateStyle();
                                            style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.MediumAquamarine, System.Drawing.Color.MediumAquamarine);
                                            sl.SetCellStyle(fila, columnstart, style);
                                        }
                                        break;
                                }

                            }

                            columnstart = (columnstart == 0 ? 2 : columnstart + 1);
                            var charColumnToYTD = Dal.Utils.ColumnTo(columnstart - 1);
                            switch (pl.label)
                            {
                                case "Total Income":
                                    plEndTotalIncome = fila;
                                    sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnToYTD + (plStartIncome + 1) + ":" + charColumnToYTD + (plEndTotalIncome - 1) + ")");
                                    break;
                                case "Total Cost of Sales":
                                    plEndTotalCostofSales = fila;
                                    sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnToYTD + (plStartLessCostofSales + 1) + ":" + charColumnToYTD + (plEndTotalCostofSales - 1) + ")");
                                    break;
                                case "Total Other Income":
                                    plEndTotalOtherIncome = fila;
                                    sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnToYTD + (plStarPlusOtherIncome + 1) + ":" + charColumnToYTD + (plEndTotalOtherIncome - 1) + ")");
                                    break;
                                case "Total Operating Expenses":
                                    plEndTotalOperatingExpenses = fila;
                                    sl.SetCellValue(fila, columnstart, "=SUM(" + charColumnToYTD + (plStartLessOperatingExpenses + 1) + ":" + charColumnToYTD + (plEndTotalOperatingExpenses - 1) + ")");
                                    break;
                                case "Gross Profit":
                                    plGrossProfit = fila;
                                    var totalIncome = plEndTotalIncome == 0 ? "0" : charColumnToYTD + plEndTotalIncome;
                                    var totalCostofSales = plEndTotalCostofSales == 0 ? "0" : charColumnToYTD + plEndTotalCostofSales;
                                    sl.SetCellValue(fila, columnstart, "=(" + totalIncome + ")-(" + totalCostofSales + ")");
                                    break;
                                case "Net Profit":
                                    var grossProfit = plGrossProfit == 0 ? "0" : charColumnToYTD + plGrossProfit;
                                    var totalOtherIncome = plEndTotalOtherIncome == 0 ? "0" : charColumnToYTD + plEndTotalOtherIncome;
                                    var totalOperatingExpenses = plEndTotalOperatingExpenses == 0 ? "0" : charColumnToYTD + plEndTotalOperatingExpenses;
                                    sl.SetCellValue(fila, columnstart, "=(" + grossProfit + ")+(" + totalOtherIncome + ")-(" + totalOperatingExpenses + ")+(0)-(0)");
                                    break;
                                default:
                                    if (pl.fullyear != null)
                                    {
                                        if (double.TryParse(pl.fullyear.Replace(".", ","), out double num2))
                                        {
                                            sl.SetCellValue(fila, columnstart, (num2 / 100));

                                            style = sl.CreateStyle();
                                            style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.MediumAquamarine, System.Drawing.Color.MediumAquamarine);
                                            sl.SetCellStyle(fila, columnstart, style);
                                        }
                                    }
                                    else
                                    {
                                        sl.SetCellValue(fila, columnstart, 0);
                                        style = sl.CreateStyle();
                                        style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.MediumAquamarine, System.Drawing.Color.MediumAquamarine);
                                        sl.SetCellStyle(fila, columnstart, style);
                                    }
                                    break;
                            }

                            if (!string.IsNullOrEmpty(pl.rowTypeIn))
                            {

                                if (pl.rowTypeIn.ToLower().Equals("summaryrow"))
                                {
                                    sl.SetCellValue(fila, 1, pl.label);

                                    style = sl.CreateStyle();
                                    style.Font.Bold = true;
                                    sl.SetCellStyle(fila, 1, style);

                                    style = sl.CreateStyle();
                                    style.Border.BottomBorder.BorderStyle = BorderStyleValues.Thin;
                                    style.Fill.SetPattern(PatternValues.None, System.Drawing.Color.White, System.Drawing.Color.White);
                                    sl.SetCellStyle(fila, 1, fila, columnstart, style);

                                    fila++;
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(pl.title))
                                    {
                                        sl.SetCellValue(fila, 1, pl.label);

                                        style = sl.CreateStyle();
                                        style.Font.Bold = true;
                                        sl.SetCellStyle(fila, 1, style);

                                        style = sl.CreateStyle();
                                        style.Border.BottomBorder.BorderStyle = BorderStyleValues.Double;
                                        style.Fill.SetPattern(PatternValues.None, System.Drawing.Color.White, System.Drawing.Color.White);
                                        sl.SetCellStyle(fila, 1, fila, columnstart, style);

                                        fila++;
                                    }
                                    else
                                    {
                                        sl.SetCellValue(fila, 1, " " + pl.label);
                                    }
                                }
                            }

                            fila++;
                            index = pl.index;
                        }

                        style = sl.CreateStyle();
                        style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.Gold, System.Drawing.Color.Gold);
                        sl.SetCellStyle((fila - 2), columnstart, style);

                        sl.MergeWorksheetCells(1, 1, 1, columnstart);
                        sl.MergeWorksheetCells(2, 1, 2, columnstart);
                        sl.MergeWorksheetCells(3, 1, 3, columnstart);

                        style = sl.CreateStyle();
                        style.Font.Bold = true;
                        style.Alignment.Horizontal = HorizontalAlignmentValues.Center;
                        sl.SetCellStyle(1, 1, 3, columnstart, style);

                        sl.SetCellValue(5, columnstart, "YTD");

                        style = sl.CreateStyle();
                        style.FormatCode = "$#,##0.00;($#,##0.00)";
                        sl.SetCellStyle(5, 2, fila, columnstart, style);

                        style = sl.CreateStyle();
                        style.Font.FontName = "Arial";
                        style.Font.FontSize = 12;
                        sl.SetCellStyle(1, 1, 3, columnstart, style);

                        sl.AutoFitColumn(1, columnstart);
                        style = sl.CreateStyle();
                        style.Font.FontName = "Arial";
                        style.Font.FontSize = 8;
                        sl.SetCellStyle(4, 1, fila, columnstart, style);

                    }
                    else
                    {
                        sl.SetCellValue(3, 1, "Could not get Profit and Loss report for " + dataClient.name + " client");
                    }
























                    // Worksheet P&L
                    //var nameWorksheetPandL = dataClient.idClient + "-" + Dal.Utils.quitarCaracteres(dataClient.name) + " Xero P&L " + txtyear + " YTD";
                    //sl.AddWorksheet(nameWorksheetPandL);
                    //ps.TabColor = colors[position];
                    //sl.SetPageSettings(ps, nameWorksheetPandL);

                    //plStartIncome = 0;
                    //plEndTotalIncome = 0;
                    //plStartLessCostofSales = 0;
                    //plEndTotalCostofSales = 0;
                    //plGrossProfit = 0;
                    //plStartLessOperatingExpenses = 0;
                    //plEndTotalOperatingExpenses = 0;

                    //conn = Dal.Connection.GetConnection(dataClient.idConnection.Value);
                    //xeroToken = TokenUtilities.GetStoredToken(conn.storedToken, conn.name);
                    //utcTimeNow = DateTime.UtcNow;
                    //serviceProvider = new ServiceCollection().AddHttpClient().BuildServiceProvider();
                    //httpClientFactory = serviceProvider.GetService<IHttpClientFactory>();
                    //XeroConfig = new XeroConfiguration
                    //{
                    //    ClientId = conn.xeroClientId,
                    //    ClientSecret = conn.xeroClientSecret,
                    //    CallbackUri = new Uri(conn.xeroCallbackUri),
                    //    Scope = conn.xeroScope,
                    //    State = conn.xeroState
                    //};
                    //if (utcTimeNow > xeroToken.ExpiresAtUtc)
                    //{
                    //    var client = new XeroClient(XeroConfig, httpClientFactory);
                    //    xeroToken = (XeroOAuth2Token)await client.RefreshAccessTokenAsync(xeroToken);

                    //    var txtXeroToken = TokenUtilities.StoreToken(xeroToken, conn.name);

                    //    var conn_ = Dal.Connection.UpdConnection(conn.idConnection, conn.name, conn.xeroClientId, conn.xeroClientSecret, conn.xeroCallbackUri, conn.xeroScope, conn.xeroState, txtXeroToken, conn.status, user.idUser);
                    //    conn.storedToken = conn_.storedToken;

                    //}
                    //accessToken = xeroToken.AccessToken;
                    //xeroTenantId = xeroToken.Tenants.ToList().Where(t => t.TenantId.ToString() == dataClient.idTenant.ToString()).Select(t => t.TenantId.ToString()).FirstOrDefault();
                    //if (string.IsNullOrEmpty(xeroTenantId))
                    //{
                    //    return Json(new { Cod = 0, Msg = "Could not get the " + dataClient.name + " client information" });
                    //}
                    //AccountingApi = new AccountingApi();

                    //var fiscal2 = Dal.Utils.GetFiscalYear();

                    ////get report profit and lost
                    //dt1 = fiscal.startReportFiscalYear;
                    //dt2 = fiscal.endReportFiscalYear;

                    //int? periods = null;
                    //if (fiscal2.qMonthsProgress > 1)
                    //{
                    //    //periods = (fiscal.qMonthsProgress - 1);
                    //    periods = fiscal2.qMonthsProgress;
                    //}
                    //if (fiscal2.qMonthsProgress >= 12)
                    //{
                    //    periods = 11;
                    //}

                    //response = await AccountingApi.GetReportProfitAndLossAsync(accessToken, xeroTenantId, null, null, periods, "MONTH", null, null, null, null, true, null);
                    //var reportPandL = response.Reports[0];

                    //if (reportPandL != null)
                    //{

                    //    sl.SetCellValue(1, 1, reportPandL.ReportTitles[0]);
                    //    sl.SetCellValue(2, 1, reportPandL.ReportTitles[1]);
                    //    sl.SetCellValue(3, 1, "For the month ended " + dt2.ToString("dd MMMM yyyy"));

                    //    style = sl.CreateStyle();
                    //    style.Font.FontName = "Arial";
                    //    style.Font.FontSize = 12;
                    //    style.Font.Bold = true;
                    //    sl.SetCellStyle(1, 1, 3, 1, style);

                    //    sl.MergeWorksheetCells(1, 1, 1, (fiscal.qMonthsProgress + 2));
                    //    sl.MergeWorksheetCells(2, 1, 2, (fiscal.qMonthsProgress + 2));
                    //    sl.MergeWorksheetCells(3, 1, 3, (fiscal.qMonthsProgress + 2));

                    //    style = sl.CreateStyle();
                    //    style.Alignment.Horizontal = HorizontalAlignmentValues.Center;
                    //    sl.SetCellStyle(1, 1, 3, (fiscal.qMonthsProgress + 2), style);

                    //    int fila = 5;
                    //    foreach (var f in reportPandL.Rows)
                    //    {
                    //        if (f.Cells != null)
                    //        {
                    //            int ultimacol = 0;
                    //            for (int g = 0; g < f.Cells.Count; g++)
                    //            {
                    //                sl.SetCellValue(fila, g + 1, f.Cells[g].Value);
                    //                ultimacol = g + 2;
                    //            }
                    //            sl.SetCellValue(fila, ultimacol, "YTD");

                    //            style = sl.CreateStyle();
                    //            style.SetFontBold(true);
                    //            sl.SetCellStyle(fila, 1, fila, ultimacol, style);

                    //            fila++;
                    //        }
                    //        else
                    //        {
                    //            int cantidad = 1;
                    //            foreach (var r in f.Rows)
                    //            {
                    //                if (cantidad == 1)
                    //                {
                    //                    switch (f.Title)
                    //                    {
                    //                        case "Income":
                    //                            plStartIncome = fila;
                    //                            break;
                    //                        case "Less Cost of Sales":
                    //                            plStartLessCostofSales = fila;
                    //                            break;
                    //                        case "Less Operating Expenses":
                    //                            plStartLessOperatingExpenses = fila;
                    //                            break;
                    //                        default:
                    //                            break;
                    //                    }

                    //                    sl.SetCellValue(fila, 1, f.Title);
                    //                    style = sl.CreateStyle();
                    //                    style.SetFontBold(true);
                    //                    sl.SetCellStyle(fila, 1, style);
                    //                    fila++;
                    //                }

                    //                int ultimacol = 0;
                    //                for (int g = 0; g < r.Cells.Count; g++)
                    //                {
                    //                    switch (r.Cells[0].Value)
                    //                    {
                    //                        case "Total Income":
                    //                            if (g != 0)
                    //                            {
                    //                                plEndTotalIncome = fila;
                    //                                sl.SetCellValue(fila, g + 1, "=SUM(" + Dal.Utils.ColumnTo(g) + (plStartIncome + 1) + ":" + Dal.Utils.ColumnTo(g) + (plEndTotalIncome - 1) + ")");
                    //                            }
                    //                            else
                    //                            {
                    //                                sl.SetCellValue(fila, g + 1, r.Cells[g].Value);
                    //                            }
                    //                            break;
                    //                        case "Total Cost of Sales":
                    //                            if (g != 0)
                    //                            {
                    //                                plEndTotalCostofSales = fila;
                    //                                sl.SetCellValue(fila, g + 1, "=SUM(" + Dal.Utils.ColumnTo(g) + (plStartLessCostofSales + 1) + ":" + Dal.Utils.ColumnTo(g) + (plEndTotalCostofSales - 1) + ")");
                    //                            }
                    //                            else
                    //                            {
                    //                                sl.SetCellValue(fila, g + 1, r.Cells[g].Value);
                    //                            }
                    //                            break;
                    //                        case "Total Operating Expenses":
                    //                            if (g != 0)
                    //                            {
                    //                                plEndTotalOperatingExpenses = fila;
                    //                                sl.SetCellValue(fila, g + 1, "=SUM(" + Dal.Utils.ColumnTo(g) + (plStartLessOperatingExpenses + 1) + ":" + Dal.Utils.ColumnTo(g) + (plEndTotalOperatingExpenses - 1) + ")");
                    //                            }
                    //                            else
                    //                            {
                    //                                sl.SetCellValue(fila, g + 1, r.Cells[g].Value);
                    //                            }
                    //                            break;
                    //                        case "Gross Profit":
                    //                            if (g != 0)
                    //                            {
                    //                                plGrossProfit = fila;
                    //                                sl.SetCellValue(fila, g + 1, "=(" + Dal.Utils.ColumnTo(g) + (plEndTotalIncome) + ")-(" + Dal.Utils.ColumnTo(g) + (plEndTotalCostofSales) + ")");
                    //                            }
                    //                            else
                    //                            {
                    //                                sl.SetCellValue(fila, g + 1, r.Cells[g].Value);
                    //                            }
                    //                            break;
                    //                        case "Net Profit":
                    //                            if (g != 0)
                    //                            {
                    //                                sl.SetCellValue(fila, g + 1, "=(" + Dal.Utils.ColumnTo(g) + (plGrossProfit) + ")+(0)-(" + Dal.Utils.ColumnTo(g) + (plEndTotalOperatingExpenses) + ")+(0)-(0)");
                    //                            }
                    //                            else
                    //                            {
                    //                                sl.SetCellValue(fila, g + 1, r.Cells[g].Value);
                    //                            }
                    //                            break;
                    //                        default:
                    //                            if (double.TryParse(r.Cells[g].Value.Replace(".", ","), out double num))
                    //                            {
                    //                                sl.SetCellValue(fila, g + 1, (num / 100));
                    //                            }
                    //                            else
                    //                            {
                    //                                sl.SetCellValue(fila, g + 1, r.Cells[g].Value);
                    //                            }
                    //                            break;
                    //                    }
                    //                    ultimacol = g + 2;
                    //                }

                    //                double suma = 0;

                    //                for (int g = (fiscal2.qMonthsProgress - fiscal.qMonthsProgress) + 2; g < r.Cells.Count; g++)
                    //                {
                    //                    if (double.TryParse(r.Cells[g].Value.Replace(".", ","), out double num))
                    //                    {
                    //                        suma += num;
                    //                    }
                    //                }
                    //                sl.SetCellValue(fila, ultimacol, (suma / 100));

                    //                if (1 == f.Rows.Count)
                    //                {
                    //                    style = sl.CreateStyle();
                    //                    style.SetFontBold(true);
                    //                    style.Border.BottomBorder.BorderStyle = BorderStyleValues.Double;
                    //                    sl.SetCellStyle(fila, 1, fila, ultimacol, style);

                    //                    style = sl.CreateStyle();
                    //                    style.FormatCode = "$#,##0.00;-$#,##0.00";
                    //                    sl.SetCellStyle(fila, 2, fila, ultimacol, style);
                    //                }
                    //                else
                    //                {
                    //                    if (cantidad == f.Rows.Count)
                    //                    {
                    //                        style = sl.CreateStyle();
                    //                        style.SetFontBold(true);
                    //                        style.Border.BottomBorder.BorderStyle = BorderStyleValues.Thin;
                    //                        sl.SetCellStyle(fila, 1, fila, ultimacol, style);

                    //                        style = sl.CreateStyle();
                    //                        style.FormatCode = "$#,##0.00;-$#,##0.00";
                    //                        sl.SetCellStyle(fila, 2, fila, ultimacol, style);
                    //                    }
                    //                    else
                    //                    {
                    //                        style = sl.CreateStyle();
                    //                        style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.MediumAquamarine, System.Drawing.Color.MediumAquamarine);
                    //                        sl.SetCellStyle(fila, 2, fila, ultimacol, style);

                    //                        style = sl.CreateStyle();
                    //                        style.FormatCode = "$#,##0.00;-$#,##0.00";
                    //                        sl.SetCellStyle(fila, 2, fila, ultimacol, style);
                    //                    }
                    //                }
                    //                cantidad++;
                    //                fila++;
                    //            }
                    //        }
                    //        fila++;
                    //    }

                    //    style = sl.CreateStyle();
                    //    style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.Gold, System.Drawing.Color.Gold);
                    //    sl.SetCellStyle((fila - 2), (fiscal.qMonthsProgress + 3), style);

                    //    sl.AutoFitColumn(1, 14);
                    //    style = sl.CreateStyle();
                    //    style.Font.FontName = "Arial";
                    //    style.Font.FontSize = 8;
                    //    sl.SetCellStyle(4, 1, fila, 14, style);

                    //    sl.DeleteColumn(2, (fiscal2.qMonthsProgress - fiscal.qMonthsProgress) + 1);
                    //}
                    //else
                    //{
                    //    sl.SetCellValue(3, 1, "Could not get Profit and Loss report for " + dataClient.name + " client");
                    //}


                    // Worksheet BS
                    var nameWorksheetBS = dataClient.idClient + "-" + Dal.Utils.quitarCaracteres(dataClient.name) + " Xero BS " + txtyear + " YTD";
                    sl.AddWorksheet(nameWorksheetBS);
                    ps.TabColor = colors[position];
                    sl.SetPageSettings(ps, nameWorksheetBS);

                    var bsStartBank = 0;
                    var bsEndTotalBank = 0;

                    var bsStartCurrentAssets = 0;
                    var bsEndTotalCurrentAssets = 0;

                    var bsStarFixedAssets = 0;
                    var bsEndTotalFixedAssets = 0;

                    var bsStartNoncurrentAssets = 0;
                    var bsEndTotalNoncurrentAssets = 0;

                    var bsTotalAssets = 0;

                    var bsStartCurrentLiabilities = 0;
                    var bsEndTotalCurrentLiabilities = 0;

                    var bsStartNonCurrentLiabilities = 0;
                    var bsEndTotalNonCurrentLiabilities = 0;

                    var bsTotalLiabilities = 0;

                    var bsNetAssets = 0;

                    var bsStartEquity = 0;
                    var bsEndTotalEquity = 0;

                    //get report BS actual v/s past year

                    //get report BS past year 
                    conn = Dal.Connection.GetConnection(dataClient.idConnection.Value);
                    xeroToken = TokenUtilities.GetStoredToken(conn.storedToken, conn.name);
                    utcTimeNow = DateTime.UtcNow;
                    serviceProvider = new ServiceCollection().AddHttpClient().BuildServiceProvider();
                    httpClientFactory = serviceProvider.GetService<IHttpClientFactory>();
                    XeroConfig = new XeroConfiguration
                    {
                        ClientId = conn.xeroClientId,
                        ClientSecret = conn.xeroClientSecret,
                        CallbackUri = new Uri(conn.xeroCallbackUri),
                        Scope = conn.xeroScope,
                        State = conn.xeroState
                    };
                    if (utcTimeNow > xeroToken.ExpiresAtUtc)
                    {
                        var client = new XeroClient(XeroConfig, httpClientFactory);
                        xeroToken = (XeroOAuth2Token)await client.RefreshAccessTokenAsync(xeroToken);
                        var txtXeroToken = TokenUtilities.StoreToken(xeroToken, conn.name);
                        var conn_ = Dal.Connection.UpdConnection(conn.idConnection, conn.name, conn.xeroClientId, conn.xeroClientSecret, conn.xeroCallbackUri, conn.xeroScope, conn.xeroState, txtXeroToken, conn.status, user.idUser);
                        conn.storedToken = conn_.storedToken;
                    }
                    accessToken = xeroToken.AccessToken;
                    xeroTenantId = xeroToken.Tenants.ToList().Where(t => t.TenantId.ToString() == dataClient.idTenant.ToString()).Select(t => t.TenantId.ToString()).FirstOrDefault();
                    if (string.IsNullOrEmpty(xeroTenantId))
                    {
                        return Json(new { Cod = 0, Msg = "Could not get the " + dataClient.name + " client information" });
                    }
                    AccountingApi = new AccountingApi();

                    string endfiscalyear = fiscal.endFiscalYear.ToString("yyyy-MM-dd");
                    response = await AccountingApi.GetReportBalanceSheetAsync(accessToken, xeroTenantId, endfiscalyear, 1, "YEAR", null, null, true, null);
                    var reportBSPast = response.Reports[0];

                    var listaBS = new List<ApiResponse>();

                    if (reportBSPast != null)
                    {
                        var index = 1;
                        foreach (var f in reportBSPast.Rows)
                        {
                            if (f.RowType.ToString() == "Header")
                            {
                                var apiResponse = new ApiResponse();
                                apiResponse.index = index;
                                apiResponse.rowType = "Header";
                                apiResponse.label = f.Cells[0].Value;
                                //apiResponse.actualValue = f.Cells[1].Value;
                                apiResponse.pastValue = f.Cells[2].Value;
                                listaBS.Add(apiResponse);
                            }
                            else
                            {
                                if (f.Rows.Count != 0)
                                {
                                    foreach (var r in f.Rows)
                                    {
                                        var apiResponse = new ApiResponse();
                                        apiResponse.index = index;
                                        apiResponse.rowType = f.RowType.ToString();
                                        apiResponse.title = f.Title.ToString();
                                        if (r.Cells != null)
                                        {
                                            apiResponse.rowTypeIn = r.RowType.ToString();
                                            apiResponse.label = r.Cells[0].Value;
                                            //apiResponse.actualValue = r.Cells[1].Value;
                                            apiResponse.pastValue = r.Cells[2].Value;
                                        }
                                        listaBS.Add(apiResponse);
                                    }
                                }
                                else
                                {
                                    var apiResponse = new ApiResponse();
                                    apiResponse.index = index;
                                    apiResponse.rowType = f.RowType.ToString();
                                    apiResponse.title = f.Title.ToString();
                                    apiResponse.label = f.Title.ToString();
                                    listaBS.Add(apiResponse);
                                }
                            }
                            index++;
                        }
                    }

                    //get report BS actual
                    conn = Dal.Connection.GetConnection(dataClient.idConnection.Value);
                    xeroToken = TokenUtilities.GetStoredToken(conn.storedToken, conn.name);
                    utcTimeNow = DateTime.UtcNow;
                    serviceProvider = new ServiceCollection().AddHttpClient().BuildServiceProvider();
                    httpClientFactory = serviceProvider.GetService<IHttpClientFactory>();
                    XeroConfig = new XeroConfiguration
                    {
                        ClientId = conn.xeroClientId,
                        ClientSecret = conn.xeroClientSecret,
                        CallbackUri = new Uri(conn.xeroCallbackUri),
                        Scope = conn.xeroScope,
                        State = conn.xeroState
                    };
                    if (utcTimeNow > xeroToken.ExpiresAtUtc)
                    {
                        var client = new XeroClient(XeroConfig, httpClientFactory);
                        xeroToken = (XeroOAuth2Token)await client.RefreshAccessTokenAsync(xeroToken);

                        var txtXeroToken = TokenUtilities.StoreToken(xeroToken, conn.name);

                        var conn_ = Dal.Connection.UpdConnection(conn.idConnection, conn.name, conn.xeroClientId, conn.xeroClientSecret, conn.xeroCallbackUri, conn.xeroScope, conn.xeroState, txtXeroToken, conn.status, user.idUser);
                        conn.storedToken = conn_.storedToken;
                    }
                    accessToken = xeroToken.AccessToken;
                    xeroTenantId = xeroToken.Tenants.ToList().Where(t => t.TenantId.ToString() == dataClient.idTenant.ToString()).Select(t => t.TenantId.ToString()).FirstOrDefault();
                    if (string.IsNullOrEmpty(xeroTenantId))
                    {
                        return Json(new { Cod = 0, Msg = "Could not get the " + dataClient.name + " client information" });
                    }
                    AccountingApi = new AccountingApi();

                    string endreportfiscalyear = fiscal.endReportFiscalYear.ToString("yyyy-MM-dd");
                    response = await AccountingApi.GetReportBalanceSheetAsync(accessToken, xeroTenantId, endreportfiscalyear, null, null, null, null, true, null);
                    var reportBSActual = response.Reports[0];

                    var listaBS2 = new List<ApiResponse>();

                    if (reportBSActual != null)
                    {
                        var index = 1;
                        foreach (var f in reportBSActual.Rows)
                        {
                            if (f.RowType.ToString() == "Header")
                            {
                                var apiResponse = new ApiResponse();
                                apiResponse.index = index;
                                apiResponse.rowType = "Header";
                                apiResponse.label = f.Cells[0].Value;
                                apiResponse.actualValue = f.Cells[1].Value;
                                listaBS2.Add(apiResponse);
                            }
                            else
                            {
                                if (f.Rows.Count != 0)
                                {
                                    foreach (var r in f.Rows)
                                    {
                                        var apiResponse = new ApiResponse();
                                        apiResponse.index = index;
                                        apiResponse.rowType = f.RowType.ToString();
                                        apiResponse.title = f.Title.ToString();
                                        if (r.Cells != null)
                                        {
                                            apiResponse.rowTypeIn = r.RowType.ToString();
                                            apiResponse.label = r.Cells[0].Value;
                                            apiResponse.actualValue = r.Cells[1].Value;
                                        }
                                        listaBS2.Add(apiResponse);
                                    }
                                }
                                else
                                {
                                    var apiResponse = new ApiResponse();
                                    apiResponse.index = index;
                                    apiResponse.rowType = f.RowType.ToString();
                                    apiResponse.title = f.Title.ToString();
                                    apiResponse.label = f.Title.ToString();
                                    listaBS2.Add(apiResponse);
                                }
                            }
                            index++;
                        }
                    }

                    foreach (var bs in listaBS)
                    {
                        bs.actualValue = "0";
                        foreach (var bs2 in listaBS2)
                        {
                            if (bs.rowType == bs2.rowType && bs.title == bs2.title && bs.rowTypeIn == bs2.rowTypeIn && bs.label == bs2.label)
                            {
                                bs.actualValue = bs2.actualValue;
                            }
                        }
                    }

                    var listBSFinal = listaBS;

                    if (listBSFinal != null)
                    {
                        sl.SetCellValue(1, 1, "Balance Sheet");
                        sl.SetCellValue(2, 1, dataClient.name);
                        sl.SetCellValue(3, 1, "As at " + fiscal.endReportFiscalYear.ToString("dd MMMM yyyy"));

                        sl.MergeWorksheetCells(1, 1, 1, 3);
                        sl.MergeWorksheetCells(2, 1, 2, 3);
                        sl.MergeWorksheetCells(3, 1, 3, 3);

                        sl.SetCellValue(5, 2, fiscal.endReportFiscalYear.ToString("dd MMM yyyy"));
                        sl.SetCellValue(5, 3, fiscal.endFiscalYear.AddYears(-1).ToString("dd MMM yyyy"));

                        style = sl.CreateStyle();
                        style.Font.Bold = true;
                        style.Alignment.Horizontal = HorizontalAlignmentValues.Center;
                        sl.SetCellStyle(1, 1, 5, 3, style);

                        int fila = 6;
                        var index = 1;
                        foreach (var bs in listBSFinal)
                        {
                            if (!string.IsNullOrEmpty(bs.label))
                            {
                                if (index != bs.index)
                                {
                                    if (!string.IsNullOrEmpty(bs.title))
                                    {
                                        if (bs.title.Equals(bs.label))
                                        {
                                            sl.SetCellValue(fila, 1, bs.title);
                                            style = sl.CreateStyle();
                                            style.Font.Bold = true;
                                            sl.SetCellStyle(fila, 1, style);
                                        }
                                        else
                                        {
                                            switch (bs.title)
                                            {
                                                case "Bank":
                                                    bsStartBank = fila;
                                                    break;
                                                case "Current Assets":
                                                    bsStartCurrentAssets = fila;
                                                    break;
                                                case "Fixed Assets":
                                                    bsStarFixedAssets = fila;
                                                    break;
                                                case "Non-current Assets":
                                                    bsStartNoncurrentAssets = fila;
                                                    break;
                                                case "Current Liabilities":
                                                    bsStartCurrentLiabilities = fila;
                                                    break;
                                                case "Non-Current Liabilities":
                                                    bsStartNonCurrentLiabilities = fila;
                                                    break;
                                                case "Equity":
                                                    bsStartEquity = fila;
                                                    break;
                                                default:
                                                    break;
                                            }

                                            sl.SetCellValue(fila, 1, "   " + bs.title);
                                            style = sl.CreateStyle();
                                            style.Font.Bold = true;
                                            sl.SetCellStyle(fila, 1, style);
                                            fila++;
                                        }
                                    }
                                }




                                switch (bs.label)
                                {
                                    case "Total Bank":
                                        bsEndTotalBank = fila;
                                        sl.SetCellValue(fila, 2, "=SUM(B" + (bsStartBank + 1) + ":B" + (bsEndTotalBank - 1) + ")");
                                        sl.SetCellValue(fila, 3, "=SUM(C" + (bsStartBank + 1) + ":C" + (bsEndTotalBank - 1) + ")");
                                        break;
                                    case "Total Current Assets":
                                        bsEndTotalCurrentAssets = fila;
                                        sl.SetCellValue(fila, 2, "=SUM(B" + (bsStartCurrentAssets + 1) + ":B" + (bsEndTotalCurrentAssets - 1) + ")");
                                        sl.SetCellValue(fila, 3, "=SUM(C" + (bsStartCurrentAssets + 1) + ":C" + (bsEndTotalCurrentAssets - 1) + ")");
                                        break;
                                    case "Total Fixed Assets":
                                        bsEndTotalFixedAssets = fila;
                                        sl.SetCellValue(fila, 2, "=SUM(B" + (bsStarFixedAssets + 1) + ":B" + (bsEndTotalFixedAssets - 1) + ")");
                                        sl.SetCellValue(fila, 3, "=SUM(C" + (bsStarFixedAssets + 1) + ":C" + (bsEndTotalFixedAssets - 1) + ")");
                                        break;
                                    case "Total Non-current Assets":
                                        bsEndTotalNoncurrentAssets = fila;
                                        sl.SetCellValue(fila, 2, "=SUM(B" + (bsStartNoncurrentAssets + 1) + ":B" + (bsEndTotalNoncurrentAssets - 1) + ")");
                                        sl.SetCellValue(fila, 3, "=SUM(C" + (bsStartNoncurrentAssets + 1) + ":C" + (bsEndTotalNoncurrentAssets - 1) + ")");
                                        break;
                                    case "Total Assets":
                                        bsTotalAssets = fila;

                                        var totalbank = bsEndTotalBank == 0 ? "0" : "B" + bsEndTotalBank;
                                        var TotalCurrentAssets = bsEndTotalCurrentAssets == 0 ? "0" : "B" + bsEndTotalCurrentAssets;
                                        var TotalFixedAssets = bsEndTotalFixedAssets == 0 ? "0" : "B" + bsEndTotalFixedAssets;
                                        var TotalNoncurrentAssets = bsEndTotalNoncurrentAssets == 0 ? "0" : "B" + bsEndTotalNoncurrentAssets;
                                        sl.SetCellValue(fila, 2, "=(0+(0)+(" + totalbank + ")+(" + TotalCurrentAssets + ")+(" + TotalFixedAssets + ")+(" + TotalNoncurrentAssets + "))-(0)");

                                        totalbank = bsEndTotalBank == 0 ? "0" : "C" + bsEndTotalBank;
                                        TotalCurrentAssets = bsEndTotalCurrentAssets == 0 ? "0" : "C" + bsEndTotalCurrentAssets;
                                        TotalFixedAssets = bsEndTotalFixedAssets == 0 ? "0" : "C" + bsEndTotalFixedAssets;
                                        TotalNoncurrentAssets = bsEndTotalNoncurrentAssets == 0 ? "0" : "C" + bsEndTotalNoncurrentAssets;
                                        sl.SetCellValue(fila, 3, "=(0+(0)+(" + totalbank + ")+(" + TotalCurrentAssets + ")+(" + TotalFixedAssets + ")+(" + TotalNoncurrentAssets + "))-(0)");
                                        break;
                                    case "Total Current Liabilities":
                                        bsEndTotalCurrentLiabilities = fila;
                                        sl.SetCellValue(fila, 2, "=SUM(B" + (bsStartCurrentLiabilities + 1) + ":B" + (bsEndTotalCurrentLiabilities - 1) + ")");
                                        sl.SetCellValue(fila, 3, "=SUM(C" + (bsStartCurrentLiabilities + 1) + ":C" + (bsEndTotalCurrentLiabilities - 1) + ")");
                                        break;
                                    case "Total Non-Current Liabilities":
                                        bsEndTotalNonCurrentLiabilities = fila;
                                        sl.SetCellValue(fila, 2, "=SUM(B" + (bsStartNonCurrentLiabilities + 1) + ":B" + (bsEndTotalNonCurrentLiabilities - 1) + ")");
                                        sl.SetCellValue(fila, 3, "=SUM(C" + (bsStartNonCurrentLiabilities + 1) + ":C" + (bsEndTotalNonCurrentLiabilities - 1) + ")");
                                        break;
                                    case "Total Liabilities":
                                        bsTotalLiabilities = fila;
                                        var totalCurrentLiabilities = bsEndTotalCurrentLiabilities == 0 ? "0" : "B" + bsEndTotalCurrentLiabilities;
                                        var totalNonCurrentLiabilities = bsEndTotalNonCurrentLiabilities == 0 ? "0" : "B" + bsEndTotalNonCurrentLiabilities;
                                        sl.SetCellValue(fila, 2, "=(0+(0)+(" + totalCurrentLiabilities + ")+(" + totalNonCurrentLiabilities + "))-(0)");

                                        totalCurrentLiabilities = bsEndTotalCurrentLiabilities == 0 ? "0" : "C" + bsEndTotalCurrentLiabilities;
                                        totalNonCurrentLiabilities = bsEndTotalNonCurrentLiabilities == 0 ? "0" : "C" + bsEndTotalNonCurrentLiabilities;
                                        sl.SetCellValue(fila, 3, "=(0+(0)+(" + totalCurrentLiabilities + ")+(" + totalNonCurrentLiabilities + "))-(0)");
                                        break;
                                    case "Net Assets":
                                        bsNetAssets = fila;
                                        var totalAssets = bsTotalAssets == 0 ? "0" : "B" + bsTotalAssets;
                                        var totalLiabilities = bsTotalLiabilities == 0 ? "0" : "B" + bsTotalLiabilities;
                                        sl.SetCellValue(fila, 2, "=(" + totalAssets + ")-(" + totalLiabilities + ")");

                                        totalAssets = bsTotalAssets == 0 ? "0" : "C" + bsTotalAssets;
                                        totalLiabilities = bsTotalLiabilities == 0 ? "0" : "C" + bsTotalLiabilities;
                                        sl.SetCellValue(fila, 3, "=(" + totalAssets + ")-(" + totalLiabilities + ")");
                                        break;
                                    case "Total Equity":
                                        bsEndTotalEquity = fila;
                                        sl.SetCellValue(fila, 2, "=SUM(B" + (bsStartEquity + 1) + ":B" + (bsEndTotalEquity - 1) + ")");
                                        sl.SetCellValue(fila, 3, "=SUM(C" + (bsStartEquity + 1) + ":C" + (bsEndTotalEquity - 1) + ")");
                                        break;
                                    default:
                                        if (bs.actualValue != null)
                                        {
                                            if (double.TryParse(bs.actualValue.Replace(".", ","), out double num))
                                            {
                                                sl.SetCellValue(fila, 2, (num / 100));

                                                style = sl.CreateStyle();
                                                style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.MediumAquamarine, System.Drawing.Color.MediumAquamarine);
                                                sl.SetCellStyle(fila, 2, style);
                                            }
                                            else
                                            {
                                                sl.SetCellValue(fila, 2, bs.actualValue);
                                            }
                                        }
                                        else
                                        {
                                            if (!bs.title.Equals(bs.label))
                                            {
                                                sl.SetCellValue(fila, 2, "-");
                                                style = sl.CreateStyle();
                                                style.Alignment.Horizontal = HorizontalAlignmentValues.Center;
                                                style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.MediumAquamarine, System.Drawing.Color.MediumAquamarine);
                                                sl.SetCellStyle(fila, 2, style);
                                            }
                                        }

                                        if (bs.pastValue != null)
                                        {
                                            if (double.TryParse(bs.pastValue.Replace(".", ","), out double num2))
                                            {
                                                sl.SetCellValue(fila, 3, (num2 / 100));

                                                style = sl.CreateStyle();
                                                style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.MediumAquamarine, System.Drawing.Color.MediumAquamarine);
                                                sl.SetCellStyle(fila, 3, style);
                                            }
                                        }
                                        else
                                        {
                                            if (!bs.title.Equals(bs.label))
                                            {
                                                sl.SetCellValue(fila, 3, "-");
                                                style = sl.CreateStyle();
                                                style.Alignment.Horizontal = HorizontalAlignmentValues.Center;
                                                style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.MediumAquamarine, System.Drawing.Color.MediumAquamarine);
                                                sl.SetCellStyle(fila, 3, style);
                                            }
                                        }
                                        break;
                                }

                                if (bs.rowTypeIn != null)
                                {
                                    if (bs.rowTypeIn.ToLower().Equals("summaryrow"))
                                    {
                                        if (string.IsNullOrEmpty(bs.title))
                                        {
                                            sl.SetCellValue(fila, 1, bs.label);

                                            style = sl.CreateStyle();
                                            style.Font.Bold = true;
                                            sl.SetCellStyle(fila, 1, style);

                                            style = sl.CreateStyle();
                                            style.Border.BottomBorder.BorderStyle = BorderStyleValues.Double;
                                            style.Fill.SetPattern(PatternValues.None, System.Drawing.Color.White, System.Drawing.Color.White);
                                            sl.SetCellStyle(fila, 1, fila, 3, style);
                                        }
                                        else
                                        {
                                            sl.SetCellValue(fila, 1, "   " + bs.label);

                                            style = sl.CreateStyle();
                                            style.Font.Bold = true;
                                            sl.SetCellStyle(fila, 1, style);

                                            style = sl.CreateStyle();
                                            style.Border.BottomBorder.BorderStyle = BorderStyleValues.Thin;
                                            style.Fill.SetPattern(PatternValues.None, System.Drawing.Color.White, System.Drawing.Color.White);
                                            sl.SetCellStyle(fila, 1, fila, 3, style);
                                        }


                                        fila++;
                                    }
                                    else
                                    {
                                        if (string.IsNullOrEmpty(bs.title))
                                        {
                                            sl.SetCellValue(fila, 1, bs.label);

                                            style = sl.CreateStyle();
                                            style.Font.Bold = true;
                                            sl.SetCellStyle(fila, 1, style);

                                            style = sl.CreateStyle();
                                            style.Border.BottomBorder.BorderStyle = BorderStyleValues.Double;
                                            style.Fill.SetPattern(PatternValues.None, System.Drawing.Color.White, System.Drawing.Color.White);
                                            sl.SetCellStyle(fila, 1, fila, 3, style);

                                            fila++;
                                        }
                                        else
                                        {
                                            sl.SetCellValue(fila, 1, "   " + bs.label);
                                        }
                                    }
                                }
                                fila++;
                            }
                            index = bs.index;
                        }

                        style = sl.CreateStyle();
                        style.FormatCode = "$#,##0.00;-$#,##0.00";
                        sl.SetCellStyle(6, 2, fila, 3, style);

                        style = sl.CreateStyle();
                        style.Font.FontName = "Arial";
                        style.Font.FontSize = 12;
                        sl.SetCellStyle(1, 1, 3, 3, style);

                        sl.AutoFitColumn(1, 3);
                        style = sl.CreateStyle();
                        style.Font.FontName = "Arial";
                        style.Font.FontSize = 8;
                        sl.SetCellStyle(4, 1, fila, 3, style);

                    }
                    else
                    {
                        sl.SetCellValue(5, 1, "Could not get Balance sheet report for " + dataClient.name + " client");
                    }



                    // Worksheet Payroll
                    var nameWorksheetPayroll = dataClient.idClient + "-" + Dal.Utils.quitarCaracteres(dataClient.name) + " Xero PR " + txtyear + " YTD";
                    sl.AddWorksheet(nameWorksheetPayroll);
                    ps.TabColor = colors[position];
                    sl.SetPageSettings(ps, nameWorksheetPayroll);

                    //get report Payroll
                    conn = Dal.Connection.GetConnection(dataClient.idConnection.Value);
                    xeroToken = TokenUtilities.GetStoredToken(conn.storedToken, conn.name);
                    utcTimeNow = DateTime.UtcNow;
                    serviceProvider = new ServiceCollection().AddHttpClient().BuildServiceProvider();
                    httpClientFactory = serviceProvider.GetService<IHttpClientFactory>();
                    XeroConfig = new XeroConfiguration
                    {
                        ClientId = conn.xeroClientId,
                        ClientSecret = conn.xeroClientSecret,
                        CallbackUri = new Uri(conn.xeroCallbackUri),
                        Scope = conn.xeroScope,
                        State = conn.xeroState
                    };
                    if (utcTimeNow > xeroToken.ExpiresAtUtc)
                    {
                        var client = new XeroClient(XeroConfig, httpClientFactory);
                        xeroToken = (XeroOAuth2Token)await client.RefreshAccessTokenAsync(xeroToken);

                        var txtXeroToken = TokenUtilities.StoreToken(xeroToken, conn.name);

                        var conn_ = Dal.Connection.UpdConnection(conn.idConnection, conn.name, conn.xeroClientId, conn.xeroClientSecret, conn.xeroCallbackUri, conn.xeroScope, conn.xeroState, txtXeroToken, conn.status, user.idUser);
                        conn.storedToken = conn_.storedToken;

                    }
                    accessToken = xeroToken.AccessToken;
                    xeroTenantId = xeroToken.Tenants.ToList().Where(t => t.TenantId.ToString() == dataClient.idTenant.ToString()).Select(t => t.TenantId.ToString()).FirstOrDefault();
                    if (string.IsNullOrEmpty(xeroTenantId))
                    {
                        return Json(new { Cod = 0, Msg = "Could not get the " + dataClient.name + " client information" });
                    }
                    var rtn = new Utilities.PayrollClass.Rootobject();
                    var clientehttp = new RestClient("https://api.xero.com/payroll.xro/1.0/PayRuns");
                    clientehttp.Timeout = -1;
                    var request = new RestRequest(Method.GET);
                    request.AddHeader("Authorization", "Bearer " + accessToken);
                    request.AddHeader("xero-tenant-id", xeroTenantId);
                    request.AddHeader("Accept", "application/json");

                    var responses = clientehttp.Execute<Utilities.PayrollClass.Rootobject>(request);

                    sl.SetCellValue(1, 1, "Payroll Employee Summary");
                    sl.SetCellValue(2, 1, "From " + fiscal.startFiscalYear.ToString("dd MMMM yyyy") + " to " + fiscal.endReportFiscalYear.ToString("dd MMMM yyyy"));
                    sl.SetCellValue(3, 1, dataClient.name);

                    style = sl.CreateStyle();
                    style.Font.FontName = "Arial";
                    style.Font.FontSize = 12;
                    style.Font.Bold = true;
                    sl.SetCellStyle(1, 1, 3, 1, style);

                    sl.MergeWorksheetCells(1, 1, 1, 7);
                    sl.MergeWorksheetCells(2, 1, 2, 7);
                    sl.MergeWorksheetCells(3, 1, 3, 7);

                    style = sl.CreateStyle();
                    style.Alignment.Horizontal = HorizontalAlignmentValues.Center;
                    sl.SetCellStyle(1, 1, 3, 7, style);

                    if (responses.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        rtn = JsonConvert.DeserializeObject<Utilities.PayrollClass.Rootobject>(responses.Content);

                        conn = Dal.Connection.GetConnection(dataClient.idConnection.Value);

                        xeroToken = TokenUtilities.GetStoredToken(conn.storedToken, conn.name);

                        utcTimeNow = DateTime.UtcNow;

                        serviceProvider = new ServiceCollection().AddHttpClient().BuildServiceProvider();
                        httpClientFactory = serviceProvider.GetService<IHttpClientFactory>();

                        XeroConfig = new XeroConfiguration
                        {
                            ClientId = conn.xeroClientId,
                            ClientSecret = conn.xeroClientSecret,
                            CallbackUri = new Uri(conn.xeroCallbackUri),
                            Scope = conn.xeroScope,
                            State = conn.xeroState
                        };

                        if (utcTimeNow > xeroToken.ExpiresAtUtc)
                        {
                            var client = new XeroClient(XeroConfig, httpClientFactory);
                            xeroToken = (XeroOAuth2Token)await client.RefreshAccessTokenAsync(xeroToken);

                            var txtXeroToken = TokenUtilities.StoreToken(xeroToken, conn.name);

                            var conn_ = Dal.Connection.UpdConnection(conn.idConnection, conn.name, conn.xeroClientId, conn.xeroClientSecret, conn.xeroCallbackUri, conn.xeroScope, conn.xeroState, txtXeroToken, conn.status, user.idUser);
                            conn.storedToken = conn_.storedToken;

                        }

                        accessToken = xeroToken.AccessToken;
                        xeroTenantId = xeroToken.Tenants.ToList().Where(t => t.TenantId.ToString() == dataClient.idTenant.ToString()).Select(t => t.TenantId.ToString()).FirstOrDefault();

                        if (string.IsNullOrEmpty(xeroTenantId))
                        {
                            return Json(new { Cod = 0, Msg = "Could not get the " + dataClient.name + " client information" });
                        }

                        var payslipListComplete = new List<Utilities.PayrollClass.Payslip>();

                        var listPayRuns = rtn.PayRuns.Where(p => p.PayRunStatus == "POSTED").Where(s => s.PaymentDate >= fiscal.startFiscalYear).Where(s => s.PaymentDate <= fiscal.endReportFiscalYear).Select(r => r).ToList();

                        if (listPayRuns.Count > 0)
                        {

                            sl.SetCellValue(5, 1, "Employee");
                            sl.SetCellValue(5, 2, "Earnings");
                            sl.SetCellValue(5, 3, "Deductions");
                            sl.SetCellValue(5, 4, "Tax");
                            sl.SetCellValue(5, 5, "Super");
                            sl.SetCellValue(5, 6, "Net Pay");
                            sl.SetCellValue(5, 7, "Reimbursements");

                            style = sl.CreateStyle();
                            style.SetFontBold(true);
                            style.Font.FontName = "Arial";
                            style.Font.FontSize = 8;
                            sl.SetCellStyle(5, 1, 5, 7, style);

                            int fila = 6;


                            foreach (var payrun in listPayRuns)
                            {
                                var payrun_ = new Utilities.PayrollClass.Rootobject();

                                var url3 = "https://api.xero.com/payroll.xro/1.0/PayRuns/" + payrun.PayRunID;

                                var clientehttp3 = new RestClient(url3);
                                clientehttp3.Timeout = -1;

                                var request3 = new RestRequest(Method.GET);
                                request3.AddHeader("Authorization", "Bearer " + accessToken);
                                request3.AddHeader("xero-tenant-id", xeroTenantId);
                                request3.AddHeader("Accept", "application/json");

                                var response3 = clientehttp3.Execute<Utilities.PayrollClass.Rootobject>(request3);

                                var return3 = new Utilities.PayrollClass.Rootobject();

                                if (response3.StatusCode == System.Net.HttpStatusCode.OK)
                                {
                                    return3 = JsonConvert.DeserializeObject<Utilities.PayrollClass.Rootobject>(response3.Content);

                                    var payslipList = return3.PayRuns.FirstOrDefault().Payslips.ToList();
                                    foreach (var psl in payslipList)
                                    {
                                        payslipListComplete.Add(psl);
                                    }
                                }
                            }

                            foreach (var pss in payslipListComplete)
                            {
                                pss.PayslipID = "";
                            }

                            //var payslipListComplete_ = payslipListComplete.GroupBy(a => new
                            //{
                            //    a.EmployeeID,
                            //    a.PayslipID,
                            //    a.FirstName,
                            //    a.LastName,
                            //    a.Wages,
                            //    a.Deductions,
                            //    a.Tax,
                            //    a.Super,
                            //    a.Reimbursements,
                            //    a.NetPay,
                            //    a.UpdatedDateUTC
                            //}).Select(o => o.FirstOrDefault()).ToList();

                            var payslipGroupList = payslipListComplete.GroupBy(g => new
                            {
                                g.FirstName,
                                g.LastName
                            }).Select(group => new
                            {
                                Name = group.Key.FirstName + " " + group.Key.LastName,
                                Deductions = group.Sum(c => c.Deductions),
                                Tax = group.Sum(c => c.Tax),
                                Super = group.Sum(a => a.Super),
                                NetPay = group.Sum(c => c.NetPay),
                                Reimbursements = group.Sum(c => c.Reimbursements)
                            }).OrderBy(p => p.Name).ToList();


                            foreach (var psgc in payslipGroupList)
                            {
                                sl.SetCellValue(fila, 1, psgc.Name);
                                sl.SetCellValue(fila, 2, (psgc.Deductions + psgc.Tax + psgc.NetPay));
                                sl.SetCellValue(fila, 3, psgc.Deductions);
                                sl.SetCellValue(fila, 4, psgc.Tax);
                                sl.SetCellValue(fila, 5, psgc.Super);
                                sl.SetCellValue(fila, 6, psgc.NetPay);
                                sl.SetCellValue(fila, 7, psgc.Reimbursements);
                                fila++;
                            }

                            style = sl.CreateStyle();
                            style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.MediumAquamarine, System.Drawing.Color.MediumAquamarine);
                            style.Font.FontName = "Arial";
                            style.Font.FontSize = 8;
                            style.FormatCode = "$#,##0.00;-$#,##0.00";
                            sl.SetCellStyle(6, 2, fila - 1, 7, style);

                            //if (payslipGroupList != null)
                            //{
                            //    sl.SetCellValue(fila, 1, "");
                            //    sl.SetCellValue(fila, 2, (payslipGroupList.Sum(p => p.Deductions + p.Tax + p.NetPay)));
                            //    sl.SetCellValue(fila, 3, (payslipGroupList.Sum(p => p.Deductions)));
                            //    sl.SetCellValue(fila, 4, (payslipGroupList.Sum(p => p.Tax)));
                            //    sl.SetCellValue(fila, 5, (payslipGroupList.Sum(p => p.Super)));
                            //    sl.SetCellValue(fila, 6, (payslipGroupList.Sum(p => p.NetPay)));
                            //    sl.SetCellValue(fila, 7, (payslipGroupList.Sum(p => p.Reimbursements)));
                            //    fila++;
                            //}

                            if (payslipGroupList != null)
                            {
                                var rowToSum = fila - 1;
                                sl.SetCellValue(fila, 1, "");
                                sl.SetCellValue(fila, 2, "=SUM(B6:B" + rowToSum + ")");
                                sl.SetCellValue(fila, 3, "=SUM(C6:C" + rowToSum + ")");
                                sl.SetCellValue(fila, 4, "=SUM(D6:D" + rowToSum + ")");
                                sl.SetCellValue(fila, 5, "=SUM(E6:E" + rowToSum + ")");
                                sl.SetCellValue(fila, 6, "=SUM(F6:F" + rowToSum + ")");
                                sl.SetCellValue(fila, 7, "=SUM(G6:G" + rowToSum + ")");
                            }

                            style = sl.CreateStyle();
                            style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.Gold, System.Drawing.Color.Gold);
                            sl.SetCellStyle(fila, 2, style);
                            sl.SetCellStyle(fila, 5, style);

                            sl.AutoFitColumn(1, 7);

                            style = sl.CreateStyle();
                            style.SetFontBold(true);
                            style.Font.FontName = "Arial";
                            style.Font.FontSize = 8;
                            style.FormatCode = "$#,##0.00;-$#,##0.00";
                            style.Border.BottomBorder.BorderStyle = BorderStyleValues.Double;
                            sl.SetCellStyle(fila, 2, fila, 7, style);

                            sl.AutoFitColumn(1, 7);
                        }
                        else
                        {
                            sl.SetCellValue(5, 1, "No results found in the Payroll Employee Summary report for " + dataClient.name + " client");
                        }
                    }
                    else
                    {
                        sl.SetCellValue(5, 1, "Could not get Payroll Employee Summary report for " + dataClient.name + " client [" + responses.ErrorMessage + "]");
                    }

                    position++;
                }


                if (Dal.Binnacle.AddBinnacle(user.idCompany, 1, clientids.Length, "report file generation") == 0)
                {
                    return Json(new { Cod = 2, Msg = "An error has occurred on the log record." });
                }

                MemoryStream mr = new MemoryStream();
                sl.SaveAs(mr);
                var handler = Guid.NewGuid().ToString();
                TempData[handler] = mr.GetBuffer();
                return Json(new { Cod = 1, Msg = "", Handler = handler, Group = groupid });
            }
            catch (Exception ex)
            {
                return Json(new { Cod = 0, Msg = ex.Message });
            }
        }

        public virtual ActionResult Download(string Handler, int? Group = null)
        {
            if (TempData[Handler] != null)
            {
                var groupName = "Custom";
                if (Group != null)
                {
                    var group = Dal.Group.GetGroup(Group.Value);
                    groupName = group != null ? group.name : "Custom";
                }

                var nombreArchivo = Dal.Utils.GetYearFiscalYear() + " Tax & Cash Flow Planning " + groupName + " Group.xlsx";

                byte[] data = TempData[Handler] as byte[];
                return File(data, "application/vnd.ms-excel", nombreArchivo);
            }
            else
            {
                return new EmptyResult();
            }
        }

    }
}