﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal
{
    public class User
    {
        /// <summary>
        /// Método para obtener un listado de objetos User.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-USE-001
        /// </summary>
        /// <returns>Retorna una lista de objetos User</returns>
        public static List<Dao.User> GetUsers()
        {
            try
            {
                return new Dao.Get.User().GetUsers().ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para validar la existencia de un nombre de usuario.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-USE-002
        /// </summary>
        /// <param name="name">nombre de usuario</param>
        /// <param name="id">identificador del usuario a descartar</param>
        /// <returns>Retorna un bool con el resultado de la operacion. True si existe - False no existe</returns>
        public static bool ExistUser(string name, int id = 0)
        {
            try
            {
                var users = GetUsers();

                if (id == 0)
                {
                    users = users.Where(g => g.username.ToLower() == name.ToLower()).Select(g => g).ToList();
                }
                else
                {
                    users = users.Where(c => c.idUser != id).Where(c => c.username.ToLower() == name.ToLower()).Select(c => c).ToList();
                }

                if (users.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para insertar un registro de Usuario en la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-USE-003
        /// </summary>
        /// <param name="username">nombre de inicio de sesión de usuario</param>
        /// <param name="password">contraseña del usuario</param>
        /// <param name="name">nombre completo del usuario</param>
        /// <param name="idCompany">identificador de la compañia a la que pertenece</param>
        /// <param name="idProfile">identificador del perfil del usuario</param>
        /// <param name="status">identificador de estado del registro</param>
        /// <param name="iduser">identificador del usuario que realiza la acción</param>
        /// <returns>Retorna un entero con el identificador del elemento creado</returns>
        public static long AddUser(string username, string password, string name, int idCompany, int idProfile, int status, long iduser)
        {
            try
            {
                var user = new Dao.User
                {
                    username = username,
                    password = Utils.EncriptarMD5(password),
                    name = name,
                    idCompany = idCompany,
                    idProfile = idProfile,
                    status = status == 1 ? 1 : 0,
                    creationDate = DateTime.Now,
                    creationIdUser = iduser,
                    attempt = 0
                };

                var id = new Dao.Add.User().AddUser(user);
                return id;
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para actualizar un registro de User en la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-USE-004
        /// </summary>
        /// <param name="id">identificador del usuario</param>
        /// <param name="username">nombre de inicio de sesión de usuario</param>
        /// <param name="password">contraseña del usuario</param>
        /// <param name="name">nombre completo del usuario</param>
        /// <param name="idCompany">identificador de la compañia a la que pertenece</param>
        /// <param name="idProfile">identificador del perfil del usuario</param>
        /// <param name="status">identificador de estado del registro</param>
        /// <param name="iduser">identificador del usuario que realiza la acción</param>
        /// <returns>Retorna un objeto User con los cambios realizados</returns>
        public static Dao.User UpdUser(int id, string username, string password, string name, int idCompany, int idProfile, int status, long iduser)
        {
            try
            {
                var user = new Dao.Get.User().GetUser(id);

                if (password.Equals("editpassword"))
                {
                    password = user.password;
                }
                else
                {
                    password = Utils.EncriptarMD5(password);
                }

                var upduser = new Dao.User
                {
                    idUser = user.idUser,
                    username = username,
                    password = password,
                    name = name,
                    idCompany = idCompany,
                    idProfile = idProfile,
                    status = status == 1 ? 1 : 0,
                    creationDate = user.creationDate,
                    creationIdUser = user.creationIdUser,
                    modificationDate = DateTime.Now,
                    modificationIdUser = iduser,
                    attempt = 0
                };

                return new Dao.Upd.User().UpdUser(upduser) == 1 ? upduser : null;
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }
    }
}
