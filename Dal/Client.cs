﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal
{
    public class Client
    {
        /// <summary>
        /// Método para obtener un listado de objetos Client según su compañía.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-CLI-001
        /// </summary>
        /// <param name="idCompany">identificador de la compañia</param>
        /// <returns>Retorna una lista de objetos Client</returns>
        public static List<Dao.Client> GetClients(int idCompany)
        {
            try
            {
                return new Dao.Get.Client().GetClients(idCompany).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para obtener un objeto Client según su identificador.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-CLI-002
        /// </summary>
        /// <param name="id">identificador del cliente</param>
        /// <returns>Retorna un objeto Client</returns>
        public static Dao.Client GetClient(int id)
        {
            try
            {
                var result = new Dao.Get.Client().GetClient(id);
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para insertar un registro de Cliente en la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-CLI-003
        /// </summary>
        /// <param name="name">nombre del cliente</param>
        /// <param name="idcompany">identificador de la compañía</param>
        /// <param name="group">identificador del grupo al que pertenece</param>
        /// <param name="idconnection">identificador de la conexion utilizada</param>
        /// <param name="idtenant">identificador del tenant utilizado</param>
        /// <param name="status">identificador de estado del registro</param>
        /// <param name="user">identificador del usuario que realiza la acción</param>
        /// <returns>Retorna un entero con el identificador del elemento creado</returns>
        public static int AddClient(string name, int idcompany, int? group, int idconnection, string idtenant, int status, long user)
        {
            try
            {
                var client = new Dao.Client
                {
                    name = name,
                    idCompany = idcompany,
                    idGroup = group,
                    idConnection = idconnection,
                    idTenant = idtenant,
                    status = status == 1 ? 1 : 0,                  
                    creationDate = DateTime.Now,
                    creationIdUser = user
                };
                var id = new Dao.Add.Client().AddClient(client);
                return id;
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para actualizar un registro de cliente en la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-CLI-004
        /// </summary>
        /// <param name="idClient">identificador del cliente</param>
        /// <param name="name">nombre del cliente</param>
        /// <param name="idCompany">identificador de la compañia</param>
        /// <param name="idGroup">identificador del grupo al que pertenece</param>
        /// <param name="idConnection">identificador de la conexion utilizada</param>
        /// <param name="idTenant">identificador del tenant utilizado</param>
        /// <param name="status">identificador de estado del registro</param>
        /// <param name="user">identificador del usuario que realiza la acción</param>
        /// <returns>Retorna un objeto Cliente con los cambios realizados</returns>
        public static Dao.Client UpdClient(int idClient, string name, int idCompany, int? idGroup, int? idConnection, string idTenant, int status, long user)
        {
            try
            {
                var client_ = new Dao.Get.Client().GetClient(idClient);

                var client = new Dao.Client
                {
                    idClient = client_.idClient,
                    name = name,
                    idCompany = idCompany,
                    idGroup = idGroup,
                    idConnection = idConnection,
                    idTenant = idTenant,
                    status = status == 1 ? 1 : 0,
                    creationDate = client_.creationDate,
                    creationIdUser = client_.creationIdUser,
                    modificationDate = DateTime.Now,
                    modificationIdUser = user
                };

                return new Dao.Upd.Client().UpdClient(client) == 1 ? client : null;
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para validar la existencia de un nombre de cliente en una compañía especifica.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-CLI-005
        /// </summary>
        /// <param name="name">nombre del cliente</param>
        /// <param name="idCompany">identificador de la compañia</param>
        /// <param name="id">identificador del cliente a descartar</param>
        /// <returns>Retorna un bool con el resultado de la operacion. True si existe - False no existe</returns>
        public static bool ExistClient(string name, int idCompany, int id = 0)
        {
            try
            {
                var clients = GetClients(idCompany);

                if (id != 0)
                {
                    clients = clients.Where(c => c.idClient != id).Where(c => c.name.ToLower() == name.ToLower()).Select(c => c).ToList();
                }
                else {
                    clients = clients.Where(c => c.name.ToLower() == name.ToLower()).Select(c => c).ToList();
                }                

                if (clients.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }

    }
}
