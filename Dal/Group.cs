﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal
{
    public class Group
    {
        /// <summary>
        /// Método para obtener un objeto Group según su identificador.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-GRO-001
        /// </summary>
        /// <param name="id">identificador del grupo</param>
        /// <returns>Retorna un objeto Group</returns>
        public static Dao.Group GetGroup(int id)
        {
            try
            {
                var result = new Dao.Get.Group().GetGroup(id);
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para obtener un listado de objetos Group según su compañía.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-GRO-002
        /// </summary>
        /// <param name="idCompany">identificador de la compañía</param>
        /// <returns>Retorna una lista de objetos Group</returns>
        public static List<Dao.Group> GetGroups(int idCompany)
        {
            try
            {
                return new Dao.Get.Group().GetGroups(idCompany).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para insertar un registro de Group en la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-GRO-003
        /// </summary>
        /// <param name="name">nombre del grupo</param>
        /// <param name="company">identificador de la compañía</param>
        /// <param name="status">identificador de estado del registro</param>
        /// <param name="user">identificador del usuario que realiza la acción</param>
        /// <returns>Retorna un entero con el identificador del elemento creado</returns>
        public static int AddGroup(string name, int company, int status, long user)
        {
            try
            {
                var group = new Dao.Group
                {
                    name = name,
                    status = status == 1 ? 1 : 0,
                    idCompany = company,
                    creationDate = DateTime.Now,
                    creationIdUser = user
                };
                var id = new Dao.Add.Group().AddGroup(group);
                return id;
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para actualizar un registro de Group en la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-GRO-004
        /// </summary>
        /// <param name="id">identificador del grupo</param>
        /// <param name="company">identificador de la compañía</param>
        /// <param name="name">nombre del grupo</param>
        /// <param name="status">identificador de estado del registro</param>
        /// <param name="user">identificador del usuario que realiza la acción</param>
        /// <returns>Retorna un objeto Group con los cambios realizados</returns>
        public static Dao.Group UpdGroup(int id, int company, string name, int status, long user)
        {
            try
            {
                var group_ = new Dao.Get.Group().GetGroup(id);

                var group = new Dao.Group
                {
                    idGroup = group_.idGroup,
                    name = name,
                    idCompany = group_.idCompany,
                    status = status == 1 ? 1 : 0,
                    creationDate = group_.creationDate,
                    creationIdUser = group_.creationIdUser,
                    modificationDate = DateTime.Now,
                    modificationIdUser = user
                };

                return new Dao.Upd.Group().UpdGroup(group) == 1 ? group : null;
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para validar la existencia de un nombre de grupo en una compañía especifica.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-GRO-005
        /// </summary>
        /// <param name="name">nombre del grupo</param>
        /// <param name="idCompany">identificador de la compañía</param>
        /// <param name="id">identificador del grupo a descartar</param>
        /// <returns>Retorna un bool con el resultado de la operacion. True si existe - False no existe</returns>
        public static bool ExistGroup(string name, int idCompany, int id = 0)
        {
            try
            {
                var groups = GetGroups(idCompany);

                if (id == 0)
                {
                    groups = groups.Where(g => g.name.ToLower() == name.ToLower()).Select(g => g).ToList();
                }
                else
                {
                    groups = groups.Where(g => g.idGroup != id).Where(g => g.name.ToLower() == name.ToLower()).Select(g => g).ToList();
                }

                if (groups.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }

    }
}
