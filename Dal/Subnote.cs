﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal
{
    public class Subnote
    {
        /// <summary>
        /// Método para obtener un listado de objetos Subnote de una Note.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-SUB-001
        /// </summary>
        /// <param name="idnota">identificador de la nota</param>
        /// <returns>Retorna una lista de objetos Subnote</returns>
        public static List<Dao.Subnote> GetSubnote(int idnota)
        {
            try
            {
                return new Dao.Get.Subnote().GetSubnotes(idnota).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para obtener un objeto Subnote segun su identificador.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-SUB-002
        /// </summary>
        /// <param name="idsubnote">identificador de la subnota</param>
        /// <returns>Retorna un objeto Subnote</returns>
        public static Dao.Subnote GetOnlySubnote(int idsubnote)
        {
            try
            {
                return new Dao.Get.Subnote().GetSubnote(idsubnote);
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para insertar un registro de Subnote en la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-SUB-003
        /// </summary>
        /// <param name="txtsubnote">texto de la subnota</param>
        /// <param name="idnote"></param>
        /// <param name="status">identificador de estado del registro</param>
        /// <param name="user">identificador del usuario que realiza la acción</param>
        /// <returns>Retorna un entero con el identificador del elemento creado</returns>
        public static int AddSubnote(string txtsubnote, int idnote, int status, long user)
        {
            try
            {
                var subnote = new Dao.Subnote
                {
                    txtSubnote = txtsubnote,
                    idNote = idnote,
                    status = status == 1 ? 1 : 0,
                    creationDate = DateTime.Now,
                    creationIdUser = user
                };
                var id = new Dao.Add.Subnote().AddSubnote(subnote);
                return id;
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para actualizar un registro de Subnote en la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-SUB-004
        /// </summary>
        /// <param name="id">identificador de la subnota</param>
        /// <param name="txtsubnote">texto de la subnota</param>
        /// <param name="status">identificador de estado del registro</param>
        /// <param name="user">identificador del usuario que realiza la acción</param>
        /// <returns>Retorna un objeto Subnote con los cambios realizados</returns>
        public static Dao.Subnote UpdSubnote(int id, string txtsubnote, int status, long user)
        {
            try
            {
                var subnote_ = new Dao.Get.Subnote().GetSubnote(id);

                var subnote = new Dao.Subnote
                {
                    idSubnote = subnote_.idSubnote,
                    txtSubnote = txtsubnote,
                    idNote = subnote_.idNote,
                    status = status == 1 ? 1 : 0,
                    creationDate = subnote_.creationDate,
                    creationIdUser = subnote_.creationIdUser,
                    modificationDate = DateTime.Now,
                    modificationIdUser = user
                };

                return new Dao.Upd.Subnote().UpdSubnote(subnote) == 1 ? subnote : null;
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }

    }
}
