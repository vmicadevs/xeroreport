﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal
{
    public class Binnacle
    {
        /// <summary>
        /// Método para obtener un listado de objetos Binnacle segun compañia y tipo.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-BIN-001
        /// </summary>
        /// <param name="company">identificador de la compañia</param>
        /// <param name="type">identificador tipo de bitacora</param>
        /// <returns>Retorna una lista de objetos Binnacle</returns>
        public static List<Dao.Binnacle> GetBinnacles(int company, int type)
        {
            try
            {
                return new Dao.Get.Binnacle().GetBinnacles(company, type).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para agregar un objeto Binnacle a la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-BIN-002
        /// </summary>
        /// <param name="company">identificador de la compañia</param>
        /// <param name="type">identificador tipo de bitacora</param>
        /// <param name="quantity">cantidad de elementos afectados</param>
        /// <param name="description">descripción del hecho</param>
        /// <returns>Retorna un entero con el resultado de la operación, 1 Ok - 0 Error</returns>
        public static int AddBinnacle(int company, int type, int quantity, string description)
        {
            try
            {
                var binnacle = new Dao.Binnacle
                {
                    idCompany = company,
                    idTypeBinnacle = type,
                    quantity = quantity,
                    description = description,
                    date = DateTime.Now
                };
                var id = new Dao.Add.Binnacle().AddBinnacle(binnacle);
                if (id > 0)
                {
                    return 1;
                }
                return 0;
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }
    }
}
