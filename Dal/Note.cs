﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal
{
    public class Note
    {
        /// <summary>
        /// Método para obtener un objeto Note según su identificador.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-NOT-001
        /// </summary>
        /// <param name="id">identificador de la nota</param>
        /// <returns>Retorna una lista de objetos Note</returns>
        public static Dao.Note GetNote(int id)
        {
            try
            {
                var result = new Dao.Get.Note().GetNote(id);
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para obtener un listado de objetos Note según su compañía.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-NOT-002
        /// </summary>
        /// <param name="idCompany">identificador de la compañía</param>
        /// <returns>Retorna una lista de objetos Note</returns>
        public static List<Dao.Note> GetNotes(int idCompany)
        {
            try
            {
                return new Dao.Get.Note().GetNotes(idCompany).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para insertar un registro de Note en la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-NOT-003
        /// </summary>
        /// <param name="txtnote">contenido de la nota</param>
        /// <param name="company">identificador de la compañía</param>
        /// <param name="status">identificador de estado del registro</param>
        /// <param name="user">identificador del usuario que realiza la acción</param>
        /// <returns>Retorna un entero con el identificador del elemento creado</returns>
        public static int AddNote(string txtnote, int company, int status, long user)
        {
            try
            {
                var Note = new Dao.Note
                {
                    txtNote = txtnote,
                    isSelected = 0,
                    idCompany = company,
                    status = status == 1 ? 1 : 0,
                    creationDate = DateTime.Now,
                    creationIdUser = user
                };
                var id = new Dao.Add.Note().AddNote(Note);
                return id;
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para actualizar un registro de Note en la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-NOT-004
        /// </summary>
        /// <param name="id">identificador de la nota</param>
        /// <param name="company">identificador de la compañía</param>
        /// <param name="txtnote">contenido de la nota</param>
        /// <param name="status">identificador de estado del registro</param>
        /// <param name="user">identificador del usuario que realiza la acción</param>
        /// <returns>Retorna un objeto Note con los cambios realizados</returns>
        public static Dao.Note UpdNote(int id, int company, string txtnote, int status, long user)
        {
            try
            {
                var Note_ = new Dao.Get.Note().GetNote(id);

                var Note = new Dao.Note
                {
                    idNote = Note_.idNote,
                    txtNote = txtnote,
                    idCompany = Note_.idCompany,
                    isSelected = Note_.isSelected,
                    status = status == 1 ? 1 : 0,
                    creationDate = Note_.creationDate,
                    creationIdUser = Note_.creationIdUser,
                    modificationDate = DateTime.Now,
                    modificationIdUser = user
                };

                return new Dao.Upd.Note().UpdNote(Note) == 1 ? Note : null;
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para actualizar la marca de seleccion de un registro Note en la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-NOT-005
        /// </summary>
        /// <param name="id">identificador de la nota</param>
        /// <param name="selected">marca de seleccion de nota</param>
        /// <param name="user">identificador del usuario que realiza la acción</param>
        /// <returns>Retorna un int con el resultado de la operacion. 1[OK] - 0[Error]</returns>
        public static int UpdNote(int id, int selected, long user)
        {
            try
            {
                var Note_ = new Dao.Get.Note().GetNote(id);

                var Note = new Dao.Note
                {
                    idNote = Note_.idNote,
                    txtNote = Note_.txtNote,
                    idCompany = Note_.idCompany,
                    isSelected = selected,
                    status = Note_.status,
                    creationDate = Note_.creationDate,
                    creationIdUser = Note_.creationIdUser,
                    modificationDate = DateTime.Now,
                    modificationIdUser = user
                };

                return new Dao.Upd.Note().UpdNote(Note) == 1 ? 1 : 0;
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para restablecer la marca de seleccion de todos los registro Note de una compañía en la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-NOT-006
        /// </summary>
        /// <param name="company">identificador de la compañía</param>
        /// <param name="user">identificador del usuario que realiza la acción</param>
        /// <returns>Retorna un int con el resultado de la operacion. 1[OK] - 0[Error]</returns>
        public static int resetSelected(int company, long user)
        {
            try
            {
                var Notes_ = GetNotes(company).Where(n => n.isSelected == 1).Select(m => m).ToList();

                foreach (var n in Notes_)
                {
                    var Note = new Dao.Note
                    {
                        idNote = n.idNote,
                        txtNote = n.txtNote,
                        idCompany = n.idCompany,
                        isSelected = 0,
                        status = n.status,
                        creationDate = n.creationDate,
                        creationIdUser = n.creationIdUser,
                        modificationDate = DateTime.Now,
                        modificationIdUser = user
                    };

                    var result = new Dao.Upd.Note().UpdNote(Note) == 1 ? 1 : 0;

                    if (result == 0)
                    {
                        return 0;
                    }
                }

                return 1;
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para validar la existencia de una nota en una compañía especifica.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-NOT-007
        /// </summary>
        /// <param name="txtnote">contenido de la nota</param>
        /// <param name="idCompany">identificador de la compañía</param>
        /// <param name="id">identificador de la nota</param>
        /// <returns>Retorna un bool con el resultado de la operacion. True si existe - False no existe</returns>
        public static bool ExistNote(string txtnote, int idCompany, int id = 0)
        {
            try
            {
                var Notes = GetNotes(idCompany);

                if (id == 0)
                {
                    Notes = Notes.Where(g => g.txtNote.ToLower() == txtnote.ToLower()).Select(g => g).ToList();
                }
                else
                {
                    Notes = Notes.Where(g => g.idNote != id).Where(g => g.txtNote.ToLower() == txtnote.ToLower()).Select(g => g).ToList();
                }

                if (Notes.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }
    }
}
