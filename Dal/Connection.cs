﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal
{
    public class Connection
    {
        /// <summary>
        /// Método para obtener un listado de objetos Connection según su compañía.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-CON-001
        /// </summary>
        /// <param name="idcompany">identificador de la compañía</param>
        /// <returns>Retorna una lista de objetos Connection</returns>
        public static List<Dao.Connection> GetConnections(int idcompany)
        {
            try
            {
                return new Dao.Get.Connection().GetConnections(idcompany).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para obtener un objeto Coneccion según su identificador.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-CON-002
        /// </summary>
        /// <param name="id">identificador de la conexión</param>
        /// <returns>Retorna un objeto Connection</returns>
        public static Dao.Connection GetConnection(int id)
        {
            try
            {
                return new Dao.Get.Connection().GetConnection(id);
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para insertar un registro de Connection en la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-CON-003
        /// </summary>
        /// <param name="name">nombre de la conexión</param>
        /// <param name="idcompany">identificador de la compañía</param>
        /// <param name="clientid">hash del cliente API xero</param>
        /// <param name="clientsecret">hash de conexión secreto API xero</param>
        /// <param name="urlcallback">direccion url de retorno API xero</param>
        /// <param name="scope">permisos requeridos en la comunicacion con API xero</param>
        /// <param name="state">cadena única que se va a devolver al finalizar</param>
        /// <param name="storedtoken">token almacenado para la nueva conxion a API xero</param>
        /// <param name="status">identificador de estado del registro</param>
        /// <param name="user">identificador del usuario que realiza la acción</param>
        /// <returns>Retorna un entero con el identificador del elemento creado</returns>
        public static int AddConnection(string name, int idcompany, string clientid, string clientsecret, string urlcallback, string scope, string state, string storedtoken, int status, long user)
        {
            try
            {
                var connection = new Dao.Connection
                {
                    name = name,
                    idCompany = idcompany,
                    xeroClientId = clientid,
                    xeroClientSecret = clientsecret,
                    xeroCallbackUri = urlcallback,
                    xeroScope = scope,
                    xeroState = state,
                    storedToken = storedtoken,
                    status = status,
                    creationDate = DateTime.Now,
                    creationIdUser = user
                };
                var id = new Dao.Add.Connection().AddConnection(connection);
                return id;
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para actualizar un registro de conexión en la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-CON-004
        /// </summary>
        /// <param name="id">identificador de la conexión</param>
        /// <param name="name">nombre de la conexión</param>
        /// <param name="clientid">hash del cliente API xero</param>
        /// <param name="clientsecret">hash de conexión secreto API xero</param>
        /// <param name="urlcallback">direccion url de retorno API xero</param>
        /// <param name="scope">permisos requeridos en la comunicacion con API xero</param>
        /// <param name="state">cadena única que se va a devolver al finalizar</param>
        /// <param name="storedtoken">token almacenado para la nueva conxion a API xero</param>
        /// <param name="status">identificador de estado del registro</param>
        /// <param name="user">identificador del usuario que realiza la acción</param>
        /// <returns>Retorna un objeto Connection con los cambios realizados</returns>
        public static Dao.Connection UpdConnection(int id, string name, string clientid, string clientsecret, string urlcallback, string scope, string state, string storedtoken, int status, long user)
        {
            try
            {
                var connection_ = new Dao.Get.Connection().GetConnection(id);

                var connection = new Dao.Connection
                {
                    idConnection = connection_.idConnection,
                    name = name,
                    idCompany = connection_.idCompany,
                    xeroClientId = clientid,
                    xeroClientSecret = clientsecret,
                    xeroCallbackUri = urlcallback,
                    xeroScope = scope,
                    xeroState = state,
                    storedToken = storedtoken,
                    status = status,
                    creationDate = connection_.creationDate,
                    creationIdUser = connection_.creationIdUser,
                    modificationDate = DateTime.Now,
                    modificationIdUser = user
                };

                return new Dao.Upd.Connection().UpdConnection(connection) == 1 ? connection : null;
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para actualizar el token de acceso API a un registro de conexión en la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-CON-005
        /// </summary>
        /// <param name="id">identificador de la conexión</param>
        /// <param name="storedtoken">token almacenado para la nueva conxion a API xero</param>
        /// <param name="status">identificador de estado del registro</param>
        /// <param name="user">identificador del usuario que realiza la acción</param>
        /// <returns>Retorna un objeto Connection con los cambios realizados</returns>
        public static Dao.Connection UpdConnection(int id, string storedtoken, int status, long user)
        {
            try
            {
                var connection_ = new Dao.Get.Connection().GetConnection(id);

                var connection = new Dao.Connection
                {
                    idConnection = connection_.idConnection,
                    name = connection_.name,
                    idCompany = connection_.idCompany,
                    xeroClientId = connection_.xeroClientId,
                    xeroClientSecret = connection_.xeroClientSecret,
                    xeroCallbackUri = connection_.xeroCallbackUri,
                    xeroScope = connection_.xeroScope,
                    xeroState = connection_.xeroState,
                    storedToken = storedtoken,
                    status = status,
                    creationDate = connection_.creationDate,
                    creationIdUser = connection_.creationIdUser,
                    modificationDate = DateTime.Now,
                    modificationIdUser = user
                };

                return new Dao.Upd.Connection().UpdConnection(connection) == 1 ? connection : null;
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para validar la existencia de un nombre de conexión en una compañía especifica.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-CON-006
        /// </summary>
        /// <param name="name">nombre de la conexión</param>
        /// <param name="clientid">identificador del cliente API xero</param>
        /// <param name="idCompany">identificador de la compañía</param>
        /// <param name="id">identificador de la conexión a descartar</param>
        /// <returns>Retorna un bool con el resultado de la operacion. True si existe - False no existe</returns>
        public static bool ExistConnection(string name, string clientid, int idCompany, int id = 0)
        {
            try
            {
                var connections = GetConnections(idCompany);

                if (id == 0)
                {
                    connections = connections.Where(c => c.name.ToLower() == name.ToLower()).Where(c => c.xeroClientId.ToLower() == clientid.ToLower()).Select(c => c).ToList();
                }
                else
                {
                    connections = connections.Where(c => c.idConnection != id).Where(c => c.name.ToLower() == name.ToLower()).Where(c => c.xeroClientId.ToLower() == clientid.ToLower()).Select(c => c).ToList();
                }

                if (connections.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }
    }
}
