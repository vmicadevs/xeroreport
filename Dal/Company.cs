﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal
{
    public class Company
    {
        /// <summary>
        /// Método para obtener el nombre de una compañía especifica.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-COM-001
        /// </summary>
        /// <param name="id">identificador de la compañía</param>
        /// <returns>Retorna uan cadena de texto con el nombre de la compañía</returns>
        public static string GetCompanyName(int id)
        {
            try
            {
                var result = new Dao.Get.Company().GetCompany(id);
                return result.name;
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para obtener un listado de objetos Company.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-COM-002
        /// </summary>
        /// <returns>Retorna una lista de objetos Company</returns>
        public static List<Dao.Company> GetCompanies()
        {
            try
            {
                return new Dao.Get.Company().GetCompanies().ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para insertar un registro de Compañía en la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-COM-003
        /// </summary>
        /// <param name="name">nombre de la compañía</param>
        /// <param name="status">identificador de estado del registro</param>
        /// <param name="user">identificador del usuario que realiza la acción</param>
        /// <returns>Retorna un entero con el identificador del elemento creado</returns>
        public static int AddCompany(string name, int status, long user)
        {
            try
            {
                var company = new Dao.Company
                {
                    name = name,
                    status = status == 1 ? 1 : 0,
                    creationDate = DateTime.Now,
                    creationIdUser = user
                };
                var id = new Dao.Add.Company().AddCompany(company);
                return id;
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para actualizar un registro de compañía en la base de datos.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-COM-004
        /// </summary>
        /// <param name="id">identificador de la compañía</param>
        /// <param name="name">nombre de la compañía</param>
        /// <param name="status">identificador de estado del registro</param>
        /// <param name="user">identificador del usuario que realiza la acción</param>
        /// <returns>Retorna un objeto Company con los cambios realizados</returns>
        public static Dao.Company UpdCompany(int id, string name, int status, long user)
        {
            try
            {
                var company_ = new Dao.Get.Company().GetCompany(id);

                var company = new Dao.Company
                {
                    idCompany = company_.idCompany,
                    name = name,
                    status = status == 1 ? 1 : 0,
                    creationDate = company_.creationDate,
                    creationIdUser = company_.creationIdUser,
                    modificationDate = DateTime.Now,
                    modificationIdUser = user
                };

                return new Dao.Upd.Company().UpdCompany(company) == 1 ? company : null;
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para validar la existencia de un nombre de compañía.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-COM-005
        /// </summary>
        /// <param name="name">nombre de la compañía</param>
        /// <param name="id">identificador de la compañía a descartar</param>
        /// <returns>Retorna un bool con el resultado de la operacion. True si existe - False no existe</returns>
        public static bool ExistCompany(string name, int id = 0)
        {
            try
            {
                var company = GetCompanies();

                if (id != 0)
                {
                    company = company.Where(c => c.idCompany != id).Where(c => c.name.ToLower() == name.ToLower()).Select(c => c).ToList();
                }
                else
                {
                    company = company.Where(g => g.name.ToLower() == name.ToLower()).Select(g => g).ToList();
                }

                if (company.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }

    }
}
