﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal
{
    public class Acceso
    {
        /// <summary>
        /// Método para obtener un objeto User desde la base de datos y reiniciar sus intentos de acceso.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-ACC-001
        /// </summary>
        /// <param name="user">nombre de usuario</param>
        /// <param name="password">contraseña</param>
        /// <returns>Retorna un objeto User</returns>
        public static Dao.User GetUsuario(string user, string password)
        {
            try
            {
                var pasx = Utils.EncriptarMD5(password);
                var result = new Dao.Get.User().validaLogin(user, pasx);
                if (result != null)
                {
                    result.password = "";

                    var resetAttempt = new Dao.Get.User().GetUser(user);
                    resetAttempt.attempt = 0;
                    var user_ = new Dao.Upd.User().UpdUser(resetAttempt);
                }
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para registrar los intentos de acceso de un usuario y bloquear al usuario en caso de exceder el máximo.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-ACC-002
        /// </summary>
        /// <param name="user">nombre de usuario</param>
        /// <returns>Retorna un entero con el número de intentos</returns>
        public static int Attempts(string user)
        {
            try
            {
                int attempt = 0;
                var result = new Dao.Get.User().GetUser(user);

                if (result != null)
                {
                    result.attempt += 1;
                    if (result.attempt > 3)
                    {
                        result.status = 0;
                    }
                    var user_ = new Dao.Upd.User().UpdUser(result);

                    attempt = result.attempt == null ? 0 : result.attempt.Value;
                }
                return attempt;
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }
    }
}
