﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace Dal
{
    public class Utils
    {
        /// <summary>
        /// Encripta una cadena de caracteres a MD5
        /// </summary>
        /// <param name="str">cadena de texto de entrada</param>
        /// <returns>Retorna un hash md5</returns>
        public static string EncriptarMD5(string str)
        {
            MD5 md5 = MD5CryptoServiceProvider.Create();
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] stream = null;
            StringBuilder sb = new StringBuilder();
            stream = md5.ComputeHash(encoding.GetBytes(str));
            for (int i = 0; i < stream.Length; i++) sb.AppendFormat("{0:x2}", stream[i]);
            return sb.ToString();
        }

        /// <summary>
        /// Crea logs y registra el texto entregado como parametro. La ruta de logs se especifica en WebConfig.
        /// </summary>
        /// <param name="nombreLog">Nombre del archivo log. Le agrega la extensión .log en caso de no traerla.</param>
        /// <param name="texto">Texto que se registrara en el log</param>
        /// <param name="nombreDir">Nombre del directorio donde se guardara el log</param>
        public static void Log(string nombreLog, string texto, string nombreDir = null)
        {
            var path = ConfigurationManager.AppSettings["PathLog"];
            nombreLog += (!nombreLog.EndsWith(".log", StringComparison.OrdinalIgnoreCase) ? ".log" : "");
            if (System.IO.Directory.Exists(path))
            {
                if (nombreDir == null) nombreDir = string.Format("{0:ddMMyyyy}", DateTime.Now);
                var dirAbsoluto = System.IO.Path.Combine(path, nombreDir);
                if (!System.IO.Directory.Exists(dirAbsoluto)) System.IO.Directory.CreateDirectory(dirAbsoluto);
                dirAbsoluto = System.IO.Path.Combine(dirAbsoluto, nombreLog);
                if (!System.IO.File.Exists(dirAbsoluto)) System.IO.File.Create(dirAbsoluto).Close();
                System.IO.StreamWriter sw = new System.IO.FileInfo(dirAbsoluto).AppendText();
                sw.WriteLine("{0:HH:mm:ss} | {1}", DateTime.Now, texto);
                sw.Close();
            }
        }

        /// <summary>
        /// Metodo para obtener el último registro del log
        /// </summary>
        /// <param name="nombreLog">Nombre del archivo log</param>
        /// <param name="nombreDir">Nombre del directorio donde se encuentra el archivo</param>
        /// <returns>Retorna una cadena de texto con el registro log</returns>
        public static string GetUltimoRegistroLog(string nombreLog, string nombreDir = null)
        {
            try
            {
                if (nombreDir == null) nombreDir = string.Format("{0:ddMMyyyy}", DateTime.Now);
                if (!nombreLog.EndsWith(".log", StringComparison.OrdinalIgnoreCase)) nombreLog += ".log";
                var lines = System.IO.File.ReadAllLines(string.Format("{0}/{1}/{2}", ConfigurationManager.AppSettings["PathLog"], nombreDir, nombreLog));
                return lines[lines.Length - 1];
            }
            catch (System.IO.DirectoryNotFoundException)
            {
                return "Records not found";
            }
            catch (System.IO.FileNotFoundException)
            {
                return "Records not found";
            }
        }

        /// <summary>
        /// Metodo para reemplazar con * caracteres de una cadena de texto
        /// </summary>
        /// <param name="str">Cadena de texto</param>
        /// <returns>Retorna una cadena de texto con sus caracteres ocultos menos los 2 primeros y 2 últimos</returns>
        public static string ocultarCaracteres(string str)
        {
            try
            {
                int inicio = 2;
                int final = 2;
                int longitud;
                if (str.Length > inicio + final)
                {
                    longitud = str.Length - final - inicio;
                }
                else
                {
                    longitud = 1;
                }

                str = str.Remove(inicio, longitud).Insert(inicio, new string('*', longitud));
                return str;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        /// <summary>
        /// Metodo para acortar una cadena de texto mayor a nueve caracteres
        /// </summary>
        /// <param name="str">Cadena de texto</param>
        /// <returns>Retorna un string de 9</returns>
        public static string quitarCaracteres(string str)
        {
            try
            {
                if (str.Length > 9)
                {
                    str = str.Substring(0, 6) + "...";
                }

                return str;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        /// <summary>
        /// Clase base del objeto FiscalYear
        /// </summary>
        public class FiscalYear
        {
            public int yearToShow { get; set; }
            public DateTime startFiscalYear { get; set; }
            public DateTime endFiscalYear { get; set; }
            public DateTime startReportFiscalYear { get; set; }
            public DateTime endReportFiscalYear { get; set; }
            public int qMonthsProgress { get; set; }
            public int qMonthsPending { get; set; }

        }

        /// <summary>
        /// Metodo para calcular el año fiscal a cual pertenece una fecha (año, inicio, fin, inicio reporte, fin reporte, progreso y meses pendientes).
        /// </summary>
        /// <param name="fecha">Fecha a calcular</param>
        /// <returns>Retorna un objeto FiscalYear</returns>
        public static FiscalYear GetFiscalYear(string fecha = null)
        {
            try
            {
                var actualDate = fecha == null ? DateTime.Now : DateTime.ParseExact(fecha, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                var fiscalYear = new FiscalYear();

                //fiscalYear.yearToShow = actualDate.Month > 7 ? actualDate.Year : (actualDate.Year - 1);
                fiscalYear.yearToShow = actualDate.Month > 7 ? (actualDate.Year + 1) : actualDate.Year;

                fiscalYear.startFiscalYear = new DateTime(fiscalYear.yearToShow - 1, 07, 01);

                fiscalYear.endFiscalYear = new DateTime(fiscalYear.yearToShow, 06, 30, 23, 59, 59);

                var firstDayCurrentMonth = new DateTime(actualDate.Year, actualDate.Month, 1, 23, 59, 59);
                fiscalYear.endReportFiscalYear = firstDayCurrentMonth.AddDays(-1);

                fiscalYear.startReportFiscalYear = new DateTime(fiscalYear.endReportFiscalYear.Year, fiscalYear.endReportFiscalYear.Month, 1);

                fiscalYear.qMonthsProgress = ((((fiscalYear.endReportFiscalYear.Year - fiscalYear.startFiscalYear.Year) * 12) + fiscalYear.endReportFiscalYear.Month - fiscalYear.startFiscalYear.Month) + 1);

                fiscalYear.qMonthsPending = (12 - fiscalYear.qMonthsProgress);

                return fiscalYear;
            }
            catch (Exception ex)
            {
                throw new Exception("Error calculating fiscal year [" + ex.Message + "]");
            }
        }

        /// <summary>
        /// Metodo para obtener el año fiscal de una fecha
        /// </summary>
        /// <param name="fecha">Fecha a calcular</param>
        /// <returns>Retorna un entero con el año</returns>
        public static int GetYearFiscalYear(string fecha = null)
        {
            try
            {
                var actualDate = fecha == null ? DateTime.Now : DateTime.ParseExact(fecha, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                //var year = actualDate.Month > 7 ? actualDate.Year : (actualDate.Year - 1);
                var year = actualDate.Month > 7 ? (actualDate.Year + 1) : actualDate.Year;

                return year;
            }
            catch (Exception ex)
            {
                throw new Exception("Error calculating year of fiscal year [" + ex.Message + "]");
            }
        }

        /// <summary>
        /// Metodo para validar la correcta composición de una contraseña
        /// </summary>
        /// <param name="passWord">Cadena de texto</param>
        /// <returns>Retorna un bool con el resultado de la operacion. True si es válido - False es inválido</returns>
        public static bool validPassword(string passWord)
        {
            try
            {
                int validConditions = 0;
                foreach (char c in passWord)
                {
                    if (c >= 'a' && c <= 'z')
                    {
                        validConditions++;
                        break;
                    }
                }
                foreach (char c in passWord)
                {
                    if (c >= 'A' && c <= 'Z')
                    {
                        validConditions++;
                        break;
                    }
                }
                if (validConditions == 0) return false;
                foreach (char c in passWord)
                {
                    if (c >= '0' && c <= '9')
                    {
                        validConditions++;
                        break;
                    }
                }
                if (validConditions == 1) return false;

                char[] special = { '@', '#', '$', '%', '^', '&', '+', '=', '-', '*', '/', '!', '(', ')', '?', '¡' };
                if (passWord.IndexOfAny(special) == -1) return false;

                if (passWord.Length < 8) return false;

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Error to validate paswword [" + ex.Message + "]");
            }
        }

        /// <summary>
        /// Clase base del objeto ReportDates
        /// </summary>
        public class reportDates
        {
            public string date { get; set; }
            public string txtdate { get; set; }
        }

        /// <summary>
        /// Metodo para obtener los periodos comprendidos entre el inicio del año fiscar altual y el dia de hoy
        /// </summary>
        /// <returns>Retorna un listado de objetos ReportDates</returns>
        public static List<reportDates> GetDates()
        {
            try
            {
                var actualDate = DateTime.Now;
                var list = new List<reportDates>();

                var fiscalYear = new FiscalYear();
                fiscalYear.yearToShow = actualDate.Month > 7 ? (actualDate.Year + 1) : actualDate.Year;
                fiscalYear.startFiscalYear = new DateTime(fiscalYear.yearToShow - 1, 07, 01);
                var firstDayCurrentMonth = new DateTime(actualDate.Year, actualDate.Month, 1, 23, 59, 59);
                fiscalYear.endReportFiscalYear = firstDayCurrentMonth.AddDays(-1);
                fiscalYear.qMonthsProgress = ((((fiscalYear.endReportFiscalYear.Year - fiscalYear.startFiscalYear.Year) * 12) + fiscalYear.endReportFiscalYear.Month - fiscalYear.startFiscalYear.Month) + 1);

                var months = fiscalYear.qMonthsProgress;
                var advance = 0;
                while (months > 0)
                {
                    var reportDate = new reportDates();

                    var year = actualDate.Year;
                    var month = (actualDate.Month - advance);
                    if (month <= 0)
                    {
                        year -= 1;
                        month = month + 12;
                    }

                    var firstDayCurrentMonth_ = new DateTime(year, month, 1);
                    var endReportdate = firstDayCurrentMonth_.AddDays(-1);

                    reportDate.date = firstDayCurrentMonth_.ToString("dd-MM-yyyy");
                    reportDate.txtdate = "From " + fiscalYear.startFiscalYear.ToString("dd-MM-yyyy") + " to " + endReportdate.ToString("dd-MM-yyyy");

                    list.Add(reportDate);

                    advance++;
                    months--;
                }

                return list;
            }
            catch (Exception ex)
            {
                throw new Exception("Error getting report dates  [" + ex.Message + "]");
            }
        }

        /// <summary>
        /// Clase base del objeto DatePeriod
        /// </summary>
        public class DatePeriod
        {
            public int id { get; set; }
            public string month { get; set; }
            public DateTime start { get; set; }
            public DateTime end { get; set; }
        }

        /// <summary>
        /// Metodo para obtener los periodos comprendidos en el año fiscal de la fecha especificada
        /// </summary>
        /// <param name="fecha">Fecha a calcular</param>
        /// <returns>Retorna una lista de objetos DatePeriod</returns>
        public static List<DatePeriod> GetDatePeriods(string fecha = null)
        {
            try
            {
                var list = new List<DatePeriod>();

                var actualDate = fecha == null ? DateTime.Now : DateTime.ParseExact(fecha, "dd-MM-yyyy", CultureInfo.InvariantCulture);

                var fiscalYear = new FiscalYear();

                fiscalYear.yearToShow = actualDate.Month > 7 ? (actualDate.Year + 1) : actualDate.Year;
                fiscalYear.startFiscalYear = new DateTime(fiscalYear.yearToShow - 1, 07, 01);
                var firstDayCurrentMonth = new DateTime(actualDate.Year, actualDate.Month, 1, 23, 59, 59);
                fiscalYear.endReportFiscalYear = firstDayCurrentMonth.AddDays(-1);
                fiscalYear.startReportFiscalYear = new DateTime(fiscalYear.endReportFiscalYear.Year, fiscalYear.endReportFiscalYear.Month, 1);
                fiscalYear.qMonthsProgress = ((((fiscalYear.endReportFiscalYear.Year - fiscalYear.startFiscalYear.Year) * 12) + fiscalYear.endReportFiscalYear.Month - fiscalYear.startFiscalYear.Month) + 1);
                fiscalYear.qMonthsPending = (12 - fiscalYear.qMonthsProgress);

                for (int a = 0; a < fiscalYear.qMonthsProgress; a++)
                {
                    var period = new DatePeriod();

                    period.id = a;
                    period.month = FiscalMonthTo(a);

                    period.start = fiscalYear.startFiscalYear.AddMonths(a);
;
                    var end_ = fiscalYear.startFiscalYear.AddMonths(a + 1);
                    period.end = end_.AddDays(-1);

                    list.Add(period);
                }

                return list;
            }
            catch (Exception ex)
            {
                throw new Exception("Error getting report dates  [" + ex.Message + "]");
            }
        }
        
        /// <summary>
        /// Metodo para obtener la columna equivalente de excel de un numero
        /// </summary>
        /// <param name="num">numero entero entre 0 y 25</param>
        /// <returns>Retorna un string con la referencia a la columna</returns>
        public static string ColumnTo(int num)
        {
            if(num < 0 || num > 25)
            {
                return "";
            }

            var position = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };

            return position[num];
        }

        /// <summary>
        /// Metodo para obtener el nombre de un mes del año fiscal
        /// </summary>
        /// <param name="num">numero del mes del año fiscal</param>
        /// <returns>Retorna una string con el nombre del mes</returns>
        public static string FiscalMonthTo(int num)
        {
            if (num < 0 || num > 11)
            {
                return "";
            }

            var position = new string[] { "july", "august", "september", "october", "november", "december", "january", "february", "march", "april", "may", "june"};

            return position[num];
        }
    }
}
