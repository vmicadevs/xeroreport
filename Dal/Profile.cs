﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal
{
    public class Profile
    {
        /// <summary>
        /// Método para obtener el nombre del Perfil según su identificador.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-PRO-001
        /// </summary>
        /// <param name="id">identificador del perfil</param>
        /// <returns>Retorna una cadena de texto con el nombre</returns>
        public static string GetProfileName(int id)
        {
            try
            {
                var result = new Dao.Get.Profile().GetProfile(id);
                if (result != null) {
                    return result.name;
                }
                else {
                    return "";
                }                
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }

        /// <summary>
        /// Método para obtener un listado de todos objetos Profile.
        /// Creador: Sebastián Piña
        /// Fecha: 02/04/2021
        /// Correlativo: AR-DAL-PRO-002
        /// </summary>
        /// <returns>Retorna una lista de objetos Profile</returns>
        public static List<Dao.Profile> GetProfiles()
        {
            try
            {
                return new Dao.Get.Profile().GetProfiles().ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Controlled Exception: " + ex.Message);
            }
        }

    }
}
